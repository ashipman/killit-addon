var kUtils = require("./kUtils");


//======================================================================

exports["test Sorter 1"] = function(assert)
{
    let original = [7, 4, 5, 3];
    let expect   = [3, 4, 5, 7];
    let sorter   = new kUtils.Sorter(original, id, compare);

    let test = original.slice(0);
    sorter.apply(test, true);
    assert.deepEqual(test, expect, "Applying the sort permutation forwards");

    sorter.apply(test, false);
    assert.deepEqual(test, original, "Applying the sort permutation in reverse");
};



function id(x)
{
    return x;
}


function compare(a, b)
{
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

//======================================================================


exports["test Compare Versions 1"] = function(assert)
{
    assert.ok(kUtils.compareVersions("1", "2") < 0,        "Comparing '1' and '2'");
    assert.ok(kUtils.compareVersions("1.1", "1.2") < 0,    "Comparing '1.1' and '1.2'");
    assert.ok(kUtils.compareVersions("1.2", "1.2") === 0,  "Comparing '1.2' and '1.2'");
    assert.ok(kUtils.compareVersions("1.3", "1.2") > 0,    "Comparing '1.3' and '1.2'");
    assert.ok(kUtils.compareVersions("1.2.3", "1.2") > 0,  "Comparing '1.2.3' and '1.2'");
}



exports["test Valid Version 1"] = function(assert)
{
    assert.ok(kUtils.validVersion("1"),         "Valid version '1'");
    assert.ok(kUtils.validVersion("1.2"),       "Valid version '1.2'");
    assert.ok(kUtils.validVersion("99.23.0.1"), "Valid version '99.23.0.1'");
    assert.ok(!kUtils.validVersion(""),         "Not a valid version ''");
    assert.ok(!kUtils.validVersion("a"),        "Not a valid version 'a'");
}

//======================================================================

exports["test MyURL 1"] = function(assert)
{
    let u = kUtils.myURL("http://www.example.com/abc/def");

    assert.equal(u.scheme, "http",              "valid scheme");
    assert.equal(u.host,   "www.example.com",   "valid host");
    assert.equal(u.path,   "/abc/def",          "valid path");
}


exports["test MyURL 2"] = function(assert)
{
    // Interpret a bare host
    let u = kUtils.myURL("www.example.com");

    assert.equal(u.scheme, "",                  "valid scheme");
    assert.equal(u.host,   "www.example.com",   "valid host");
    assert.equal(u.path,   "",                  "valid path");
}


exports["test MyURL 3"] = function(assert)
{
    // Interpret a host with a path
    let u = kUtils.myURL("www.example.com/abc");

    assert.equal(u.scheme, "",                  "valid scheme");
    assert.equal(u.host,   "www.example.com",   "valid host");
    assert.equal(u.path,   "/abc",              "valid path");

    u = kUtils.myURL("//www.example.com/abc");

    assert.equal(u.scheme, "",                  "valid scheme");
    assert.equal(u.host,   "www.example.com",   "valid host");
    assert.equal(u.path,   "/abc",              "valid path");
}


exports["test MyURL 4"] = function(assert)
{
    // Interpret a host with an empty path
    let u = kUtils.myURL("www.example.com/");

    assert.equal(u.scheme, "",                  "valid scheme");
    assert.equal(u.host,   "www.example.com",   "valid host");
    assert.equal(u.path,   "/",                 "valid path");
}


exports["test MyURL 5"] = function(assert)
{
    // Interpret a bare path
    let u = kUtils.myURL("/abc");

    assert.equal(u.scheme, "",          "valid scheme");
    assert.equal(u.host,   "",          "valid host");
    assert.equal(u.path,   "/abc",      "valid path");
}


exports["test MyURL 6"] = function(assert)
{
    // Interpret a bare path
    let u = kUtils.myURL("/");

    assert.equal(u.scheme, "",          "valid scheme");
    assert.equal(u.host,   "",          "valid host");
    assert.equal(u.path,   "/",         "valid path");
}


exports["test MyURL 7"] = function(assert)
{
    // A single wildcard is treated as a host name.
    let u = kUtils.myURL("*");

    assert.equal(u.scheme, "",          "valid scheme");
    assert.equal(u.host,   "*",         "valid host");
    assert.equal(u.path,   "",          "valid path");
}


exports["test myURLtoString 1"] = function(assert)
{
    // A single wildcard is treated as a host name.
    let u1 = {scheme: "http", host: "www.example.com", path: "/abc"};
    let u2 = {scheme: "",     host: "www.example.com", path: "/abc"};
    let u3 = {scheme: "",     host: "",                path: "/abc"};
    let u4 = {scheme: "",     host: "",                path: "*"};

    assert.equal(kUtils.myURLtoString(u1), "http://www.example.com/abc",    "valid url 1");
    assert.equal(kUtils.myURLtoString(u2), "www.example.com/abc",           "valid url 2");
    assert.equal(kUtils.myURLtoString(u3), "/abc",                          "valid url 3");
    assert.equal(kUtils.myURLtoString(u4), "/*",                            "valid url 4");
}


exports["test myURLmatch 1"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com");
    let u1 = kUtils.myURL("www.example.com");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch 2"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com/abc/def");
    let u1 = kUtils.myURL("www.example.com/abc/def");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch 3"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com/abc/*");
    let u1 = kUtils.myURL("www.example.com/abc/def");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch 4"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com/*");
    let u1 = kUtils.myURL("www.example.com/abc/def");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch 5"] = function(assert)
{
    // This case is expressly disallowed.
    let p1 = kUtils.myURL("www.example.com/*");
    let u1 = kUtils.myURL("www.example.com");
    assert.ok(!kUtils.myURLmatch(p1, u1), "the pattern did not match");
}


exports["test myURLmatch 6"] = function(assert)
{
    // This case is expressly allowed.
    let p1 = kUtils.myURL("www.example.com/*");
    let u1 = kUtils.myURL("www.example.com/");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch wildcard host 1"] = function(assert)
{
    let p1 = kUtils.myURL("*.example.com");
    let u1 = kUtils.myURL("www.example.com");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch wildcard host 2"] = function(assert)
{
    let p1 = kUtils.myURL("*.example.com");
    let u1 = kUtils.myURL("www.example.com/abc");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch wildcard host 3"] = function(assert)
{
    let p1 = kUtils.myURL("*");
    let u1 = kUtils.myURL("www.example.com/abc");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch wildcard host 4"] = function(assert)
{
    let p1 = kUtils.myURL("http://*.vice.com");
    let u1 = kUtils.myURL("http://motherboard.vice.com/abc");
    assert.ok(kUtils.myURLmatch(p1, u1), "the pattern matched");
}


exports["test myURLmatch bad scheme 1"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com/*");
    let u1 = kUtils.myURL("chrome://www.example.com/");
    assert.ok(!kUtils.myURLmatch(p1, u1), "the pattern did not match");
}


exports["test myURLmatch bad scheme 2"] = function(assert)
{
    let p1 = kUtils.myURL("www.example.com/*");
    let u1 = kUtils.myURL("resource://www.example.com/");
    assert.ok(!kUtils.myURLmatch(p1, u1), "the pattern did not match");
}

//======================================================================

exports["test wildcardMatch 1"] = function(assert)
{
    assert.ok(kUtils.wildcardMatch("abc", "abc"), "pattern 'abc'");
    assert.ok(!kUtils.wildcardMatch("abc", "def"), "pattern 'abc' and 'def'");

    assert.ok(kUtils.wildcardMatch("ab*", "abf"), "pattern 'ab*' and 'abf'");
    assert.ok(!kUtils.wildcardMatch("ab*", "acf"), "pattern 'ab*' and 'acf'");

    assert.ok(kUtils.wildcardMatch("*", "abc"), "pattern '*' and 'abc'");
    assert.ok(kUtils.wildcardMatch("*", ""),    "pattern '*' and ''");

    assert.ok(kUtils.wildcardMatch("a?c", "abc"), "pattern 'a?c' and 'abc'");
    assert.ok(!kUtils.wildcardMatch("a?c", "abf"), "pattern 'a?c' and 'abf'");

    assert.ok(kUtils.wildcardMatch("a?c", "ABC", {caseInsensitive: true}), "pattern 'a?c' and 'ABC'");

    // regexp characters are matched literally
    assert.ok(kUtils.wildcardMatch("a++?c", "a++bc"), "pattern 'a++?c' and 'a++bc'");
    assert.ok(kUtils.wildcardMatch("a{?}c", "a{b}c"), "pattern 'a{?}c' and 'a{b}c'");
    assert.ok(kUtils.wildcardMatch("a$?c", "a$bc"),   "pattern 'a$?c' and 'a$bc'");
}

//======================================================================


exports["test sameJSON 1"] = function(assert)
{
    assert.ok(kUtils.sameJSON("abc", "abc"),    "the same strings match");

    assert.ok(!kUtils.sameJSON("abc", "def"),   "different strings don't match");
    assert.ok(!kUtils.sameJSON("abc", 123),     "strings and numbers don't match");
    assert.ok(!kUtils.sameJSON("abc", false),   "strings and booleans don't match");
    assert.ok(!kUtils.sameJSON("abc", null),    "strings and null don't match");

    assert.ok(kUtils.sameJSON(null, null),      "two nulls match");
    assert.ok(!kUtils.sameJSON(undefined, undefined), "undefined nevers matches");

    let j1left = {key1: "abc", key2: 123};
    let j1good = {key1: "abc", key2: 123};
    let j1bad  = {key1: "abc", key2: false};
    assert.ok(kUtils.sameJSON(j1left, j1good), "object j1good matches");
    assert.ok(!kUtils.sameJSON(j1left, j1bad), "object j1bad matches");

    // Keys are not ordered.
    let j2left = {key1: "abc", key2: {key3: 123, key4: null}};
    let j2good = {key1: "abc", key2: {key4: null, key3: 123}};
    let j2bad  = {key1: "abc", key2: {key4: "def", key3: 123}};
    assert.ok(kUtils.sameJSON(j2left, j2good), "object j2good matches");
    assert.ok(!kUtils.sameJSON(j2left, j2bad), "object j2bad matches");

    let a1left = ["abc", "def"];
    let a1good = ["abc", "def"];
    let a1bad  = ["abc", null];
    assert.ok(kUtils.sameJSON(a1left, a1good), "object a1good matches");
    assert.ok(!kUtils.sameJSON(a1left, a1bad), "object a1bad matches");

    let a2left = [];
    let a2good = [];
    let a2bad  = [1];
    assert.ok(kUtils.sameJSON(a2left, a2good), "object a2good matches");
    assert.ok(!kUtils.sameJSON(a2left, a2bad), "object a2bad matches");

}


exports["test sameJSON 2"] = function(assert)
{
    // Missing keys
    let orig = {key1: "abc", key2: {key3: 123, key4: null}};
    let bad  = {key1: "abc", key2: {key3: 123}};
    assert.ok(!kUtils.sameJSON(orig, bad), "object bad matches");
}


exports["test sameJSON 3"] = function(assert)
{
    // Extra keys
    let orig = {key1: "abc", key2: {key3: 123, key4: null}};
    let bad  = {key1: "abc", key2: {key3: 123, key4: null, key5: false}};
    assert.ok(!kUtils.sameJSON(orig, bad), "object bad matches");
}

//======================================================================

require("sdk/test").run(exports);
