"use strict";

/*  This contains the main startup code for the dialog. 
*/

const {log, showObj} = kUtils;

self.port.on("run", function(params, prefs)
{
    kUtils.setDebugLevel(params.debugLevel);

    //debug("dlgMainContent", showObj(params));
    kUtils.debug("dlgMainContent");

    // Set up the theme
    let theme = prefs.theme || "light";
    kTheme.setTheme(kTheme.mkTheme(theme));

    // Set up the tab buttons.
    let tabButtons = document.querySelectorAll("#tabs button");

    for (let i = 0; i < tabButtons.length; ++i)
    {
        tabButtons[i].addEventListener("click", onTabClick, false);
    }

    initAllPanes(params, prefs);
    showPane(1);

    /*  We can't get the close or beforeunload events, only the unload event.
        We can't stop the close.

        There is a race condition to worry about.  The worker will be
        detached soon after this. We hope that the finalizing messages
        get through before the worker is destroyed.
    */
    window.addEventListener("unload", () =>
    {
        showPane(0);
        shutdownAllPanes();
        self.port.emit("unloaded");
    }, false);
});



self.port.on("updatePrefs", function(prefs)
{
    let theme = prefs.theme || "light";
    kTheme.setTheme(kTheme.mkTheme(theme));
});



self.port.on("UICommand", function(info)
{
    /* The info object comes from the dialog module. It contains:

        command:        command name
        triggerId:      id of the context trigger
        triggerTagName: tag name of the context trigger
        id:             id of the event target
        value:          the "value" attribute of the event target

       We added the "handled" flag which callbacks set when the command
       has been handled. It is mainly for their internal use.
       REVISIT - This doesn't appear to be used now.
    */
    //kUtils.debug("dlg receives UICommand", info.command);

    info.handled = false;

    if (currentPane > 0)
    {
        let module = allPanes[currentPane];
        module.uiCommandHandler(info);
    }
});




self.port.on("contextmenushowing", function(info)
{
    // Ask the active pane for its context menu flags.
    if (currentPane > 0)
    {
        let module = allPanes[currentPane];
        let flags  = [];

        if (module && module.contextMenuShowing)
        {
            kUtils.debug("contextmenushowing", currentPane);
            flags = module.contextMenuShowing(info);
        }

        self.port.emit("command", "setMenuContext", flags);
    }
});



function requestBuiltin(cmd)
{
    self.port.emit("builtinCommand", cmd);
}

//======================================================================
// Tabbed Pane

var currentPane = 0;

/*  These are the panes that we know about. There is a module that implements
    each one. As a special case if the value is a string then it is treated
    as a command to emit rather than a change of pane.
*/

var allPanes = 
{
    1:      dlgGroupEditor,
    2:      dlgCatalog,
    3:      dlgAdmin,
    4:      dlgActivity,
    5:      "showHelp",
};



function initAllPanes(params, prefs)
{
    for (let i in allPanes)
    {
        let pane = allPanes[i];

        if (typeof pane !== "string")
        {
            pane.init(params, prefs);
        }
    }
}



function onTabClick(ev)
{
    let elem = ev.target;
    let id   = elem.id.substring(6) - 0;        // get it as an integer
    showPane(id);
}



function showPane(n)
{
    // Pass in n=0 to deactivate the current pane and leave nothing showing.
    let oldModule = allPanes[currentPane];
    let newModule = allPanes[n];


    if (typeof newModule === "string")
    {
        self.port.emit("command", newModule);
    }
    else
    {
        // Change to another module.
        if (oldModule)
        {
            oldModule.deactivate();
        }

        // Hide the other panes.
        if (n > 0)
        {
            let id    = "tabcontent" + n;
            let panes = document.querySelectorAll("div.tabcontent");

            for (let i = 0; i < panes.length; ++i)
            {
                let pane = panes[i];
                pane.setAttribute("hide", pane.id !== id);
            }

            if (newModule)
            {
                newModule.activate();
            }
        }

        currentPane = n;
        hilightTab(n);
    }
}



function hilightTab(n)
{
    let id = "button" + n;
    let bs = document.querySelectorAll("#tabs button");

    for (let i = 0; i < bs.length; ++i)
    {
        let b = bs[i];
        b.setAttribute("selected", b.id === id);
    }
}



function shutdownAllPanes()
{
    for (let n in allPanes)
    {
        let module = allPanes[n];

        if (module && typeof module !== "string")
        {
            module.shutdown();
        }
    }
}
