"use strict";

/*  This is included along-side the dlgMainContent script.
    It runs the preferences.
*/

let dlgAdmin = (function()
{
"use strict";

const {
    assert,
    debug, debugLevel,
    error, log, logException,
    showObj, showElem
    } = kUtils;

var gParams   = {};

//======================================================================

/*  We expect these globals:

    document - the dlgMain HTML document that has been loaded into the XUL shell.
    window   - for the document
    self     - the Add-On SDK communication link to the outside world.

    We export:

    init
    uiCommandHandler(info)  - return true if the command was handled
    activate                - the pane is now active
    deactivate              - the pane is now not active
    shutdown                - the dialog is being shut down

*/
let isActive = false;           // true if the editor pane is active


function init(params)
{
    gParams  = params;
    isActive = false;

    self.port.on("getPrefs", prefsToPage);

    // Localisation is done automatically via the data-l10n-id feature of the SDK.
    kDOM.findId("themeName").addEventListener("change", prefChanged, false);

    kDOM.findId("backupSave").addEventListener("click", doBackup, false);
    kDOM.findId("backupRestore").addEventListener("click", doRestore, false);
}



function activate()
{
    isActive = true;
    debug("dlgAdmin activated");

    // Load the initial preferences.
    self.port.emit("command", "getPrefs");
}



function deactivate()
{
    isActive = false;
    debug("dlgAdmin deactivated");
}



function shutdown()
{
}



var UICommands = {
    };


function uiCommandHandler(info)
{
    // Return true if we handled this command.
    if (isActive)
    {
        let func = UICommands[info.command];

        if (func)
        {
            try
            {
                func(info);
            }
            catch (e)
            {
                logException(e);
            }

            return true;
        }
    }

    return false;
}


//======================================================================


function prefsToPage(prefs)
{
    if (debugLevel())
    {
        debug("prefsToPage", showObj(prefs));
    }

    // There used to be more preferences
    let {theme} = prefs;

    kDOM.findId("themeName").value = theme;

    kDOM.findId("debugLevelRow").hidden = !gParams.debugEnable;

    if (gParams.debugEnable)
    {
        let elem = kDOM.findId("debugLevel");
        elem.value = kUtils.debugLevel();
        elem.addEventListener("change", () =>
        {
            try
            {
                let n = parseInt(elem.value, 10);
                kUtils.setDebugLevel(n);            // this is local to the dialog
                self.port.emit("command", "setDebugLevel", n);
            }
            catch(e)
            {
            }
        }, false);
    }
}




function prefChanged(ev)
{
    if (debugLevel())
    {
        debug("prefChanged", showObj(ev));
    }

    let elem = ev.target;

    switch (elem.id)
    {
    case "themeName":
        self.port.emit("command", "setPref", "theme", elem.value);
        break;
    }
}



function doBackup()
{
    debug("doBackup");
    self.port.emit("command", "doBackup");
}



function doRestore()
{
    debug("doRestore");
    self.port.emit("command", "doRestore");
}

//======================================================================

return {
    init:               init,
    uiCommandHandler:   uiCommandHandler,
    activate:           activate,
    deactivate:         deactivate,
    shutdown:           shutdown,
    };

}());
