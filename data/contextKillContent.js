"use strict";

let {debug, showObj, trimURL} = kUtils;

// This value doesn't need to match any code elsewhere.
const MAX_LEVELS = 7;

/*  This figures out what we have clicked on and what we can kill as
    a result.

    The message we return must be serialisable as JSON.  So we can only
    return the selector (typically an XPath) to the node.

    We prepare selectors for several parent levels as well.  The deepest
    one is first in the array. Since getSelectorForElement() does some
    optimisation using the #id attribute these paths are not simple
    prefixes of each other.

*/

self.on("click", function(node, data)
{
    let args = JSON.parse(data);
    kUtils.setDebugLevel(args.debugLevel);

    let levels = [];
    let elem   = node;

    for (   let i = 0;
            i < MAX_LEVELS && 
                elem !== document.documentElement &&
                elem !== document.body;
            ++i)
    {
        let level = kDOM.mkLevel(elem);
        debug("contextKillContent.click, selector", level.selector);

        levels.push(kDOM.mkLevel(elem));
        elem = elem.parentNode;
    }

    let info = {
        command: data,
        docURL:  trimURL(document.documentURI),
        levels:  levels,
        }

    self.postMessage(info);
});
