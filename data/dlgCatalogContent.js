"use strict";

/*  This is included along-side the dlgMainContent script.
*/

let dlgCatalog = (function()
{
"use strict";

const {
    assert,
    error, debug, logException,
    showObj, showElem
    } = kUtils;

var gParams  = {};

//======================================================================

/*  We expect these globals:

    document - the dlgMain HTML document that has been loaded into the XUL shell.
    window   - for the document
    self     - the Add-On SDK communication link to the outside world.

    We export:

    init
    uiCommandHandler(info)  - return true if the command was handled
    activate                - the pane is now active
    deactivate              - the pane is now not active
    shutdown                - the dialog is being shut down

*/
let isActive = false;           // true if the editor pane is active


function init(params)
{
    gParams   = params;
    isActive = false;
}



function activate()
{
    isActive = true;
    debug("dlgCatalog activated");

    self.port.on("getCatalog",      catalogToPage);
    self.port.on("groupsChanged",   groupsChanged);

    // Load the initial catalog
    self.port.emit("command", "getCatalog");
}



function deactivate()
{
    isActive = false;
    debug("dlgCatalog deactivated");
}



function shutdown()
{
}



var UICommands = {
    };


function uiCommandHandler(info)
{
    // Return true if we handled this command.
    if (isActive)
    {
        let func = UICommands[info.command];

        if (func)
        {
            try
            {
                func(info);
            }
            catch (e)
            {
                logException(e);
            }

            return true;
        }
    }

    return false;
}


//======================================================================


function catalogToPage(info)
{
    // These are arrays of objects.
    let {catalog, installed} = info;

    let root = kDOM.findId("catalogRoot");
    kDOM.clearDOMChildren(root);

    kUtils.sortGroups(catalog);

    let table = kDOM.create("table", root);

    let cg    = kDOM.create("colgroup", table);
    kDOM.create("col", cg);             // Group
    kDOM.create("col", cg);             // Version
    kDOM.create("col", cg).setAttribute("width", "70%"); // Comment

    // REVISIT - localise
    let head = kDOM.create("thead", table);
    let row  = kDOM.create("tr", head);

    head.classList.add("tableHeading");

    kDOM.appendText(kDOM.create("td", row), "Group");
    kDOM.appendText(kDOM.create("td", row), "Version");
    kDOM.appendText(kDOM.create("td", row), "Comment");

    catalog.forEach((cata) =>
    {
        row = kDOM.create("tr", table);
        kDOM.appendText(kDOM.create("td", row), cata.name);
        kDOM.appendText(kDOM.create("td", row), cata.contentVersion);
        kDOM.appendText(kDOM.create("td", row), cata.comment);

        /*  We always create a button. Its possible
            states are: installed, canUpgrade, canInstall
        */
        let uuid  = cata.uuid;
        let state = "canInstall";

        if (uuid in installed)
        {
            let inst = installed[uuid];

            let upg = inst.version && cata.contentVersion && 
                     kUtils.compareVersions(cata.contentVersion, inst.version) > 0;

            state = upg? "canUpgrade" : "installed";
        }

        let col = kDOM.create("td", row);
        let btn = kDOM.create("button", col);

        btn.setAttribute("data-url",  cata.url);
        btn.setAttribute("data-uuid", cata.uuid);
        btn.setAttribute("data-version", cata.contentVersion);
        btn.addEventListener("click", installGroup, false);

        setButtonState(btn, state);
    });
}



function setButtonState(button, state)
{
    switch (state)
    {
    case "installed":
        button.textContent = "Installed";
        button.disabled = true;
        break;

    case "canUpgrade":
        button.textContent = "Upgrade";
        button.disabled = false;
        break;

    case "canInstall":
        button.textContent = "Install";
        button.disabled = false;
        break;
    }

    button.setAttribute("data-state", state);
}



function installGroup(ev)
{
    /*  The url will be read by the top level of the add-on in dlgMain. We
        will find out about it via the groupsChanged event.
    */
    let btn  = ev.target;
    let url = btn.getAttribute("data-url");
    debug("catalog installing", url);
    self.port.emit("command", "installFromCatalog", url);
}



function groupsChanged(event)
{
    /*  Any group that is mentioned in the changed set must be treated as
        installed. Any mentioned in the removed set must be treated
        as available for installing again.
    */
    let {changed, removed} = event;

    changed.forEach((group) =>
    {
        /*  If there is a button for this uuid then set it to either
            Upgrade if the catalog has a newer group or remove the button
            since it is installed.
        */
        let btns = document.querySelectorAll("button");

        for (let i = 0; i < btns.length; ++i)
        {
            let btn = btns[i];

            if (btn.getAttribute("data-uuid") === group.uuid)
            {
                let cataVers = btn.getAttribute("data-version");

                let upg = group.version && cataVers &&
                                kUtils.compareVersions(cataVers, group.version) > 0;

                let state = upg? "canUpgrade" : "installed";
                setButtonState(btn, state);
            }
        }
    });

    removed.forEach((uuid) =>
    {
        let btns = document.querySelectorAll("button");

        for (let i = 0; i < btns.length; ++i)
        {
            let btn = btns[i];

            if (btn.getAttribute("data-uuid") === uuid)
            {
                setButtonState(btn, "canInstall");
            }
        }
    });
}

//======================================================================

return {
    init:               init,
    uiCommandHandler:   uiCommandHandler,
    activate:           activate,
    deactivate:         deactivate,
    shutdown:           shutdown,
    };

}());
