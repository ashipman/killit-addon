
let kTheme = (function()
{
"use strict";

// The style sheet we edit.
const ssTitle = "dlgMain";

//======================================================================

// Our dynamic theme
let defaultTheme = {
    bodyBackground:         "#fef8d2",
    borderColor:            "black",            // REVISIT not used?
    inputBackground:        "#cdfefd",
    openIconColor:          "black",
    selectedColor:          "#fe9000",
    selectedTabButtonShadowSize: 3,
    selectionColor:         "lightblue",
    shadeColor:             "#b0ffb0",
    tableHeadingColor:      "lightblue",
    tabBackground:          "lightblue",
    tabButtonBackground:    "linear-gradient(#ffffff, #c0c0c0)",
    tabButtonShadowColor:   "blue",
    tabButtonShadowSize:     2,
    textColor:              "black",
    };


let lightTheme = {
    bodyBackground:         "#f8f8f0",
    };


let darkTheme = {
    bodyBackground:         "#322F28",      // a shade of dark brown
    borderColor:            "white",
    inputBackground:        "#666666",
    openIconColor:          "white",
    shadeColor:             "#335533",
    tableHeadingColor:      "darkblue",
    textColor:              "white",
    };


function mkTheme(newTheme)
{
    if (typeof newTheme === "string")
    {
        switch (newTheme)
        {
        case "light": newTheme = lightTheme;    break;
        case "dark":  newTheme = darkTheme;     break;
        default:      newTheme = defaultTheme;  break;
        }
    }

    return merge(defaultTheme, newTheme);
}



function merge(oldTheme, newTheme)
{
    let t = {};

    for (let k in oldTheme)
    {
        t[k] = oldTheme[k];
    }

    for (let k in newTheme)
    {
        t[k] = newTheme[k];
    }

    return t;
}



function setTheme(theme)
{
    let s = findStyle(/body/);
    setBackground(s, theme.bodyBackground);
    s.color = theme.textColor;

    s = findStyle(/input/);
    s.borderColor     = theme.borderColor;
    s.color           = theme.textColor;
    setBackground(s, theme.inputBackground);

    s = findStyle(/openIcon/);
    s.color = theme.openIconColor;

    s = findStyle(/moz-selection/);
    setBackground(s, theme.selectionColor);

    s = findStyle(/tabs$/);
    setBackground(s, theme.tabBackground);

    s = findStyle(/data-selected-level/);
    setBackground(s, theme.selectedColor);

    s = findStyle(/tabs.*button$/);
    s.boxShadow = mkBoxShadow(theme.tabButtonShadowSize, theme.tabButtonShadowColor);
    setBackground(s, theme.tabButtonBackground);

    s = findStyle(/tabs.*button.*selected.*true/);
    s.boxShadow = mkBoxShadow(theme.selectedTabButtonShadowSize, theme.selectedColor);

    s = findStyle(/shade/);
    setBackground(s, theme.shadeColor);

    s = findStyle(/tableHeading/);
    setBackground(s, theme.tableHeadingColor);
}



function setBackground(style, value)
{
    if (value.match(/gradient/))
    {
        style.backgroundImage = value;
        style.backgroundColor = "";
    }
    else
    {
        style.setProperty("background-color", value, "important");
        style.backgroundImage = "";
    }
}


function mkBoxShadow(size, color)
{
    return "" + size + "px" +
           " " + size + "px" +
           " " + color;
}



function findStyle(regexp)
{
    /*  Find the style sheet with the fixed title in ssTitle.
        Within it find a style that matches the regexp.
    */
    let sheets = document.styleSheets;

    for (let s = 0; s < sheets.length; ++s)
    {
        let sheet = sheets[s];

        if (sheet.title === ssTitle)
        {
            let rules = sheets[s].cssRules;

            for (let r = 0; r < rules.length; ++r)
            {
                let rule = rules[r];

                if (rule.selectorText.match(regexp))
                {
                    return rule.style;
                }
            }
        }
    }

    return null;
}


return {
    defaultTheme:   defaultTheme,
    mkTheme:        mkTheme,
    setTheme:       setTheme,
    };

//======================================================================
}());
