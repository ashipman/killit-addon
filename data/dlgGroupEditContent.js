"use strict";

/*  This is included along-side the dlgMainContent script.
    It runs the group editor.
*/

let dlgGroupEditor = (function()
{
"use strict";

const {
    ANON_GROUP_NAME, ANON_GROUP_UUID,
    assert, debug,
    error, log, logException,
    showObj, showElem
    } = kUtils;

var gParams   = {};
var gPrefs    = {};

let enabledIcon  = "";
let disabledIcon = "";

const MODEL_KIND  = "model";
const GROUP_KIND  = "group";
const PART_KIND   = "part";
const SELECT_KIND = "selector";

//======================================================================

/*  We expect these globals:

    document - the dlgMain HTML document that has been loaded into the XUL shell.
    window   - for the document
    self     - the Add-On SDK communication link to the outside world.

    We export:

    init
    uiCommandHandler(info)  - return true if the command was handled
    activate                - the pane is now active
    deactivate              - the pane is now not active
    shutdown                - the dialog is being shut down

*/
let isActive = false;           // true if the editor pane is active

var theModel = null;


function init(params, prefs)
{
    kUtils.setDebugLevel(params.debugLevel);

    gParams   = params;
    gPrefs    = prefs;
    //debug("gParams", showObj(gParams));

    isActive = false;
    theModel = null;

    self.port.on("getConfigs",      configsToPage);
    self.port.on("groupsChanged",   groupsChanged);
    self.port.on("saveGroup",       saveGroup);
    self.port.on("updatePrefs",     updatePrefs);
    self.port.on("testDump",        () => theModel.testDump());

    let groupEdit = kDOM.findId("groupEditForm");
    groupEdit.addEventListener("click", groupEditClick, false);

    groupEdit.addEventListener("dragstart", dragStart, false);
    groupEdit.addEventListener("dragdrop",  dragDrop, false);
    groupEdit.addEventListener("dragenter", dragOver, false);
    groupEdit.addEventListener("dragover",  dragOver, false);
}



function activate()
{
    isActive = true;

    // Load the initial configs.
    self.port.emit("command", "getConfigs");
}



function deactivate()
{
    theModel.checkAndSave();
    isActive = false;
    theModel = null;
}



function shutdown()
{
}



var UICommands = {
    'cmd-save':             doSave,
    'cmd-close':            doClose,            // for the Ctrl-W accelerator

    'cmd-expand-all':       doExpandAll,
    'cmd-export-group':     doExportGroup,

    'cmd-new-group':        doNewGroup,
    'cmd-new-part':         doNewPart,
    'cmd-new-selector':     doNewSelector,

    'cmd-copy':             doCopy,
    'cmd-paste':            doPasteLevel,
    'cmd-cut':              doCut,
    'cmd-delete':           doDelete,
    'cmd-deselect-all':     doDeselectAll,
    'cmd-select-all':       doSelectAll,
    'cmd-sort-groups':      doSortGroups,
    'cmd-sort-parts':       doSortParts,

    'cmd-undo':             doUndo,
    'cmd-redo':             doRedo,
    };


function uiCommandHandler(info)
{
    // Return true if we handled this command.
    if (isActive)
    {
        let func = UICommands[info.command];

        if (func)
        {
            try
            {
                func(info);
            }
            catch (e)
            {
                logException(e);
            }

            return true;
        }
    }

    return false;
}



function saveConfigs(changed, removed)
{
    // Save the configs.
    // debug("dlgMainContent sending saveConfigs 1");
    self.port.emit("command", "saveConfigs", changed, removed);
}



function saveUI(ui)
{
    // Save the UI config.
    self.port.emit("command", "saveUI", ui);
}



function exportGroup(configs)
{
    // Export the configs to a file.
    self.port.emit("command", "exportGroup", configs);
}



function updatePrefs(prefs)
{
    gPrefs = prefs;
}


//======================================================================

// The id of the DOM tree for the model.
const MODEL_ROOT_ID = "groupEditView";

const LEVEL_ATTR  = "data-level-root";
const OPEN_ATTR   = "data-open-level";
const SEL_ATTR    = "data-selected-level";

const OPEN_ICON   = "open_level.png";
const CLOSED_ICON = "closed_level.png";

const ERROR_CLASS = "invalid";

/*  The big size is for editable fields and most titles.
    The small size must be big enough for a locale-specific date and time.
*/
const FIELD_SIZE_BIG   = 40;
const FIELD_SIZE_SMALL = 20;

//  Selection mode
const SELECT_SINGLE = 1;        // deselect all others and select this
const SELECT_MULTI  = 2;        // a Ctrl-Click, select individual levels
const SELECT_RANGE  = 3;        // a Shift-Click, select a range

// This allows wildcards
const reUrlPart = new RegExp(/[-*?a-zA-Z0-9]+/);


let childKind = {};

childKind[MODEL_KIND] = GROUP_KIND;
childKind[GROUP_KIND] = PART_KIND;
childKind[PART_KIND]  = SELECT_KIND;

//======================================================================


function configsToPage(data)
{
    /*  configs is an object with UUIDs as keys and the groups
        as values. It has been through JSON so it is probably a 
        copy we can damage but at the moment we preserve it.

        This creates a new model.
    */
    try
    {
        theModel = new Model(data);

        // Validation depends on theModel being fully built.
        theModel.checkValidity();
    }
    catch(e)
    {
        logException(e);
    }
}



function contextMenuShowing(info)
{
    /*  The info trigger ID tells us what level we are within. Tell the 
        top-level so that context menu items can be enabled.
    */
    let flags = ["editor"];

    /*  Find the deepest level selected by the context menu or else
        return the currently selected levels. The priority is deliberately
        given to the context menu.
    */
    let levels = contextMenuLevels(info);

    if (levels.length > 0)
    {
        let leader = levels[0];

        switch (leader.kind)
        {
        case GROUP_KIND:
            flags.push("group");
            flags.push("sortGroups");
            if (leader.validated) flags.push("validGroup");
            break;

        case PART_KIND:
            flags.push("group");
            flags.push("part");
            flags.push("sortParts");
            if (leader.validated) flags.push("validPart");
            break;

        case SELECT_KIND:
            flags.push("group");
            flags.push("part");
            flags.push("selector");
            break;
        }
    }

    return flags;
}



function groupsChanged(event)
{
    /*  This can be called from the catalog pane in which case this pane is deactivated
        and there is nothing to do. Otherwise any change must have been done outside
        of the editor. See dlgMain.groupConfigsChanged().
    */
    if (theModel && event.agent !== kUtils.AGENT_EDITOR)
    {
        event.changed.forEach((g) => theModel.importGroup(g));
    }
}



function saveGroup(group)
{
    /*  Save just the one group to the database. It is about
        to be updated. This must be unconditional in order to
        trigger further events above.
        
        Probably there is always a model but we cope in case 
        there isn't in some future version.
    */

    if (theModel)
    {
        theModel.saveGroup(group);
    }
    else
    {
        // Always indicate completion.
        saveConfigs([], []);
    }
}


//======================================================================
// The Model


function Model(data)
{
    let {configs, ui} = data;
    //debug("configsToPage", showObj(configs));

    this.ui = ui || this.defaultUIState();

    /*  The store configs is an object indexed on the uuid for easy
        update. There is a hint about the preferred order in the ui
        object.
    */
    let groups  = [];
    let ordered = {};

    let anon = configs[ANON_GROUP_UUID];

    if (anon)
    {
        // It should always be there
        groups.push(anon);
        ordered[anon.uuid] = true;
    }

    // Use the ordering info that we have
    this.ui.groupOrder.forEach((uuid) =>
    {
        let g = configs[uuid];
        if (g && !ordered[g.uuid])
        {
            groups.push(g);
            ordered[uuid] = true;
        }
    });

    // Any unordered groups will be sorted into alphabetical order and appended.
    let unordered = [];

    for (let id in configs)
    {
        let g = configs[id];

        if (!ordered[g.uuid])
        {
            unordered.push(g);
        }
    }

    unordered.sort((x, y) => x.name.localeCompare(y.name));
    groups = groups.concat(unordered);

    this.initLevel(null, MODEL_KIND,
        {
            required: false,
            invalidMessage: ""
        });

    /*  This does bulk creation of the model tree before
        realising the levels in the UI. This is a bit irregular compared
        with the use of insertChild() later.
    */
    let self = this;
    this.children = groups.map(g => new Group(g, self));
    this.realise();

    this.initialOpened(this.ui.opened);
}


// Inherit from Level
Model.prototype = new Level();
Model.prototype.constructor = Model;
Model.prototype.superClass  = Level;



Model.prototype.realise = function()
{
    /*  Create UI elements for the entire configuration info.
    */
    let gv = kDOM.findId(MODEL_ROOT_ID);
    kDOM.clearDOMChildren(gv);
    //debug("realise model #children", gv.childNodes.length, this.children.length);

    /*  It's a bit faster to realise the DOM nodes without being attached to the DOM.
        BEWARE - document.getElementById() won't work until a node is attached to the DOM.
    */
    this.childContainer = kDOM.create("div");
    this.realiseChildren();
    gv.appendChild(this.childContainer);

    // Leave something with focus
    if (this.children.length > 0)
    {
        this.children[0].focus();
    }

    /*  Save a copy of the saveable data in each group. This is the
        easiest way to reliably tell if there has been a change that
        needs saving.  See saveAll() below.

        Note that unloadConfigs() reads from elements so this can only
        be done after realising. REVISIT?
    */
    this.savedConfigs = this.unloadConfigs();
}



Model.prototype.importGroup = function(groupConfig, existing)
{
    /*  This is used by the Import Group command in which case there is
        no existing context. If there is no existing group then try to
        place the new group in alphabetical order.

        The group may also be a replacement for an existing group in the
        case of the configs database begin updated elsewhere. This will
        typically happen when a new kill is installed in the anon group.

        This is not undoable!! We don't have a way to revert the database
        after importing a group.  REVISIT - should we clear the Undo list?

    */
    debug("importGroup config", showObj(groupConfig));

    let oldGroup = this.findGroup(groupConfig.uuid);
    let opened   = false;

    if (oldGroup)
    {
        opened   = oldGroup.isOpen();
        existing = oldGroup.position();
        oldGroup.removeFromParent();
    }

    let group = new Group(groupConfig, this);

    group.initialOpened(this.ui.opened);
    this.insertChild(group, new RestoreInfo(existing));

    // Put up any error messages in case the groupConfig is still invalid
    theModel.checkValidity();
}



Model.prototype.removeGroup = function(group)
{
    /*  This is a low-level function to remove the group from the model
        but leave it unchanged.  Refuse to remove the anonymous group.

        REVISIT - this shouldn't be needed
    */
    if (group.uuid !== ANON_GROUP_UUID)
    {
        group.removeFromParent();
    }
}



Model.prototype.findGroup = function(uuid)
{
    for (let i = 0; i < this.children.length; ++i)
    {
        let g = this.children[i];

        if (g.uuid === uuid)
        {
            return g;
        }
    }

    return null;
}



Model.prototype.prepareSort = function()
{
    /*  The context menu is over a group. Sort the groups
        but never move the unsorted group. 

        Return the Sorter object that records the permutation.
        This is used to undo the sort.
    */
    function extractor(a)
    {
        return {uuid: a.uuid, text: a.getTitle().toLowerCase()};
    }

    function comparator(a, b)
    {
        if (a.uuid === ANON_GROUP_UUID)
        {
            return -1;
        }
        else
        if (b.uuid === ANON_GROUP_UUID)
        {
            return +1;
        }

        return a.text.localeCompare(b.text);
    }

    return new kUtils.Sorter(this.children, extractor, comparator);
}


Model.prototype.checkAndSave = function()
{
    /*  Check the validity of the form. If it is valid then save the
        result (but don't clear the undo history).  If it is not valid
        then put up error messages.

        This will be called at many times to put up initial messages
        for required fields that need to be filled in.

        We must have this.savedConfigs() be defined before calling this
        the first time.
    */
    debug("checkAndSave");
    this.checkValidity();
    this.saveAll();
}



Model.prototype.checkValidity = function()
{
    /*  Put up some indication of the problems in an invalid group.
        This could happen when reloading a group that is not yet
        valid.

        REVISIT - on a blur, only check within the part or group to save effort
    */
    clearAllErrorMessages();
    this.setValidated(true);

    let gv     = kDOM.findId(MODEL_ROOT_ID);
    let fields = kDOM.toArray(gv.getElementsByTagName("INPUT")).
                    concat(kDOM.toArray(gv.getElementsByTagName("TEXTAREA")));

    fields.forEach((field) =>
    {
        if (!checkValidField(field))
        {
            if (kUtils.debugLevel())
            {
                debug("group checkValidity failed", showElem(field), "in", showLevel(this.findLevel(field)));
            }

            // If it's a field in a part then the part becomes disabled.
            let part = this.findLevel(field, PART_KIND);

            if (part)
            {
                part.validated = false;
                part.setEnabled(false);     // get the UI to change
                part.parentLevel().validated = false;
            }
            else
            {
                // Otherwise it might be in a group.
                let group = this.findLevel(field, GROUP_KIND);

                if (group)
                {
                    group.validated = false;
                    group.setEnabled(false);     // get the UI to change
                }
            }
        }
    });
}



Model.prototype.saveAll = function()
{
    debug("saveAll");
    let latest = this.unloadConfigs();

    /*  Save a group found in latest unless it is found unchanged in savedConfigs.
        The comparison skips the lastModified property. See unloadConfigs().
    */
    let skips   = {lastModified: true};
    let changed = [];

    for (let gid in latest)
    {
        let orig = this.savedConfigs[gid];
        let now  = latest[gid];

        if (now.state === kUtils.STATE_IMPORTED)
        {
            now.state = kUtils.STATE_MODIFIED;
        }

        now.lastModified = Date.now();

        let skip = (orig && kUtils.sameJSON(orig, now, skips));

        if (!skip)
        {
            if (false && kUtils.debugLevel())
            {
                debug("saveAll mismatch", 
                    JSON.stringify(orig, undefined, 4),
                    JSON.stringify(now, undefined, 4));
            }
            changed.push(now);
        }
    }
    debug("saveAll changed length =", changed.length);

    // See what original valid groups have been removed.
    let removed = [];

    for (let gid in this.savedConfigs)
    {
        if (!latest[gid])
        {
            removed.push(gid);
        }
    }

    if (changed.length > 0 || removed.length > 0)
    {
        saveConfigs(changed, removed);
    }

    this.savedConfigs = latest;
    this.saveUIState();
}



Model.prototype.saveGroup = function(uuid)
{
    /*  Save just the one group to the database. It is about
        to be updated. This must be unconditional in order to
        trigger further events above.
    */
    debug("saveGroup", uuid);

    // Preserve the UI state in case we are replacing the group. See importGroup().
    this.saveUIState();

    let g = this.findGroup(uuid);

    if (g)
    {
        let info = g.unloadConfigs();
        saveConfigs([info], []);
    }
    else
    {
        // Just in case we no longer have the group.
        saveConfigs([], []);
    }
}



Model.prototype.defaultUIState = function()
{
    let ui = {
        version:    1,
        opened:     {},
        groupOrder: []
        };

    ui.opened[ANON_GROUP_UUID] = true;
    return ui;
}



Model.prototype.saveUIState = function()
{
    /*  Save info about the UI state. Use this even if we only need
        to update this.ui.
    */
    this.ui.groupOrder = [];
    this.children.forEach((g) => this.ui.groupOrder.push(g.uuid));

    this.ui.opened = {};
    this.saveOpened(this.ui.opened);
    saveUI(this.ui);
}



Model.prototype.unloadConfigs = function()
{
    /*  Return config data ready to be saved.
    */
    let configs = {};

    this.children.forEach(group =>
    {
        try
        {
            configs[group.uuid] = group.unloadConfigs();
        }
        catch(e)
        {
            logException(e);
        }
    });

    return configs;
}



Model.prototype.testDump = function()
{
    /*  This produces a data structure that a test system can 
        examine to see if the model is correct.
    */
    let result = {
        groups: this.children.map(group => group.testDump())
        };

    self.port.emit("testDump", result);
}



Model.prototype.move = function(sources, target)
{
    /*  Move the source levels to the target. The target will be 
        interpreted as a container or a reference level depending
        on its kind. This implements the drag and drop motion.

        The sources array must not be empty. They do not need to be
        siblings. Typically they come from findSelectedLevels().

        Return true if the move was effected.
    */
    let done = false;

    if (sources.length === 0 || sources.indexOf(target) >= 0)
    {
        /*  We are trying to drop the group onto itself.
            The target must not be in the sources.
        */
        return false;
    }

    let leader = sources[0];

    switch (leader.kind)
    {
    case SELECT_KIND:
        switch (target.kind)
        {
        case SELECT_KIND:
            // Move the sources to after the target.
            theModel.moveLevels(sources, target.parentLevel(), target);
            done = true;
            break;

        case PART_KIND:
            // Move the sources to the first position in the target part
            theModel.moveLevels(sources, target, 0);
            done = true;
            break;

        case GROUP_KIND:
            // Move it to the first part in the group.
            let part = target.childAt(0);

            if (part)
            {
                theModel.moveLevels(sources, part, 0);
                done = true;
            }
            break;
        }
        break;

    case PART_KIND:
        // Promote XPATH to PART
        target = target.parentByKind(PART_KIND) || target;

        switch (target.kind)
        {
        case PART_KIND:
            // Move the sources to after the target.
            theModel.moveLevels(sources, target.parentLevel(), target);
            done = true;
            break;

        case GROUP_KIND:
            // Move the sources to the first position in the target group
            theModel.moveLevels(sources, target, 0);
            done = true;
            break;
        }
        break;

    case GROUP_KIND:
        target = target.parentByKind(GROUP_KIND) || target;

        switch (target.kind)
        {
        case GROUP_KIND:
            // Move the sources to after the target.
            theModel.moveLevels(sources, target.parentLevel(), target);
            done = true;
            break;
        }
        break;
    }

    return done;
}



Model.prototype.moveLevels = function(levels, dest, position)
{
    /*  The supplied position is either 0 or an existing level within dest.
        If an existing level it is guaranteed not to be in the levels array.
    */
    levels.forEach((l) => l.removeFromParent());
    dest.insertBunch(levels, position);
}



Model.prototype.moveRestore = function(sources, restoreInfos)
{
    /*  This is to undo a call to move() above. restoreInfos is a parallel
        array of info of where to restore the sources to. The sources are
        sibling levels in the model.
    */
    assert(sources.length === restoreInfos.length);

    sources.forEach((l) => l.removeFromParent());
    insertChildren(sources, restoreInfos);
}



Model.prototype.findLevel = function(elem, withKind)
{
    // Find the model object for a DOM element.
    //debug("Model findLevel", showElem(elem));

    /*  Scan up to the element that has the LEVEL_ATTR attribute.
        If withKind is supplied then keep going up until the
        level element with the given kind is found.

        If the elem already satisfies the criterion then it will be
        returned. This allows us to use this function to search upwards
        for a given class.
    */
    //debug("findLevel elem", showElem(elem), withKind);
    let found = false;

    while (elem && elem.tagName !== "HTML")
    {
        // Text elements don't have the hasAttribute() function.
        //debug("findLevel elem", isLevelElem(elem), showElem(elem));

        if (isLevelElem(elem))
        {
            if (!withKind || elem.classList.contains(withKind))
            {
                found = true;
                break;              // don't go up again
            }
        }

        elem = elem.parentElement;
    }

    if (found)
    {
        /* We have a level element.  Find the model object which has
           elem as its level element. The children here are Group objects.
        */
        return this.children.reduce(
            (level, g) => level || g.findLevel(elem),
            null);
    }

    return null;
}



Model.prototype.findLevelsById = function(ids)
{
    // Find the levels for the ids skipping invalid ids.
    let levels = ids.map((id) => theModel.findLevelById(id));
    return levels.filter((l) => l != null);
}



Model.prototype.findLevelById = function(id)
{
    // Find a level given an id returned by Level.levelId().
    return this.findLevel(kDOM.findId(id));
}



Model.prototype.findSelectedLevels = function()
{
    /*  Find all selected levels. We insist that all of the selected
        levels be of the same kind as anything else would be weird when
        dragging or cutting. They don't have to be siblings.

        The selected levels are always in the same order they appear in
        the children array.
    */
    let found = [];

    this.children.forEach((g) => g.addSelectedLevels(found));

    if (found.length > 1)
    {
        // In case of bugs, filter out those that don't match the leader.
        let leader = found[0];
        found = found.filter((l) => l.kind === leader.kind);
    }

    return found;
}



Model.prototype.selectLevel = function(level, makeVisible)
{
    //  This leaves only one level selected.
    this.unselectAll();

    if (level.kind === MODEL_KIND)
    {
        if (level.children)
        {
            // Select the first group
            level = level.children[0];
        }
        else
        {
            level = null;
        }
    }

    if (level)
    {
        level.select(makeVisible);
    }
}



Model.prototype.selectMulti = function(level, mode)
{
    /*  This is called from a mouse click so the level should already
        be visible. We only select by a direct click on the selectable
        element in a level so level will never be MODEL_KIND.

        We insist that the selected levels have the same kind but they
        can have different parents.
    */

    if (mode === SELECT_SINGLE)
    {
        this.selectLevel(level);
    }
    else
    if (level)
    {
        let current = this.findSelectedLevels();
        let leader  = (current.length > 0) ? current[0] : null;

        if (!leader)
        {
            // nothing is selected yet.
            this.selectLevel(level);
        }
        else
        if (leader.kind === level.kind)
        {
            if (mode === SELECT_MULTI)
            {
                level.selectToggle();
            }
            else
            {
                // Select all elements from this level back to one in the current set.
                let siblings = level.parentLevel().children;
                let last     = null;
                let ix       = 0;

                // Search forwards for the last previous selected level.
                for (; ix < siblings.length; ++ix)
                {
                    let s = siblings[ix];

                    if (current.some((c) => c === s))
                    {
                        last = ix;
                    }

                    if (s === level)
                    {
                        break;
                    }
                }

                if (last !== null && ix < siblings.length)
                {
                    // Select the range
                    while (last <= ix)
                    {
                        siblings[last++].select();
                    }
                }
                else
                {
                    // Treat it as a simple selection
                    this.selectLevel(level);
                }
            }
        }
        else
        {
            /*  Without a common parent we can't add it to the selected group.
                There should be an error indication but at the moment we just
                ignore the request.
            */
        }
    }
}


//======================================================================


function Group(arg, upper)
{
    /*  The argument must be a configuration object that describes a group.
        This will set up the group and its contained parts.

        If no parts are supplied we create a new one since we are probably
        creating a new group.
    */
    if (arg === null)
    {
        // Initial args when creating a new group
        arg = kUtils.groupSkeleton();
    }

    let params;

    if (arg.uuid === ANON_GROUP_UUID)
    {
        params = {editable:  false, klass: "fixedTitle",};
    }
    else
    {
        params = {editable: true, required: true};
    }

    params.text = arg.name;
    params.invalidMessage = gParams["dlg.error.titleRequired"];

    this.uuid      = arg.uuid;
    this.comment   = arg.comment;
    this.email     = arg.contactEMail;
    this.version   = arg.contentVersion;
    this.original  = arg;

    this.initLevel(upper, GROUP_KIND, params);
    this.enabled   = arg.enabled;
    this.validated = arg.validated;

    let self = this;
    this.children = arg.parts.map(part => new Part(part, self));
}


// Inherit from Level
Group.prototype = new Level();
Group.prototype.constructor = Group;
Group.prototype.superClass  = Level;



Group.prototype.removePart = function(part)
{
    /*  Remove the part from the model but leave it unchanged.
        Find another part to select if this is the selected level.
        REVISIT - no longer used
    */
    let sel = part.isSelected();
    let pos = part.position();

    part.removeFromParent();

    if (sel)
    {
        this.selectChildAt(pos);
    }
}



Group.prototype.realise = function(parentElem, index)
{
    //  Create UI elements for the group.
    this.superClass.prototype.realise.call(this, parentElem, index);

    let content = this.levelContent;

    if (this.uuid !== ANON_GROUP_UUID)
    {
        this.createEnabledIcon();

        let table = kDOM.create("table", content);
        let row, col;

        row = kDOM.create("tr", table);
        col = kDOM.create("td", row);
        appendLabel(col, gParams["dlg.groups.version"] + ": ");

        col = kDOM.create("td", row);
        let date = new Date(this.original.lastModified);
        this.versionElem = appendField(col, {
            text:       kUtils.cleanWS(this.version),
            size:       10,
            validation: validVersion,
            afterText:  " (" + date.toLocaleString() + ")"
            });


        row = kDOM.create("tr", table);
        col = kDOM.create("td", row);
        appendLabel(col, gParams["dlg.groups.comment"] + ": ");

        col = kDOM.create("td", row);
        this.commentElem = appendField(col, {
            text:       kUtils.cleanWS(this.comment),
            size:       FIELD_SIZE_BIG,
            rows:       2
            });

        row = kDOM.create("tr", table);
        col = kDOM.create("td", row);
        appendLabel(col, gParams["dlg.groups.email"] + ": ");

        col = kDOM.create("td", row);
        this.emailElem = appendField(col, {
            text:       kUtils.cleanWS(this.email),
            size:       FIELD_SIZE_BIG,
            });

        // Note that indexing counts children wrt the parent container so we need a new one.
        this.childContainer = kDOM.create("div", content);
    }
    else
    {
        this.childContainer = content;
    }

    this.realiseChildren();
}



Group.prototype.prepareSort = function()
{
    /*  The context menu is over a part of a group. Sort the parts.

        Return the Sorter object that records the permutation.
        This is used to undo the sort.
    */
    function extractor(a)
    {
        return a.getTitle().toLowerCase();
    }

    function comparator(a, b)
    {
        return a.localeCompare(b);
    }

    return new kUtils.Sorter(this.children, extractor, comparator);
}



Group.prototype.unloadConfigs = function()
{
    //  Return an object ready to be saved.
    this.unloadDOM();

    let g = kUtils.groupSkeleton(this.uuid);

    // REVISIT state?
    g.state          = this.original.state;
    g.enabled        = this.enabled;
    g.validated      = this.validated;
    g.name           = this.getTitle();
    g.parts          = this.children.map(p => p.unloadConfigs())

    if (this.uuid !== ANON_GROUP_UUID)
    {
        g.comment        = this.comment;
        g.contentVersion = this.contentVersion;
        g.contactEMail   = this.contactEMail;
    }

    //debug("unloaded", showObj(g));
    return g;
}



Group.prototype.unloadDOM = function()
{
    /*  The enabled and validated flags are kept up to date by callbacks.
    */
    this.superClass.prototype.unloadDOM.call(this);

    if (this.uuid !== ANON_GROUP_UUID)
    {
        this.comment        = this.commentElem.value;
        this.contentVersion = this.versionElem.value;
        this.contactEMail   = this.emailElem.value;
    }
}



Group.prototype.testDump = function()
{
    this.unloadDOM();

    return {
        id:         this.levelElem? this.levelElem.id : null,
        uuid:       this.uuid,

        title:      this.getTitle(),
        comment:    this.comment,
        contactEMail:   this.contactEMail,
        contentVersion: this.contentVersion,
        state:      this.state,

        enabled:    this.enabled,
        validated:  this.validated,
        selected:   this.selected,
        open:       this.isOpen(),

        parts:      this.children.map(p => p.testDump())
        };
}


//======================================================================


function Part(arg, group)
{
    /*  The argument must be a configuration object that describes a part in a group.
        If there are no selectors then we are probably creating a new part. So create
        an initial selector too.
    */
    if (arg === null)
    {
        // Initial args when creating a new one
        arg = kUtils.partSkeleton();
    }

    this.uuid        = arg.uuid;
    this.urlPattern  = arg.urlPattern;

    this.initLevel(group, PART_KIND, {
        text:     arg.title,
        required: false,
        size:     FIELD_SIZE_BIG,
        });

    this.enabled     = arg.enabled;
    this.validated   = arg.validated;

    let self = this;

    if (arg.selectors.length > 0)
    {
        this.children = arg.selectors.map(sel => new Selector(sel, self));
    }
    else
    {
        this.children = [new Selector(null, self)];
    }
}



// Inherit from Level
Part.prototype = new Level();
Part.prototype.constructor = Part;
Part.prototype.superClass  = Level;



Part.prototype.realise = function(parentElem, index)
{
    this.superClass.prototype.realise.call(this, parentElem, index);
    let content = this.levelContent;

    this.createEnabledIcon();

    // A table gives a more solid layout even for a single line.
    let table = kDOM.create("table", content);
    let row = kDOM.create("tr", table);

    let col = kDOM.create("td", table);
    let urlLabel = appendLabel(col, gParams["dlg.groups.url"] + ": ");

    col = kDOM.create("td", table);
    this.urlElem = appendField(col, {
        text:           kUtils.myURLtoString(this.urlPattern),
        klass:          "partURL",
        size:           FIELD_SIZE_BIG,
        required:       true,
        validation:     validURL,
        });
    urlLabel.for = this.urlElem.id;

    // Note that indexing counts children wrt the parent container so we need a new one.
    this.childContainer = kDOM.create("div", content);
    this.realiseChildren();
    this.shade();
}



Part.prototype.unloadDOM = function()
{
    /*  The enabled and validated flags are kept up to date by callbacks.
    */
    this.superClass.prototype.unloadDOM.call(this);

    this.urlPattern = kUtils.myURL(this.urlElem.value);
}



Part.prototype.unloadConfigs = function()
{
    // Return an object ready to be saved.
    this.unloadDOM();

    let p = kUtils.partSkeleton(this.uuid);

    p.title       = this.getTitle();
    p.urlPattern  = this.urlPattern;
    p.enabled     = this.enabled;
    p.validated   = this.validated;
    p.selectors   = this.children.map(s => s.unloadConfigs());

    return p;
}



Part.prototype.testDump = function()
{
    this.unloadDOM();

    return {
        id:         this.levelElem? this.levelElem.id : null,
        uuid:       this.uuid,

        title:      this.getTitle(),
        urlPattern: this.urlPattern,

        enabled:    this.enabled,
        validated:  this.validated,
        selected:   this.selected,
        open:       this.isOpen(),

        selectors:  this.children.map(s => s.testDump())
        };
}



Part.prototype.removeSelector = function(selector)
{
    let sel = selector.isSelected();
    let pos = selector.position();

    selector.removeFromParent();
    this.shade();

    if (sel)
    {
        this.selectChildAt(pos);
    }
}



Part.prototype.insertChild = function(selector, restoreInfo)
{
    this.superClass.prototype.insertChild.call(this, selector, restoreInfo);
    this.shade();
}



Part.prototype.shade = function()
{
    // Recompute the shading from scratch for simplicity. We shade the even indexes.
    let parent = this.childContainer;

    for (let ix = 0; ix < parent.childNodes.length; ++ix)
    {
        let odd   = ((ix % 2) === 1);
        let child = parent.childNodes[ix];

        if (odd)
        {
            child.classList.remove("shade");
        }
        else
        {
            child.classList.add("shade");
        }
    }
}


//======================================================================


function Selector(arg, part)
{
    /*  The argument must be a configuration object that describes a part in a group.
    */
    if (arg === null)
    {
        // Initial args when creating a new one
        arg = kUtils.selectorSkeleton();
    }

    this.initLevel(part, SELECT_KIND);

    this.comment     = arg.comment;
    this.selector    = arg.selector;
    this.method      = arg.method

    // UI elements
    this.tableElem    = null;
    this.commentElem  = null;
    this.selectorElem = null;
    this.methodElem   = null;
}



// Inherit from Level
Selector.prototype = new Level();
Selector.prototype.constructor = Selector;
Selector.prototype.superClass  = Level;



Selector.prototype.realise = function(parentElem, index)
{
    /*  index controls shading and is also the child position in the
        parent container. The child position is most important when inserting
        new selectors.
    */
    this.superClass.prototype.realise.call(this, parentElem, index);

    this.tableElem = kDOM.create("table", this.levelContent);

    let row, col, label;

    row   = kDOM.create("tr", this.tableElem);
    col   = kDOM.create("td", row);
    label = appendLabel(col, "Selector:");

    /*  We don't collapse multiple WS chars in an selector in case there
        are quoted strings.
    */
    col = kDOM.create("td", row);
    this.selectorElem = appendField(col, {
        text:           this.selector.trim(),
        klass:          "selectorValue",
        size:           FIELD_SIZE_BIG,
        required:       true,
        validation:     validSelector,
        });
    label.for = this.selectorElem.id;

    // The Selector label is draggable.
    label.setAttribute("draggable", true);

    this.selectTarget = label;
    this.focusTarget  = this.selectorElem;

    row   = kDOM.create("tr", this.tableElem);
    col   = kDOM.create("td", row);
    label = appendLabel(col, gParams["dlg.groups.comment"] + ":");

    col = kDOM.create("td", row);
    this.commentElem = appendField(col, {
        text: kUtils.cleanWS(this.comment),
        klass: "selectorComment",
        size: FIELD_SIZE_BIG,
        });
    label.for = this.commentElem.id;

    row   = kDOM.create("tr", this.tableElem);
    col   = kDOM.create("td", row);
    label = appendLabel(col, gParams["dlg.groups.method"] + ":");

    col = kDOM.create("td", row);
    this.methodElem = appendChoice(col, {
        options: [
            {label: gParams["dlg.groups.hide"],   value: "hide"},
            {label: gParams["dlg.groups.delete"], value: "delete"}
            ],
        initial:    this.method,
        required:   true,
        klass:      "methodValue",
        });
    label.for = this.methodElem.id;
}



Selector.prototype.unloadConfigs = function()
{
    // Return an object ready to be saved.
    this.unloadDOM();

    let x = kUtils.selectorSkeleton();

    x.comment  = this.comment;
    x.selector = this.selector;
    x.method   = this.method;

    return x;
}



Selector.prototype.unloadDOM = function()
{
    this.superClass.prototype.unloadDOM.call(this);

    this.comment  = this.commentElem.value;
    this.selector = this.selectorElem.value;
    this.method   = this.methodElem.value;
}



Selector.prototype.testDump = function()
{
    this.unloadDOM();

    return {
        id:         this.levelElem? this.levelElem.id : null,

        comment:    this.comment,
        selector:   this.selector,
        method:     this.method,

        selected:   this.selected,
        open:       this.isOpen(),
        };
}

//======================================================================

/*  If a level is openable then its containing structure is

    <div class="openableLevel">
        <span class="openableBox"><img src="open_level.png"></span>
        <div class="openableContainer">
            ... title ...
            <div class="levelContent" data-open-level="true">
            </div>
        </div>
    </div>

    with styles

    .openableLevel {
        position: relative;
        }

    .openableBox {
        position: absolute;
        }

    .openableContainer {
        margin-left: 20px;
        }

    The span will make the icon be placed within the span on the baseline
    according to the following text. The span itself has top=left=0.

    Otherwise it is

    <div class="levelContainer">
        ... title ...
        <div class="levelContent">
        </div>
    </div>


*/

function Level()
{
    // A child can be detached from the model in which case upper is null.
    this.upper      = null;
    this.kind       = null;
    this.selected   = false;    // never true for the MODEL level
    this.wantOpen   = false;    // remembers the prefered state when out of tree
    this.openable   = false;
    this.opened     = false;    // never true for the MODEL level

    /*  These are passed to appendField.
        Note that gParams is not defined at the time this constructor is called.
        Subclasses must specify the invalidMessage explicitly.
    */
    this.titleDefaults = {
        text:       "",
        size:       20,
        required:   true,
        editable:   true,
        invalidMessage: "The title is required"
        },

    // The child levels are held here.
    this.children = [];
    this.childContainer = null; // a parent element for realising the children
}



Level.prototype.initLevel = function(parentLevel, kind, titleParams)
{
    /*  This is functionality that is common to levels.
        It includes the DOM stuff for the level button and title.

        The Model level is treated specially. It is the root and does
        not appear in the DOM.
    */
    this.upper      = parentLevel;
    this.kind       = kind;
    this.titleLabel = "";
    this.titleShow  = "";
    this.enabled    = false;    // This can be controlled by an enable icon.
    this.validated  = true;     // define it for type safety

    this.title = {};
    kUtils.copyObj(this.title, this.titleDefaults);
    kUtils.copyObj(this.title, titleParams || {});

    //debug("initLevel defaults", kind, showObj(this.titleDefaults));
    //debug("initLevel params", kind, showObj(titleParams));
    //debug("initLevel", kind, showObj(this.title));

    let lbl;

    switch (kind)
    {
    case GROUP_KIND:
        lbl = gParams["dlg.groups.group"];
        this.titleLabel = lbl + ": ";
        this.titleShow  = lbl + ": " + this.title.text;
        this.openable    = true;
        break;

    case PART_KIND:
        lbl = gParams["dlg.groups.part"];
        this.titleLabel = lbl + ": ";
        this.titleShow  = lbl + ": " + this.title.text;
        this.openable    = true;
        break;

    case SELECT_KIND:
        this.titleLabel = "";           // no titleElem will be created
        this.titleShow  = "Selector";
        break;
    }

    this.levelElem = null;
}



Level.prototype.initialOpened = function(map)
{
    if (this.uuid && map[this.uuid])
    {
        this.setOpen(true);
    }

    this.children.forEach((c) => c.initialOpened(map));
}



Level.prototype.saveOpened = function(map)
{
    /*  Save the opened flag for each level so that we can preserve
        over runs of the dialog.
    */
    if (this.uuid)
    {
        map[this.uuid] = this.opened;
    }

    this.children.forEach((c) => c.saveOpened(map));
}



Level.prototype.insertBunch = function(newChildren, existing)
{
    /*  Insert an array of children together as a bunch after the
        existing position (which is numeric or an element or null).
    */
    for (let i = 0; i < newChildren.length; ++i)
    {
        let child = newChildren[i];
        this.insertChild(child, new RestoreInfo(existing));
        existing = child;
    }
}



function insertChildren(newChildren, restoreInfos)
{
    /*  Insert an array of children. restoreInfos is a parallel array
        of extra info. The children can go into different parents.

        Typically the positions in restoreInfos were saved as a snapshot
        before they were removed from somewhere.  The children must be
        inserted into increasing order of thisposition in any level to
        end up back in the right position.
    */
    for (let i = 0; i < newChildren.length; ++i)
    {
        let rinfo = restoreInfos[i];
        rinfo.parentLevel.insertChild(newChildren[i], rinfo);
    }
}



Level.prototype.insertChild = function(newChild, restoreInfo)
{
    /*  Insert a level as a child of this level.

        The restoreInfo provides information on where to put it.

        If the restoreInfo position is a number then use it as the
        index for the child. The position can be beyond either end of
        the children array to insert at that end.  It would typically
        be the result from level.position().

        If the position is a level that is an existing child of this
        level then place the new child after it.

        Otherwise (with position = null) we put the new child at the end.

        The new child may or may not be a part of the model. We will
        force the upper property. Let the caller beware.

        The new child will be realised in the UI.

        REVISIT - it might be useful to have a "realised" flag so that
        insertChild can be used in both cases.

    */
    assert(restoreInfo instanceof RestoreInfo);
    assert(newChild.kind === childKind[this.kind], "insertChild - wrong kind");

    //debug("insertChild rinfo", restoreInfo.toString());

    let nc  = this.children.length;
    let ix  = nc;
    let pos = restoreInfo.position;

    if (typeof pos === "number")
    {
        ix = pos;
        ix = (ix < 0)? 0 : ((ix > nc)? nc : ix);
    }
    else
    if (pos instanceof Level)
    {
        let j = this.childIndex(pos);

        if (j >= 0)
        {
            ix = j + 1;
        }
    }

    // The newChild ends up at the index ix.
    this.children.splice(ix, 0, newChild);
    newChild.upper = this;
    newChild.realise(ix);

    if (restoreInfo.selected)
    {
        newChild.select();
    }
}



Level.prototype.removeFromParent = function()
{
    //  This will unrealise the level and remove it from the parent and the model.
    let ix = this.position();

    if (ix >= 0)
    {
        this.unrealise();
        this.upper.children.splice(ix, 1);
        this.upper = null;
    }
}



Level.prototype.position = function()
{
    //  Return the position of this level in the parent's child list.
    return this.upper.childIndex(this);
}



Level.prototype.getRestoreInfo = function()
{
    // Make some info to restore the level its current place.
    let r = new RestoreInfo(this.position());

    r.selected    = this.selected;
    r.parentLevel = this.parentLevel();
    return r;
}



Level.prototype.show = function()
{
    let a = [];

    if (this.upper)
    {
        let s = this.upper.show();
        if (s) a.push(s);
    }

    a.push(this.titleShow);
    return a.join(", ");
}



Level.prototype.parentLevel = function()
{
    /*  The result for the model level is null. We
        return a parent for a group so that we can find a sibling.
    */
    return this.upper;
}



Level.prototype.parentByKind = function(kind)
{
    /*  Keep going up until a parent of the given kind is found.
    */
    let level = this;

    while (level && level.kind !== kind)
    {
        level = level.upper;
    }

    return level;
}



Level.prototype.childIndex = function(elem)
{
    /*  If the element is a child of this level return its
        position in the children array, else -1.
    */
    for (let i = 0; i < this.children.length; ++i)
    {
        if (this.children[i] === elem)
        {
            return i;
        }
    }

    return -1;
}



Level.prototype.realiseChildren = function()
{
    this.children.forEach((c, ix) => c.realise(ix));
}



Level.prototype.realise = function(index)
{
    /*  The index is the index of this level in the parent's list of
        children.  It can be undefined.  If we have an index then the
        new level element will be given that position among its siblings.

        We define:

        this.levelElem  - the root of this level's UI. This is the child node
                          of the parent level's this.childContainer.
        this.levelIcon  - the img element for openable levels

        this.levelContainer - the container for the title and content
                              This has a block layout.
        this.levelContent   - the container for the content

        this.titleContainer - the container with the title field. This has
                              a flex layout.

        this.selectTarget   - the element to highlight when selected
        this.focusTarget    - the element to focus on when selected

        Also there is:
        this.childContainer - created by the derived class

        We make the title label draggable. This will be highlighted in the CSS.
        The subclass will have to make something else draggable for Selectors.
    */
    let self       = this;
    let parentElem = this.upper.childContainer;

    this.selectTarget = null;
    this.selected     = false;
    this.focusTarget  = null;

    if (this.kind === MODEL_KIND)
    {
        this.levelElem = null;
        this.titleElem = null;
        return;
    }

    //debug("Level realise", this.kind, showElem(parentElem), "index", index);

    /*  We may be restoring a removed level in which case we will
        recreate all UI elements. The wantOpen flag remembers the
        opened state.
    */
    if (this.openable)
    {
        this.opened = false;

        this.levelElem = kDOM.create("div");
        this.levelElem.classList.add(this.kind, "openableLevel");

        let span = kDOM.create("span", this.levelElem);
        span.classList.add("openableBox");

        this.levelIcon = kDOM.create("canvas", span);
        this.levelIcon.classList.add("openIcon");

        this.levelIcon.addEventListener("click",
            (event) => self.iconClick(event), false);

        this.levelContainer = kDOM.create("div", this.levelElem);
        this.levelContainer.classList.add("openableContainer");
    }
    else
    {
        // It is always open if not openable.
        this.opened = true;

        this.levelElem = kDOM.create("div");
        this.levelElem.classList.add(this.kind);
        this.levelContainer = this.levelElem;
    }

    this.levelElem.setAttribute(LEVEL_ATTR, true);

    if (typeof index === "number" && index >= 0 && index < parentElem.childNodes.length)
    {
        let refElem = parentElem.childNodes[index];
        parentElem.insertBefore(this.levelElem, refElem);
    }
    else
    {
        parentElem.appendChild(this.levelElem);
    }

    /*  If only a titleLabel is supplied then the title element is a
        label element. We put this all into a DIV so that addErrorMessage()
        will work properly.
    */
    if (this.titleLabel)
    {
        this.titleContainer = kDOM.create("div", this.levelContainer);
        this.titleContainer.style.display = "flex";
        this.titleContainer.style["flex-direction"] = "row";

        let label = kDOM.create("label", this.titleContainer);
        label.classList.add("titleLabel");
        kDOM.appendText(label, this.titleLabel);
        label.setAttribute("draggable", true);

        if (typeof this.title.text === "string")
        {
            this.titleElem = appendField(this.titleContainer, this.title);
            label.for = this.titleElem.id;
        }
        else
        {
            this.titleElem = label;
        }

        this.titleElem.classList.add("levelTitle");

        if (this.title.klass)
        {
            this.titleElem.classList.add(this.title.klass);
        }

        this.selectTarget = label;
        this.focusTarget  = this.titleElem;
    }

    this.levelContent = kDOM.create("div", this.levelContainer);
    this.setOpen(this.wantOpen);
}



Level.prototype.unrealise = function()
{
    /*  Delete the level UI elements from the DOM. Often
        that is all that is needed since all parts of the level
        should be children of levelContent.
    */
    if (this.levelElem)
    {
        this.unloadDOM();

        let parent = this.levelElem.parentNode;
        parent.removeChild(this.levelElem);

        this.levelElem    = null;
        this.levelIcon    = null;
        this.levelContent = null;
        this.selectTarget = null;
    }
}



Level.prototype.unloadDOM = function()
{
    /*  Get information out of the DOM before unrealising.
        Subclasses should extend this.
    */
    this.getTitle();
}



Level.prototype.isRealised = function()
{
    return !!this.levelElem;
}



Level.prototype.sortChildren = function(sorter, forwards)
{
    /*  Sort the children of this level. If the children are realised
        then update the DOM.
    */
    sorter.apply(this.children, forwards);

    if (this.childContainer.firstChild)
    {
        while (this.childContainer.firstChild)
        {
            this.childContainer.removeChild(this.childContainer.firstChild);
        }

        this.children.forEach((c) => this.childContainer.appendChild(c.levelElem));
    }
}



Level.prototype.getTitle = function()
{
    // REVISIT calls to this probably just want to do unloadDOM() and then read this.title.text
    if (this.title.editable && this.titleElem)
    {
        this.title.text = this.titleElem.value;
    }

    return this.title.text;
}



Level.prototype.setTitle = function(title)
{
    this.title.text = title;

    if (this.title.editable && this.titleElem)
    {
        this.titleElem.value = title;
    }
}



Level.prototype.createEnabledIcon = function()
{
    /*  Put a clickable icon at the end of the title to show if it is enabled.
        This operates the this.enabled flag. It must be called after Level.realise().
    */
    let self = this;

    this.enabledElem = kDOM.create("img", this.titleContainer);
    this.enabledElem.classList.add("enabledIcon");
    this.setEnabled(this.enabled);

    this.enabledElem.addEventListener("click", (ev) =>
    {
        self.setEnabled(!self.enabled);
    });
}



Level.prototype.setValidated = function(flag)
{
    // Pass the flag down through the model.
    this.validated = flag;
    this.children.forEach((c) => c.setValidated(flag));
}



Level.prototype.canEnable = function()
{
    // Override this.
    return this.validated;
}



Level.prototype.setEnabled = function(flag)
{
    //  Set the variable and icon.
    flag = flag && this.canEnable();
    assert(typeof flag === "boolean", "setEnabled - bad flag");

    if (this.enabledElem)
    {
        this.enabledElem.src   = flag? gParams.enabledIcon : gParams.disabledIcon;
        this.enabledElem.title = flag? gParams["dlg.enabledTip"] : gParams["dlg.disabledTip"];
    }
    this.enabled = flag;
}



Level.prototype.iconClick = function(event)
{
    // When called 'this' is the Level object.
    this.swapOpen();

    // Erase the selection that appears when clicking on a LI
    //clearGuiSelection();
}



Level.prototype.childAt = function(ix)
{
    if (ix >= 0 && ix < this.children.length)
    {
        return this.children[ix];
    }

    return null;
}



Level.prototype.sibling = function(forwards)
{
    /*  Return the next sibling of this level in the parent's list
        of children. If there isn't one then return null. We don't cycle
        around here. The forwards flag selects the next or previous one.
    */
    let found = null;

    if (this.upper)
    {
        let cs = this.upper.children;

        for (let i = 0; i < cs.length; ++i)
        {
            if (cs[i] === this)
            {
                if (forwards && i < cs.length - 1)
                {
                    found = cs[i+1];
                }
                else
                if (!forwards && i > 0)
                {
                    found = cs[i-1];
                }
                break;
            }
        }
    }

    return found;
}



Level.prototype.levelId = function()
{
    // Return a unique id for this level. See Model.findLevelById().
    return this.levelElem.id;
}



Level.prototype.findLevel = function(elem)
{
    // Find the model object which has elem as its level element.
    // Return null if it couldn't be found.

    //debug("Level findLevel", showElem(elem));
    if (this.matchesLevel(elem))
    {
        //debug("Level findLevel matches");
        return this;
    }

    return this.children.reduce(
        function(level, c) {return level || c.findLevel(elem);},
        null);
}



Level.prototype.matchesLevel = function(elem)
{
    //debug("LevelUI matches", showElem(elem), showElem(this.levelElem));
    return elem === this.levelElem;
}



Level.prototype.swapOpen = function()
{
    this.setOpen(!this.isOpen());
}



Level.prototype.setOpenAll = function(flag)
{
    this.setOpen(flag);
    this.children.forEach(c => c.setOpenAll(flag));
}



Level.prototype.setOpen = function(flag)
{
    if (this.levelIcon)
    {
        //this.levelIcon.setAttribute("src", flag? OPEN_ICON : CLOSED_ICON);
        kDOM.drawOpenClosed(this.levelIcon, flag);
        this.levelContent.setAttribute(OPEN_ATTR, flag);
    }

    this.wantOpen = flag;
    this.opened   = flag;
}



Level.prototype.isOpen = function()
{
    return this.opened;
}



Level.prototype.isOpenable = function()
{
    return this.openable;
}



Level.prototype.unselectAll = function()
{
    // Unselect this level and all lower ones.
    this.unselect();
    this.children.forEach(c => c.unselectAll());
}



Level.prototype.selectChildren = function()
{
    //  Select just the children of this level
    this.children.forEach((c) => c.select());
}



Level.prototype.selectChildAt = function(index)
{
    // Ensure there is a child selected if possible.
    if (this.children.length > 0)
    {
        if (index < 0)
        {
            index = 0;
        }
        else
        if (index >= this.children.length)
        {
            index = this.children.length - 1;
        }

        theModel.selectLevel(this.children[index]);
    }
}



Level.prototype.selectToggle = function()
{
    this.isSelected()? this.unselect() : this.select();
}



Level.prototype.select = function(makeVisible)
{
    /*  Select this level. Other levels should be unselected before
        coming here.  If makeVisible is false then we were selected by
        a mouse click so we don't want any more scrolling.
    */
    //debug("Level.select", showLevel(this), this.selected, this.selectTarget);

    if (!this.selected && this.kind !== MODEL_KIND)
    {
        this.selected = true;          // set this flag before calling elsewhere

        if (this.selectTarget)
        {
            this.selectTarget.setAttribute(SEL_ATTR, true);
            this.focus();

            if (makeVisible)
            {
                this.ensureVisible(this.selectTarget);
            }
        }
    }
}



Level.prototype.unselect = function()
{
    //  Unselect this level.
    if (this.selectTarget && this.selected)
    {
        this.selectTarget.removeAttribute(SEL_ATTR);
        this.selected = false;
    }
}



Level.prototype.isSelected = function()
{
    return this.kind !== MODEL_KIND && this.selected;
}



Level.prototype.addSelectedLevels = function(found)
{
    if (this.isSelected())
    {
        found.push(this);
    }

    // Add the child objects that are currently selected.
    return this.children.forEach((c) => c.addSelectedLevels(found));
}



Level.prototype.findSelectedLevel = function()
{
    // Return the first selected level we can find.
    if (this.isSelected())
    {
        return this;
    }

    // Find the child object that is currently selected.
    return this.children.reduce((found, l) => found || l.findSelectedLevel(), null);
}



Level.prototype.focus = function()
{
    if (this.focusTarget)
    {
        this.focusTarget.focus();
    }
}



Level.prototype.ensureVisible = function(elem)
{
    //  Ensure that all higher levels are open.
    //debug("Level.ensureVisible", showElem(elem));
    let level = this;

    do
    {
        // Always go up one first.
        level = level.parentLevel();

        if (level)
        {
            level.setOpen(true);
        }
    }
    while (level);

    if (elem)
    {
        elem.scrollIntoView();
    }
}



function RestoreInfo(position)
{
    /*  This is a class for parameters passed to Level.insertChild to
        provide information about position etc during the insert.

        The position argument is either: 
            an existing level - the child is inserted after it
            an integer index  - the child is positioned at this index in the siblings
            null              - the child is inserted after the last sibling
    */
    this.parentLevel = null;
    this.position    = kUtils.have(position)? position : null;
    this.selected    = false;
}



RestoreInfo.prototype.toString = function()
{
    return "position=" + this.position + ", selected=" + this.selected;
}

//======================================================================
// Commands


function doSave(info)
{
    theModel.checkAndSave();
}



function doClose(info)
{
    // REVISIT - not useful
    requestClose();
}



function doExpandAll(info)
{
    // Find the current level and swap the open state for all below it.
    // REVISIT - after collapsing move the selection to this level.
    let level = contextTarget(info);

    if (level)
    {
        level.setOpenAll(!level.isOpen());
    }
}




function doExportGroup(info)
{
    let group = contextTarget(info, GROUP_KIND);

    if (group)
    {
        let configs = group.unloadConfigs();
        exportGroup(configs);
    }
}



function contextTarget(info, withKind)
{
    /*  Find the model object we are to deal with. If the event info
        doesn't tell us (from the context menu) then try starting with the
        currently selected level and going up to a level of the given kind.

        The result will be a Level object.
    */
    //debug("contextTarget info", showObj(info));
    let level = contextLevel(info, withKind);

    if (!level)
    {
        level = theModel.findSelectedLevel();
        //debug("contextTarget selected level", showLevel(level));

        if (level && withKind)
        {
            level = level.parentByKind(withKind);
            //debug("contextTarget selected level with kind", showLevel(level));
        }
    }

    return level;
}



function contextLevel(info, withKind)
{
    /*  Find the model object of the appropriate kind that contains
        the event.  If the event info doesn't tell us (from the context
        menu) then return null. The result is always of the required
        kind or else null.

        We need the triggerId field in info. This requires that all
        elements in the model have an id.
        a selector.
    */
    //debug("contextLevel info", showObj(info));
    let level = null;

    if (info.triggerId)
    {
        let trigger = kDOM.findId(info.triggerId);
        //debug("contextLevel trigger", showElem(trigger));
        level = theModel.findLevel(trigger, withKind);
    }

    return level;
}



function contextMenuLevels(info)
{
    /*  Find the deepest level selected by the context menu or else
        return the currently selected levels. The priority is deliberately
        given to the context menu.
    */
    let levels = contextDeepest(info);

    if (levels.length == 0)
    {
        levels = theModel.findSelectedLevels();
    }

    return levels;
}



function contextDeepest(info)
{
    /*  Find the deepest level that the event is within.
        This uses info.triggerId. We return the level as an array.
    */
    let kinds = [SELECT_KIND, PART_KIND, GROUP_KIND];

    for (let k in kinds)
    {
        let level = contextLevel(info, kinds[k]);

        if (level)
        {
            return [level];
        }
    }

    return [];
}



function allSelectedOrContext(info)
{
    /*  Find all of the selected levels. If there aren't any then see if the
        context menu is pointing to one and find the deepest. We always
        return an array of levels.
    */
    let levels = theModel.findSelectedLevels();

    if (levels.length === 0)
    {
        levels = contextDeepest(info);
    }

    return levels;
}

//======================================================================
// Editing

/*  Each action is an object with a redo for the forward step and undo
    for the reverse.  The action object will be 'this' for the undo and
    redo functions.
*/
var     undoList = [];          // new values are pushed
var     redoList = [];          // new values are pushed

var     gCopiedLevels = [];     // unloaded data from copied levels
var     gCopiedKind   = null;   // unloaded data from a copied level

// This maps from an element to its the value before the latest input operation.
var     savedValues = new WeakMap();

// This maps from an element to its field parameters. This is used for validation.
var     fieldParams = new WeakMap();


function saveValue(elem)
{
    //  Record the current value of some input element.
    //debug("saveValue", showElem(elem), "saving", getElemValue(elem));
    savedValues.set(elem, getElemValue(elem));
}


function getSavedValue(elem)
{
    //  Return the current value of some input element or undefine.
    return savedValues.get(elem);
}



function clearRedo()
{
    redoList = [];
}



function recordControlChange(elem)
{
    /*  An input element has changed. Record the new value.
        We should have an undo object on the undo list. Update
        it with the element's value.

        REVISIT - undoing a change should restore the status of the group
        so that it can appear unchanged.
    */
    let oldValue = getSavedValue(elem);
    let newValue = getElemValue(elem);

    saveValue(elem);

    undoList.push({
        redo: () => elem.value = newValue,
        undo: () => elem.value = oldValue
        });
}



function newUndoAction(undo)
{
    /*  This installs the new forwards action. This erase anything
        waiting to be redone. It doesn't perform the forward action.
    */
    clearRedo();
    undoList.push(undo);
}



function doUndo()
{
    /*  Perform the action in the 'undo' property of the object.
        This must leave the system in a state where the 'redo' function
        will work again.
    */
    if (undoList.length > 0)
    {
        let action = undoList.pop();
        action.undo();
        redoList.push(action);
    }
}



function doRedo()
{
    if (redoList.length > 0)
    {
        let action = redoList.pop();
        action.redo();
        undoList.push(action);
    }
}



function doNewGroup(info)
{
    /*  Create a new group before or after the existing one and
        realise it.  The existing one may be null.

        This is called from a user event so the model is already realised.
    */
    let existing = contextTarget(info, GROUP_KIND);

    let group = new Group(null, theModel);
    group.wantOpen = true;
    addGroupsWithUndo([group], existing);
    group.focus();
}



function doNewPart(info)
{
    //  Create a new part within the selected group.
    let existing = contextTarget(info, PART_KIND);

    // Maybe we are inside a group with no parts
    let group = existing? existing.parentLevel() :
                          contextTarget(info, GROUP_KIND);
    if (group)
    {
        let part = new Part(null, group);
        part.wantOpen = true;
        addPartsWithUndo([part], existing);
        part.focus();
    }
}



function doNewSelector(info)
{
    /*  Append a new selector to the tree within the selected part.
    */
    let existing = contextTarget(info, SELECT_KIND);

    // Maybe we are inside a part with no selectors
    let part = existing? existing.parentLevel() :
                         contextTarget(info, PART_KIND);
    if (part)
    {
        let selector = new Selector(null, part);
        selector.wantOpen = true;
        addSelectorsWithUndo([selector], existing);
        selector.focus();
    }
}



function addGroupsWithUndo(groups, position)
{
    /*  Add some groups wrt the position.
        This makes an Undo entry.
    */
    if (groups.length > 0)
    {
        let leader = groups[0];
        assert(leader instanceof Group, "addGroupsWithUndo");

        let redo = function()
        {
            // The first part is placed at the given position. The rest follow it.
            theModel.insertBunch(groups, position);
            theModel.checkValidity();
        }

        let undo = function()
        {
            groups.forEach((g) => theModel.removeGroup(g));
            theModel.checkValidity();
        }

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function addPartsWithUndo(parts, position)
{
    /*  This adds the parts to the model keeping them together. They must
        all be siblings. This is for newly generated parts such as when
        doing copy and paste.
    */
    if (parts.length > 0)
    {
        let leader = parts[0];
        let group  = leader.parentLevel();
        assert(leader instanceof Part, "addPartsWithUndo");

        let redo = function()
        {
            group.insertBunch(parts, position);
        }

        let undo = function()
        {
            parts.forEach((p) => p.removeFromParent());
        }

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function addSelectorsWithUndo(selectors, position)
{
    /*  This adds the selectors to the model keeping them together. They
        must all be siblings. This is for newly generated parts such as
        when doing copy and paste.
    */
    let leader = selectors[0];
    let part   = leader.parentLevel();
    assert(leader instanceof Selector, "addSelectorsWithUndo");

    let redo = function()
    {
        part.insertBunch(selectors, position);
    }

    let undo = function()
    {
        selectors.forEach((s) => part.removeSelector(s));
    }

    redo();
    newUndoAction({redo: redo, undo: undo});
}



function doSelectAll(info)
{
    /*  If we are within a field then do the standard select-all so that
        text can be selected. If we are in a level then select all
        siblings.
    */
    if (eventInField(info))
    {
        requestBuiltin("cmd_selectAll");
    }
    else
    {
        let levels = contextDeepest(info);

        if (levels.length > 0)
        {
            let leader = levels[0];     // there should only be one
            theModel.unselectAll();
            leader.parentLevel().selectChildren();
        }
    }
}



function doDeselectAll(info)
{
    if (eventInField(info))
    {
        // There is no builtin deselect for fields.
        try
        {
            window.getSelection().removeAllRanges();
            if (info.triggerId)
            {
                let trigger = kDOM.findId(info.triggerId);
                debug("clearing selection on", showElem(trigger));
                trigger.setSelectionRange(0, 0);
            }
        }
        catch(e)
        {
        }
    }
    else
    {
        theModel.unselectAll();
    }
}



function doCut(info)
{
    /*  If we have selected levels then cut them. Otherwise
        if we are focussed on an element with text that can be deleted
        then dispatch to the built-in cut. Otherwise find the innermost
        level and delete it.

        See also doCopy().
    */
    let levels = theModel.findSelectedLevels();

    if (levels.length > 0)
    {
        doDeleteLevels(levels);
    }
    else
    {
        if (eventInField(info))
        {
            debug("doCopy built-in cut");
            requestBuiltin("cmd_cut");
        }
        else
        {
            doDeleteLevels(contextDeepest(info));
        }
    }
}



function doDelete(info)
{
    doDeleteLevels(allSelectedOrContext(info));
}



function doDeleteLevels(levels)
{
    //  We save the deleted levels as for pasting.
    if (levels.length > 0)
    {
        //debug("doDelete deepest", showLevel(levels[0]));
        doCopyLevels(levels);

        // REVISIT this could be more OO
        switch (levels[0].kind)
        {
        case SELECT_KIND:
            doDeleteSelectors(levels);
            break;

        case PART_KIND:
            doDeleteParts(levels);
            break;

        case GROUP_KIND:
            doDeleteGroups(levels);
            break;
        }
    }
}



function doDeleteGroups(groups)
{
    /*  Usually this is deleting selected groups. The groups
        may be discontiguous.  Each removal changes the position of the
        remaining groups so we need to save the position first.
    */
    // Refuse to remove the anonymous group.
    groups = groups.filter((g) => g.uuid !== ANON_GROUP_UUID);

    if (groups.length > 0)
    {
        let rinfos = groups.map((g) => g.getRestoreInfo());
        let redo   = () => groups.forEach((g) => g.removeFromParent());
        let undo   = () => insertChildren(groups, rinfos);

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function doDeleteParts(parts)
{
    // Record the current status
    if (parts.length > 0)
    {
        let leader = parts[0];
        let group  = leader.parentLevel();

        let rinfos = parts.map((p) => p.getRestoreInfo());
        let redo   = () => parts.forEach((p) => p.removeFromParent());
        let undo   = () => insertChildren(parts, rinfos);

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function doDeleteSelectors(selectors)
{
    if (selectors.length > 0)
    {
        let leader = selectors[0];
        let part   = leader.parentLevel();

        let rinfos = selectors.map((s) => s.getRestoreInfo());
        let redo   = () => selectors.forEach((s) => part.removeSelector(s));
        let undo   = () => insertChildren(selectors, rinfos);

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function doCopy(info)
{
    /*  If we have selected levels then copy them. Otherwise
        if we are focussed on an element with text that can be copied
        then dispatch to the built-in copy. Otherwise find the innermost
        level and copy it. We don't copy to the system.

        See also doDelete() and doCut().
    */
    let levels = theModel.findSelectedLevels();

    if (levels.length > 0)
    {
        doCopyLevels(levels);
    }
    else
    {
        if (eventInField(info))
        {
            //debug("doCopy built-in copy");
            requestBuiltin("cmd_copy");
        }
        else
        {
            //debug("doCopy deepest", showObj(info));
            doCopyLevels(contextDeepest(info));
        }
    }
}



function doCopyLevels(levels)
{
    // We expect that the levels are all of the same kind.
    if (levels.length > 0)
    {
        let leader = levels[0];
        debug("doCopyLevels", showLevel(leader));

        gCopiedLevels = levels.map((l) => l.unloadConfigs());
        gCopiedKind   = leader.kind;
    }
    else
    {
        gCopiedLevels = [];
        gCopiedKind   = null;
    }
}



function doPasteLevel(info)
{
    debug("doPasteLevel copy is", showObj(gCopiedLevels), gCopiedKind);

    if (gCopiedLevels.length === 0)
    {
        // Dispatch to the standard paste. There might be text.
        requestBuiltin("cmd_paste");
        return;
    }

    // Paste wrt where we are.
    let existing = contextLevel(info, gCopiedKind);

    debug("doPasteLevel", showLevel(existing));

    switch (gCopiedKind)
    {
    case GROUP_KIND:
        /*  Remove the group name to avoid duplicates. This
            will make an invalid name which must be fixed by the user.
            Give it a new uuid. It starts off disabled. If there is no
            existing group to paste after then it just goes to the end.
        */
        let groups = gCopiedLevels.map((g) =>
            {
                g.name    = "";
                g.uuid    = kUtils.createUUID();
                g.enabled = false;
                return new Group(g, theModel);
            });

        // Helpfully open the first of the pasted groups
        if (groups.length > 0)
        {
            groups[0].wantOpen = true;
        }

        addGroupsWithUndo(groups, existing)
        break;

    case PART_KIND:
        /*  Give it a new uuid. It starts off disabled.
            We can keep the title the same as we are probably copying
            it to another group.
        */
        let group;

        if (existing)
        {
            group = existing.parentLevel();
        }
        else
        {
            // We might be inside an empty group
            group = contextLevel(info, GROUP_KIND);
        }

        if (group)
        {
            // If we have existing then it must be of gCopiedKind.
            let parts = gCopiedLevels.map((p) =>
                {
                    p.uuid = kUtils.createUUID();
                    return new Part(p, group);
                });

            // Helpfully open the first of the pasted parts
            if (parts.length > 0)
            {
                parts[0].wantOpen = true;
            }

            addPartsWithUndo(parts, existing);
        }
        break;

    case SELECT_KIND:
        let part;

        if (existing)
        {
            part = existing.parentLevel();
        }
        else
        {
            // We might be inside an empty part
            part = contextLevel(info, PART_KIND);
        }

        if (part)
        {
            let sels = gCopiedLevels.map((s) => new Selector(s, part));

            // Helpfully open the first of the pasted selectors
            if (sels.length > 0)
            {
                sels[0].wantOpen = true;
            }

            addSelectorsWithUndo(sels, existing);
        }
        break;
    }
}



function doSortGroups(info)
{
    let sorter = theModel.prepareSort();
    let redo   = () => theModel.sortChildren(sorter, true);
    let undo   = () => theModel.sortChildren(sorter, false);

    redo();
    newUndoAction({redo: redo, undo: undo});
}



function doSortParts(info)
{
    /*  The context menu is over a part. Sort the parts in its group.
    */
    let levels = contextMenuLevels(info);
    let group  = null;

    if (levels.length > 0)
    {
        let leader = levels[0];

        if (leader.kind === GROUP_KIND)
        {
            group = leader;
        }
        else
        if (leader.kind === PART_KIND)
        {
            group = leader.parentLevel();
        }
    }
    //debug("doSortParts group", showLevel(group));

    if (group)
    {
        let sorter = group.prepareSort();
        let redo   = () => group.sortChildren(sorter, true);
        let undo   = () => group.sortChildren(sorter, false);

        redo();
        newUndoAction({redo: redo, undo: undo});
    }
}



function afterInput(event)
{
    let elem = event.target;
    //debug("afterInput", showElem(elem));

    clearRedo();
    recordControlChange(elem);
    checkValidField(elem);
}



function afterBlur(event)
{
    //debug("afterBlur", showElem(event.target));
    theModel.checkValidity();
}



function getElemValue(elem)
{
    let value = null;

    if (elem.nodeName === "INPUT")
    {
        if (elem.type === "checkbox")
        {
            value = elem.checked;
        }
        else
        {
            value = elem.value;
        }
    }
    else
    if (elem.nodeName === "SELECT" || elem.nodeName === "TEXTAREA")
    {
        value = elem.value;
    }

    return value;
}

//======================================================================
// Drag and Drop

/*  The dragged data is a collection of element IDs. We represent them
    as blank-separated within a string. If the dragged level is one of
    the selected ones then all other selected levels are dragged too.

    We expect that all selected elements are siblings of the same kind.
*/

const DRAG_TYPE = "application/x-killit-level";


function dragStart(ev)
{
    let leader = theModel.findLevel(ev.target);

    if (leader)
    {
        let levels;

        if (leader.isSelected())
        {
            levels = theModel.findSelectedLevels();
        }
        else
        {
            levels = [leader];
        }

        let data = levels.map((l) => l.levelId()).join(" ");

        debug("drag start", showElem(ev.target), showLevel(leader), data);

        ev.dataTransfer.setData(DRAG_TYPE, data);
        ev.dataTransfer.effectAllowed = "move";
    }
}



function dragDrop(ev)
{
    let parties = dragParties(ev);

    if (parties !== null)
    {
        let {sources, target} = parties;
        debug("drag dropping onto", showLevel(target));

        let rinfos = sources.map((s) => s.getRestoreInfo());

        if (theModel.move(sources, target))
        {
            // Only accept the drop event if the move was successful.
            ev.preventDefault();
            theModel.checkValidity();

            clearRedo();
            newUndoAction({
                redo: function()
                {
                    theModel.move(sources, target);
                    theModel.checkValidity();
                },

                undo: function()
                {
                    theModel.moveRestore(sources, rinfos);
                    theModel.checkValidity();
                }
            });
        }
    }
}



function dragOver(ev)
{
    //  This gets drag enter and over events.
    if (dragParties(ev) !== null)
    {
        ev.preventDefault();    // accept the drag
    }
}



function dragParties(ev)
{
    /*  Get what kind of level is being dragged and see if we are being
        dragged into another level.
    */
    let data = ev.dataTransfer.getData(DRAG_TYPE);

    if (data)
    {
        let srcIds  = data.split(" ");
        let sources = theModel.findLevelsById(srcIds);

        if (sources.length > 0)
        {
            let target = theModel.findLevel(ev.target);

            if (target)
            {
                //debug("drag over", showLevel(source), "*", showLevel(target));
                return {sources: sources, target: target};
            }
        }
    }

    return null;
}



//======================================================================


function clearGuiSelection()
{
    /*  Now that we do our own selection the browser's selection
        gets in the way.
    */
    window.getSelection().removeAllRanges();
}



function groupEditClick(ev)
{
    let t = ev.target;
    //debug("clicked button", ev.button, ", nodeName", t.nodeName);

    // We only handle the left button here.
    if (ev.button !== 0)
    {
        return;
    }

    if (t.nodeName === "INPUT" && !isLevelTitle(t) ||
        t.nodeName === "SELECT" ||
        t.nodeName === "OPTION" ||
        t.nodeName === "TEXTAREA")
    {
        /*  Allow clicking in table cells and drop-downs without affecting
            the selected level.
        */
    }
    else
    {
        //  Find the enclosing level and select it.
        let level = theModel.findLevel(t);

        if (level)
        {
            let mode = SELECT_SINGLE;

            if (ev.ctrlKey)
            {
                mode = SELECT_MULTI;
            }
            else
            if (ev.shiftKey)
            {
                mode = SELECT_RANGE;
            }

            //debug("groupEditClick: selecting a level");
            theModel.selectMulti(level, mode);
        }
    }
}

//======================================================================


function setEditable(elem, flag)
{
    initEditable(elem, flag);

    if (flag)
    {
        elem.focus();
    }
}



function initEditable(elem, flag)
{
    if (elem.nodeName === "INPUT" || elem.nodeName === "TEXTAREA")
    {
        if (flag)
        {
            elem.removeAttribute("readonly");
        }
        else
        {
            elem.setAttribute("readonly", true);
        }
    }
}


function isLevelTitle(elem)
{
    // REVISIT this is only used in one place
    return elem.classList.contains("levelTitle");
}



function appendField(parent, params)
{
    /*  params:
            text
            size  (chars)
            rows  (number of lines)
            placeholder (string)
            required (bool)
            klass (string)
            editable (bool)
            afterText

            invalidMessage
            validation

        A red asterisk will be placed after the INPUT if it is marked as required.
    */
    if (typeof params.editable === "undefined")
    {
        params.editable = true;
    }

    if (params.editable)
    {
        return appendEditableField(parent, params);
    }
    else
    {
        // Just put the text as a span without limit or validation.
        let field = kDOM.create("span", parent);
        kDOM.appendText(field, params.text);

        if (params.klass)
        {
            field.classList.add(params.klass);
        }

        if (params.afterText)
        {
            // Get this in before the error container so that it is inline
            let span = kDOM.create("span", parent);
            kDOM.appendText(span, params.afterText);
        }

        return field;
    }
}



function appendEditableField(parent, params)
{
    //debug("appendEditableField", showObj(params));
    let field;

    if (params.rows > 1)
    {
        field = kDOM.create("textarea", parent);
        field.rows = params.rows;

        if (params.size)
        {
            // The number of chars to show. This ends up a bit too wide.
            field.cols = params.size - 2;
        }
    }
    else
    {
        // This includes the rows = undefined case
        field = kDOM.create("input", parent);
        field.type = "text";

        if (params.size)
        {
            // The number of chars to show
            field.size = params.size;
        }
    }

    if (params.afterText)
    {
        // Get this in before the error container so that it is inline
        let span = kDOM.create("span", parent);
        kDOM.appendText(span, params.afterText);
    }

    fieldParams.set(field, params);

    if (params.klass)
    {
        field.classList.add(params.klass);
    }

    if (params.text)
    {
        field.value = params.text;
    }

    if (params.size)
    {
        // The number of chars to show
        field.size = params.size;
    }

    if (params.placeholder)
    {
        field.placeholder = params.placeholder;
    }

    if (params.required)
    {
        // A regexp to validate the field value.
        field.setAttribute("required", true);

        if (false)
        {
            // REVISIT - this clutters the UI. The validation is now enough.
            let asterisk = kDOM.create("span", parent);
            kDOM.appendText(asterisk, "\u00a0*")
            asterisk.classList.add("red");
        }
    }

    initFieldEditable(field, params.editable);

    return field;
}



function isTextField(field)
{
    return (field.nodeName === "INPUT" && field.type === "text") ||
           (field.nodeName === "TEXTAREA");
}



function eventInField(info)
{
    // Test if the context menu event is within a field.
    const tags = ["INPUT", "TEXTAREA", "SELECT"];

    return (info.triggerTagName && tags.indexOf(info.triggerTagName) >= 0);
}



function initFieldEditable(field, editable)
{
    field.setAttribute("data-editable", editable);
    initEditable(field, editable);

    if (isTextField(field))
    {
        // Get each key press
        field.addEventListener("input", afterInput, false);
        field.addEventListener("blur",  afterBlur,  false);
    }
    else
    {
        // Get the final value
        field.addEventListener("change", afterInput, false);
        field.addEventListener("blur",  afterBlur,  false);
    }

    saveValue(field);
}



function appendCheckbox(parent, params)
{
    let field = kDOM.create("input", parent);

    field.setAttribute("type", "checkbox");

    if (params.value)
    {
        field.checked = params.value;
    }

    if (params.klass)
    {
        field.className = params.klass;
    }

    initFieldEditable(field, true);

    return field;
}



function appendChoice(parent, params)
{
    let field = kDOM.create("select", parent);

    if (params.multiple)
    {
        field.setAttribute("multiple", true);
    }

    if (params.size && params.size >= 1)
    {
        field.setAttribute("size", params.size);
    }

    params.options.forEach(opt =>
    {
        let label = "Unknown";
        let value = "unknown";

        if (opt instanceof Object)
        {
            label = opt.label;
            value = opt.value;
        }
        else
        if (typeof opt === "string")
        {
            label = opt;
            value = label;
        }

        let option = kDOM.create("option");

        option.setAttribute("value", value);
        kDOM.appendText(option, label);

        if (params.initial && params.initial === value)
        {
            option.setAttribute("selected", true);
        }

        field.appendChild(option);
    });

    if (params.klass)
    {
        field.className = params.klass;
    }

    if (params.required)
    {
        // Can't be blank
        field.setAttribute("required", true);
    }

    initFieldEditable(field, true);
    return field;
}



function appendLabel(parent, text)
{
    let label = kDOM.create("LABEL", parent);
    kDOM.appendText(label, text);
    return label;
}



function checkValidField(field)
{
    let params = fieldParams.get(field);
    let valid  = true;

    if (params)
    {
        if (params.validation)
        {
            valid = params.validation(field, params);
        }
        else
        {
            valid = basicValidation(field, params);
        }

        //if (!valid) debug("checkValidField field", valid, showElem(field));
        //if (!valid) debug("checkValidField params", showObj(params));

        if (valid)
        {
            clearErrorMessage(field);
        }
        else
        if (params.invalidMessage)
        {
            addErrorMessage(field, params.invalidMessage);
        }
    }

    return valid;
}



function validVersion(field, params)
{
    let valid = true;

    if (field.value && !kUtils.validVersion(field.value))
    {
        params.invalidMessage = gParams["dlg.error.badVersion"];
        valid = false;
    }

    return valid;
}



function validURL(field, params)
{
    let url   = kUtils.myURL(field.value);
    let valid = true;

    if (url)
    {
        /*  Check that the parts of the host name are valid.
            Since we allow "*" for the host name we can have as few as
            1 part.
        */
        if (url.host)
        {
            let parts = url.host.split(".");

            if (!parts.every((p) => reUrlPart.test(p)))
            {
                params.invalidMessage = gParams["dlg.error.urlInvalid"];
                valid = false;
            }
        }
    }
    else
    {
        params.invalidMessage = gParams["dlg.error.urlRequired"];
        valid = false;
    }

    return valid;
}



function validSelector(field, params)
{
    // See if there is a syntax error.
    let selector = field.value;
    //debug("validSelector selector=", selector, showElem(field));

    if (selector)
    {
        if (kDOM.isXPath(selector))
        {
            try
            {
                document.evaluate(selector, document.documentElement, null,
                            XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
                return true;
            }
            catch(e)
            {
                //logException(e);
                params.invalidMessage = gParams["dlg.error.xpathSyntax"];
            }
        }
        else
        {
            try
            {
                document.querySelectorAll(selector);
                return true;
            }
            catch(e)
            {
                //logException(e);
                params.invalidMessage = gParams["dlg.error.cssSelectorSyntax"];
            }
        }
    }
    else
    {
        params.invalidMessage = gParams["dlg.error.selectorRequired"];
    }

    return false;
}



function basicValidation(field, params)
{
    let valid = true;

    if (params.required)
    {
        // It must be non-blank
        let value = field.value.trim();

        if (!value)
        {
            if (!params.invalidMessage)
            {
                params.invalidMessage = gParams["dlg.error.required"];
            }

            //debug("basicValidation failed", value, params.invalidMessage);
            valid = false;
        }
    }

    return valid;
}



function addErrorMessage(field, text)
{
    /*  The field should be an INPUT. See appendField().
        Put the message in as a tool-tip for the field.
    */
    debug("addErrorMessage", showElem(field), text);

    field.setCustomValidity(text);
    field.checkValidity();          // this puts up the browser's error marker
    field.classList.add(ERROR_CLASS);
}



function clearErrorMessage(field)
{
    // Remove the error message div contents.
    field.setCustomValidity("");    // remove the error indication
    field.checkValidity();
    field.classList.remove(ERROR_CLASS);
}



function clearAllErrorMessages()
{
    //  Remove all error messages.
    let msgs = document.getElementsByClassName(ERROR_CLASS);

    for (let i = 0; i < msgs.length; ++i)
    {
        clearErrorMessage(msgs[i]);
    }
}



function isLevelElem(elem)
{
    return elem.hasAttribute && elem.hasAttribute(LEVEL_ATTR);
}



function showLevel(level)
{
    if (level)
    {
        return level.show();
    }

    return "null";
}

//======================================================================

return {
    init:               init,
    contextMenuShowing: contextMenuShowing,
    uiCommandHandler:   uiCommandHandler,
    activate:           activate,
    deactivate:         deactivate,
    shutdown:           shutdown,
    };

}());
