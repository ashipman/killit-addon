"use strict";

/*  This is included along-side the dlgMainContent script.
*/

let dlgActivity = (function()
{
"use strict";

const {
    MAX_ACTIVITY,
    debug, debugLevel,
    escapeHtml,
    log, showObj
    } = kUtils;

var gParams = {};

//======================================================================

/*  We expect these globals:

    document - the dlgMain HTML document that has been loaded into the XUL shell.
    window   - for the document
    self     - the Add-On SDK communication link to the outside world.

    We export:

    init
    uiCommandHandler(info)  - return true if the command was handled
    activate                - the pane is now active
    deactivate              - the pane is now not active
    shutdown                - the dialog is being shut down

*/
let isActive = false;           // true if the editor pane is active


function init(params)
{
    gParams  = params;
    isActive = false;

    self.port.on("getActivity", activityToPage);
    self.port.on("newActivity", newActivity);
    self.port.on("activityCleared", activityCleared);
}



function activate()
{
    isActive = true;
    debug("dlgActivity activated");

    // Load the initial preferences.
    self.port.emit("command", "getActivity");
}



function deactivate()
{
    isActive = false;
    debug("dlgActivity deactivated");
}



function shutdown()
{
}


function uiCommandHandler(info)
{
    // Return true if we handled this command.
    return false;
}



function contextMenuShowing(info)
{
    return ["activity"];
}


//======================================================================


function activityToPage(activity)
{
    if (debugLevel())
    {
        debug("activityToPage", showObj(activity));
    }

    if (activity.length === 0)
    {
        showNoActivity();
    }
    else
    {
        let table = startTable();
        activity.forEach((info) => appendEntry(table, info));
    }
}



function showNoActivity()
{
    let root = clearPane();
    kDOM.appendText(root, gParams["dlg.activity.none"]);
}



function clearPane()
{
    let root = kDOM.findId("activityRoot");
    kDOM.clearDOMChildren(root);
    return root;
}



function startTable()
{
    let root = clearPane();
    let table = kDOM.create("table", root);

    let cg = kDOM.create("colgroup", table);
    kDOM.create("col", cg).setAttribute("width", "5%");     // Serial
    kDOM.create("col", cg).setAttribute("width", "95%");    // Text

    return table;
}



function appendEntry(table, entry)
{
    let {serial, groupDescr, partDescr, url, kills} = entry;

    let row = kDOM.create("tr", table);
    let col;

    kDOM.create("td", row).textContent = serial + ":";

    // innerHTML is deprecated
    col = kDOM.create("td", row);
    headingText(col, gParams["dlg.groups.group"]+ ":");
    normalText(col, " " + groupDescr + " ");
    headingText(col, gParams["dlg.groups.part"]+ ":");
    normalText(col, " " + partDescr);

    let lines = [url];

    kills.forEach(({method, selector}) =>
    {
        let selDescr = gParams["dlg.activity." + method] + " " + selector;
        lines.push(selDescr);
    });

    lines.forEach((text) =>
    {
        let row = kDOM.create("tr", table);
        kDOM.create("td", row).textContent = ""; // blank serial col
        kDOM.create("td", row).textContent = text;
    });
}



function headingText(parent, text)
{
    let span = kDOM.create("span", parent);
    span.classList.add("activityHeading");
    span.textContent = text;
    return span;
}



function normalText(parent, text)
{
    let span = kDOM.create("span", parent);
    span.textContent = text;
    return span;
}



function newActivity(entry)
{
    if (debugLevel())
    {
        debug("newActivity", showObj(entry));
    }

    // Truncate the table
    let root  = kDOM.findId("activityRoot");
    let rows  = root.querySelectorAll("table tr");
    let num   = rows.length / 2;

    if (num === 0)
    {
        startTable();
    }
    else
    if (num >= MAX_ACTIVITY)
    {
        // Delete the first two rows
        kDOM.removeElem(rows[0]);
        kDOM.removeElem(rows[1]);
    }

    let table = root.querySelector("table");
    appendEntry(table, entry);
}



function activityCleared()
{
    showNoActivity();
}

//======================================================================

return {
    init:               init,
    contextMenuShowing: contextMenuShowing,
    uiCommandHandler:   uiCommandHandler,
    activate:           activate,
    deactivate:         deactivate,
    shutdown:           shutdown,
    };

}());
