"use strict";

let {debug, showObj} = kUtils;


self.port.on("setCanUndo",    setCanUndo);
self.port.on("setCanInstall", setCanInstall);

self.port.on("setLevels", function(levels, canUndo, canWhichSite)
{
    /*  For each level we enable the button and load its text
        with either its element id or a classification (from
        kDOM.classifyElem).
    */
    //debug("setLevels 1");
    let base = document.querySelector("#levels");
    kDOM.clearDOMChildren(base);

    for (let i = 0; i < levels.length; ++i)
    {
        let btn = kDOM.create("button", base);

        let level = levels[i];

        // REVISIT This length limit should vary with the font size or panel width somehow
        btn.textContent = level.descr.substring(0, 25);
        btn.setAttribute("data-level", i + 1);

        btn.addEventListener("mouseenter", buttonOver,  false);
        btn.addEventListener("mouseleave", buttonOver,  false);
        btn.addEventListener("click",      buttonClick, false);
    }

    //debug("setLevels 2");
    setCanUndo(canUndo);

    kDOM.findId("thisSiteWhich").disabled = !canWhichSite;
    kDOM.findId("thisSiteWhich").selected = canWhichSite;       // let it be the default
});



function setCanUndo(flag)
{
    debug("choosePanel: setCanUndo", flag);
    kDOM.findId("undoButton").disabled = !flag;
}



function setCanInstall(flag)
{
    debug("choosePanel: setCanInstall", flag);
    kDOM.findId("installButton").disabled = !flag;
}



function onLoad()
{
    kDOM.findId("thisSiteWhich").textContent = self.options.thisSiteWhich;
    kDOM.findId("icon").src                  = self.options.iconURL;

    let elem;

    elem = kDOM.findId("undoButton");
    elem.onclick = () => self.port.emit("undo");

    elem = kDOM.findId("installButton");
    elem.onclick = () => self.port.emit("install");
}


function buttonOver(ev)
{
    /*  When we enter the button it will be highlighted. We want to
        broadcast an event to the killit script to change the highlighted
        element.
    */
    let target = ev.target;

    if (target.nodeName === "BUTTON")
    {
        let level = target.getAttribute("data-level") - 1;
        self.port.emit("selectLevel", level, ev.type === "mouseenter");
    }
}



function buttonClick(ev)
{
    let target = ev.target;

    if (target.nodeName === "BUTTON")
    {
        let method = kDOM.findId("methodSelect").value;
        let which  = kDOM.findId("whichSelect").value;
        let level  = target.getAttribute("data-level") - 1;
        self.port.emit("clickLevel", level, method, which);
    }
}


// This content script is only loaded after the page is loaded.
onLoad();
