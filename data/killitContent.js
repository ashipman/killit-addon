// This is the content script that goes into each page.

"use strict";

let {log, logException, showElem, showObj} = kUtils;


var gSerialNum = 0;
var gParts     = [];
var gLateNodes = false;         // set if nodes have arrived late

// This is a timer to wait for late additions to stop.
var gLateWait  = 1000;          // in milliseconds
var gLateTimer = null;

var highlightElement = null;

/*  This is set when we can undo a recent kill.
    It is an array of {element, display}.
*/
var undoList = [];


function debug()
{
    if (kUtils.debugLevel())
    {
        let args = Array.slice(arguments);
        args.unshift("Killit: " + gSerialNum + ":");
        console.log(args.join(" "));
    }
}

onLoad();

//======================================================================
// Killing


function onLoad()
{
    self.port.on("init",            init);
    self.port.on("setConfigs",      setConfigs);
    self.port.on("highlightLevel",  highlightLevel);
    self.port.on("removeHighlight", removeHighlight);
    self.port.on("killit",          killit);
    self.port.on("undo",            undo);


    window.addEventListener("pagehide", (ev) =>
    {
        //debug("pagehide for", document.documentURI);
        removeHighlight();
    });

    window.addEventListener("pageshow", (ev) =>
    {
        // This will be done on the first load.
        //debug("pageshow for", document.documentURI);
        self.port.emit("requestConfigs");
    });
}



function init(info)
{
    gSerialNum = info.serialNum;
    kUtils.setDebugLevel(info.debugLevel);
    initObserver();
}


function setConfigs(parts)
{
    /*  This supplies either the initial kill configs or an update.
        Either way we can apply them from scratch.

        This isn't called until after the document is ready so we can
        immediately prune the document.

        parts is an array of objects with information about the group
        and a part property.
    */
    gParts = parts;

    if (kUtils.debugLevel())      // more efficient
    {
        debug("gParts for", document.documentURI, showObj(gParts));
    }

    runKill();
}



function runKill()
{
    // We should only get enabled parts.
    gParts.forEach((info) =>
    {
        let part = info.part;

        if (part.enabled)
        {
            let kills = [];

            part.selectors.forEach((sel) => killFromSelector(sel, kills));

            // Select the non-null kills.
            kills = kills.filter((k) => !!k);

            if (kills.length > 0)
            {
                self.port.emit("killActivity", {
                    groupDescr: info.groupName,
                    partDescr:  part.title,
                    url:        document.documentURI,
                    kills:      kills,
                    });
            }
        }
    });
}



function killFromSelector(selectorObj, kills)
{
    // This pushes actual kills onto the supplied array. There may be nulls.
    try
    {
        let {selector, method} = selectorObj;

        if (kDOM.isXPath(selector))
        {
            let snapshot = document.evaluate(selector, document.documentElement, null,
                            XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);

            for (let i = 0 ; i < snapshot.snapshotLength; i++ )
            {
                kills.push(killOneElem(snapshot.snapshotItem(i), selector, method));
            }
        }
        else
        {
            let elems = document.querySelectorAll(selector);

            for (let i = 0 ; i < elems.length; i++ )
            {
                kills.push(killOneElem(elems[i], selector, method));
            }
        }
    }
    catch(e)
    {
        logException(e, selectorObj.selector);
    }
}



function killOneElem(elem, selector, method)
{
    //  This returns an object describing the kill or null.
    let did = false;

    switch (method)
    {
    case "hide":
        let style = window.getComputedStyle(elem);

        if (style.display !== "none")
        {
            //debug("KILLED: hide", document.documentURI, selector);
            saveUndo(elem, style.display);
            elem.style.setProperty("display", "none", "important");
            did = true;
        }
        break;

    case "delete":
        //debug("KILLED: delete", document.documentURI, selector);
        elem.parentNode.removeChild(elem);
        clearUndo();
        did = true;
        break;
    }

    return did? {method: method, selector: selector} : null;
}

//======================================================================

/*  Test this on www.wired.com which has very complicated pages with
    many iframes and scripts that load disqus material late.
*/

function treeTimeout()
{
    //debug("treeTimeout doing late nodes");
    gLateTimer = null;
    runKill();
}



function treeObserver(records)
{
    // Remember if we have late nodes but skip firebug nodes.
    records.forEach(function(r)
    {
        let num = r.addedNodes.length;

        if (num > 0)
        {
            if (kUtils.debugLevel() >= 2)
            {
                debug("treeObserver new nodes num=" + num);
            }

            for (let i = 0; i < num; ++i)
            {
                let node = r.addedNodes.item(i);
                let tag  = node.nodeName;
                let fbug = node.className && node.className.match('firebug') !== null;
                //debug("late node", showElem(node));

                if (!fbug)
                {
                    gLateNodes = true;
                }
            }

            if (gLateTimer)
            {
                clearTimeout(gLateTimer);
            }

            if (gLateNodes)
            {
                gLateTimer = setTimeout(treeTimeout, gLateWait);
            }
        }
    });
}



function initObserver()
{
    if (MutationObserver)
    {
        if (document && document.body)
        {
            debug("creating a MutationObserver");

            var obs = new MutationObserver(treeObserver);
            obs.observe(document.body, {childList: true, subtree: true});
        }
        else
        {
            debug("no document");
        }
    }
    else
    {
        debug("no MutationObserver was found");
    }
}


//======================================================================
// Designing


function highlightLevel(level)
{
    // The level is the object returned from contextContent.js.
    //debug("highlightLevel", showObj(level));

    let elem = kDOM.findElemBySelector(level.selector, document);

    if (elem)
    {
        /*  Create an highlight element that covers the original.
        */
        //debug("creating highlightElement");

        removeHighlight();

        let pos = level.position;

        highlightElement = kDOM.create("div", document.documentElement);
        highlightElement.id = "highlightElement";
        highlightElement.style =
            "position: " + (pos.fixed? "fixed;" : "absolute;") +
            "width: "   + pos.width + "px;" +
            "height: "  + pos.height + "px;" +
            "left: "    + pos.left + "px;" +
            "top: "     + pos.top + "px;" +
            "background-color: rgba(255, 100, 0, 0.4);" +
            "z-index:   2147483647;";    // bigger than wired uses
    }
}



function removeHighlight()
{
    if (highlightElement)
    {
        highlightElement.parentNode.removeChild(highlightElement);
        highlightElement = null;
    }
}



function killit(level, method)
{
    // The level is the object returned from contextContent.js.
    if (kUtils.debugLevel())
    {
        debug("killit in", document.documentURI);

        if (kUtils.debugLevel() >= 2)
        {
            debug("killing", showObj(level), "using", method);
        }
    }

    removeHighlight();

    let elem = kDOM.findElemBySelector(level.selector, document);

    if (elem)
    {
        killOneElem(elem, level.selector, method);
    }
}



function undo()
{
    undoList.forEach(({element, display}) =>
    {
        debug("unhide", showElem(element));
        element.style.display = display;
    });

    clearUndo();
}



function saveUndo(elem, display)
{
    if (undoList.length === 0)
    {
        self.port.emit("canUndo", true);
    }
    undoList.push({element: elem, display: display});
}



function clearUndo()
{
    undoList = [];
    self.port.emit("canUndo", false);
}
