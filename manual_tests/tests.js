
const allTestDocs = [
    "tc_create_group_from_empty_1",
    "tc_create_group_from_empty_2",
    "tc_create_group_from_empty_3",
    "tc_kill_page_1",
    "tc_kill_page_2",
    "tc_organise_1",
    "tc_organise_2",
    "tc_organise_3",
    "tc_organise_4",
    "tc_import_1",
    "tc_import_2",
    "tc_private_1",
    "tc_catalog_1",
    "tc_backup_1"
    ];
