"use strict";

//======================================================================


function nextTests(testName)
{
    let prev = null;
    let next = null;

    for (let i = 0; i < allTestDocs.length; ++i)
    {
        if (allTestDocs[i] === testName)
        {
            if (i + 1 < allTestDocs.length)
            {
                next = allTestDocs[i + 1];
            }
            break;
        }

        prev = allTestDocs[i];
    }

    return {prev: prev, next: next};
}


//======================================================================
// Create Test Pages

/*  Build this structure

        <div id="testCase">

            <table id="topTable">

            <colgroup>
                <col width="10%"></col>
                <col width="90%"></col>
            </colgroup>

            <tr>
                <th>Test Name
                <td>tc_create_group_from_empty
            </tr>

            </table>

            <table id="stepTable">

            <colgroup>
                <col width="10%"></col>
                <col width="45%"></col>
                <col width="45%"></col>
            </colgroup>

            <thead> <tr> <td>Step <td>Description <td>Expected </tr> </thead>

            <tr>
                <td>1
                <td> Click on the toolbar button to open the dialog box.
                <td> The <i>unsorted</i> group will appear and be empty.
                     <img src="tc_create_group_from_empty_good_1_img1.png">
            </tr>

            </table>

        </div>
*/

function buildTestPage()
{
    let tc = create({tag: "div", id: "testCase"});
    let toptab  = create({tag: "table", id: "topTable", parent: tc});

    let colg = create({tag: "colgroup", parent: toptab});
    create({tag: "col", parent: colg, attrs: {width: "10%"}});
    create({tag: "col", parent: colg, attrs: {width: "90%"}});

    let tr, th, td;

    /*  Step through the rows in #headerData and copy them to the header.
    */
    let headers = [
        {id: "precondition",    label: "Precondition"},
        {id: "postcondition",   label: "Postcondition"},
        {id: "notes",           label: "Notes"},
        ];

    headers.forEach(({id, label}) =>
    {
        let hpart = document.querySelector("#" + id);

        if (hpart)
        {
            let tr = create({tag: "tr", parent: toptab});
            create({tag: "th", parent: tr, html: label});
            create({tag: "td", parent: tr, html: hpart.innerHTML});
        }
    });

    let steptab  = create({tag: "table", id: "stepTable", parent: tc});

    colg = create({tag: "colgroup", parent: steptab});
    create({tag: "col", parent: colg, attrs: {width: "10%"}});
    create({tag: "col", parent: colg, attrs: {width: "45%"}});
    create({tag: "col", parent: colg, attrs: {width: "45%"}});

    let thead = create({tag: "thead", parent: steptab});
    tr = create({tag: "tr", parent: thead});
    create({tag: "th", parent: tr, html: "Step"});
    create({tag: "th", parent: tr, html: "Description"});
    create({tag: "th", parent: tr, html: "Expected"});

    // Copy data from the rows of the step table.
    let steps = document.querySelectorAll("#stepData tr");

    for (let i = 0; i < steps.length; ++i)
    {
        let parts = steps[i].querySelectorAll("td");
        let descr = parts.length > 0? parts[0].innerHTML : "";
        let expec = parts.length > 1? parts[1].innerHTML : "";

        tr = create({tag: "tr", parent: steptab});
        create({tag: "td", parent: tr, html: (i+1) + ""});
        create({tag: "td", parent: tr, html: descr});
        create({tag: "td", parent: tr, html: expec});
    }

    document.body.appendChild(tc);
}


//======================================================================

function create(args)
{
    let elem = document.createElement(args.tag);

    if (args.parent)
    {
        args.parent.appendChild(elem);
    }

    if (args.id)
    {
        elem.id = args.id;
    }

    if (args.classes)
    {
        let cs = [].concat(args.classes);
        cs.forEach((c) => elem.classList.add(c));
    }

    if (args.attrs)
    {
        for (let n in args.attrs)
        {
            elem.setAttribute(n, args.attrs[n]);
        }
    }

    if (args.html)
    {
        elem.innerHTML = args.html;
    }

    return elem;
}



function getQuery(args)
{
    // Return an object with the query parameters according to HTML5
    let pairs = {};
    let q     = document.location.search;

    if (q)
    {
        if (q[0] === "?")
        {
            q = q.substring(1);
        }

        q.split("&").forEach((p) =>
        {
            let words = p.split("=");
            let left;
            let right;

            if (words.length === 1)
            {
                left = words[0];
                right = "";
            }
            else
            {
                left = words[0];
                right = words[1];
            }

            left  = unescapeHex(left.replace(/[+]/g, " "));
            right = unescapeHex(right.replace(/[+]/g, " "));
            pairs[left] = right;
        });
    }

    return pairs;
}


function unescapeHex(s)
{
    // Undo %hex escaping.
    return s.replace(/%([0-9a-f]{2})/i, (all, hex) =>
        {
            try
            {
                return String.fromCharCode(parseInt(hex, 16));
            }
            catch(e)
            {
                return all;
            }
        });
}
