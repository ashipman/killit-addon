#!/usr/bin/python

import json
import os
import re
import sys
import time

import ffRunner
import actions
import bug_actions


def main():
    # Get a test dump
    conn = ffRunner.startFirefox(debug = True)

    if False:
        print "Press return for a test dump"
        sys.stdin.readline()

        ffRunner.findAnyMainWindow()
        result = actions.sendTestCommand("testDump")
        print "result is", actions.formatJSON(result)

    ffRunner.gotoInitialWindow()
    conn.switch_to_frame()          # ensure at the top of the window
    conn.navigate("file:///home/felix/public_html/index.html")
    time.sleep(1)

    # This old version requires the tab be the context
    doContextMenuAtElement("div.title", "Kill It")

    r = actions.sendTestCommand("clickChooseLevel", 1)
    print "***** clickChooseLevel returns", r

    time.sleep(1)
    actions.sendTestCommand("hideChoosePanel")

    time.sleep(2)
    ffRunner.stopFirefox()


#======================================================================

# This is old code that exposes a bug with SpecialPowers but only
# in certain circumstances.

def doContextMenuAtElement(selector, labelRegExp):
    # Try popping up the context menu on the element chosen by
    # the selector. Then search for the menu item that matches
    # the given label. The label will be matched as a JS RegExp
    # e.g. "/Kill It/"

    # This requires that the connection be in a Content context
    # with a HTML page loaded.  It will leave the connection in
    # Chrome context.

    script = """
    let elem = document.querySelector("%s");
    let s = false
    if (elem)
    {
        let utils = SpecialPowers.getDOMWindowUtils(window);
        let rect  = elem.getBoundingClientRect();
        utils.sendMouseEvent("contextmenu",
            rect.left + (rect.width / 2),
            rect.top + (rect.height / 2),
            2, 1, 0);
        s = true;
    }
    return s;
    """ % selector

    result = ffRunner.conn().execute_script(script, special_powers=True)
    print "******  contextMenuAtElement popup result", result
    time.sleep(2)

    # Try to switch to chrome context and find the context menu itself
    ffRunner.findAnyMainWindow()

    script = """
    let popup = document.querySelector("#contentAreaContextMenu");
    let items = popup.querySelectorAll("menuitem");
    let s = false;
    for (let i = 0; i < items.length; ++i)
    {
        let item = items[i];
        if (item.label.match(/%s/))
        {
            item.doCommand();
            s = true;
        }
    }
    setTimeout(() => popup.hidePopup(), 500);
    return s;
    """ % labelRegExp

    result = ffRunner.conn().execute_script(script);
    print "******  context menu command result", result


main()
