
import json
import os
import sys
import time
import types

import ffRunner
from marionette import Marionette, MarionetteException

#======================================================================

# Some key codes used by the Marionette send_keys() function.
# See marionette-listener.js

TAB     = "\uE004"
RETURN  = "\uE006"
ENTER   = "\uE007"
ESCAPE  = "\uE00C"

SHIFT   = "\uE008"
CONTROL = "\uE009"
ALT     = "\uE00A"
META    = "\uE03D"

LEFT    = "\uE012"
RIGHT   = "\uE014"
DOWN    = "\uE015"
UP      = "\uE013"

#======================================================================

def sendTestCommand(command, arg = None, timeout = None):
    # Try sending a test harness event to a top-level window.
    # Our add-on will see the custom event. We need to be in Chrome context
    # for this to be likely to work.

    # Note that json.dumps() generates strings with "". We must quote it with ''.
    script = """
    var event = new CustomEvent("testHarness", {
        detail: {
            command:  "%s",
            arg:      '%s',
            callback: marionetteScriptFinished
        }});
    window.top.dispatchEvent(event);
    """ % (command, json.dumps(arg))

    if timeout != None:
        ffRunner.conn().set_script_timeout(timeout * 1000)  # in milliseconds

    result = None

    try:
        result = ffRunner.conn().execute_async_script(script)
    except Exception, exn:
        print exn

    return result



def formatJSON(s):
    if s == None:
        return "None"
    elif isinstance(s, types.StringTypes):
        return json.dumps(json.loads(s), indent=4)
    else:
        return json.dumps(s, indent=4)



def pruneJSON(obj, edits):
    # obj is the result from json.loads() on a test dump of the dialog model
    # We normalise it by removing the uuid fields and cutting URL patterns
    # A new object is returned
    if isinstance(obj, types.ListType):
        return [pruneJSON(e, edits) for e in obj]

    if isinstance(obj, types.DictType):
        o = {}
        for key in obj:
            v  = obj[key]
            #print "pruneJSON", key

            ed = None
            if key in edits:
                ed = edits[key]

            if ed != "remove":
                v = pruneJSON(v, edits)

                #print "pruneJSON ed", ed, v, type(v)
                if ed == "basename" and isinstance(v, types.StringTypes):
                    v = os.path.basename(v)
                    #print "pruneJSON basename", v

                o[key] = v

        return o

    # Return unstructured values
    return obj



def sameJSON(obj1, obj2):
    # Print the differences to stdout. Return True if they are the same.
    if isinstance(obj1, types.ListType) and isinstance(obj2, types.ListType):
        if len(obj1) == len(obj2):
            for (o1, o2) in zip(obj1, obj2):
                if not sameJSON(o1, o2):
                    return False
        else:
            print "lists lengths differ"
            return False

    if isinstance(obj1, types.DictType) and isinstance(obj2, types.DictType):
        k1 = obj1.keys()
        k2 = obj2.keys()
        k1.sort()
        k2.sort()
        if k1 == k2:
            for k in k1:
                v1 = obj1[k]
                v2 = obj2[k]
                if not sameJSON(v1, v2):
                    print "key differs", k
                    return False
            return True
        else:
            print "keys differ", set(k1) ^ set(k2)
            return False

    return obj1 == obj2


def waitForReturn():
    print "Press return to continue"
    sys.stdin.readline()

#======================================================================


def doMainContextMenuAtElement(selector, labelRegExp):
    # This requires chrome context
    sendTestCommand("clickContextMenu", ["main", selector, labelRegExp, 500])



def doDlgContextMenuAtElement(selector, labelRegExp):
    # This requires chrome context
    sendTestCommand("clickContextMenu", ["dialog", selector, labelRegExp, 500])



def clickToolbar():
    # Click on the add-on widget to get the dialog to pop up.
    # REVISIT - move this to kTesting
    script = """
    let elems = document.querySelectorAll('toolbaritem');
    let s = false;
    for (let i = 0; i < elems.length; ++i)
    {
        if (elems[i].id.match(/killit/))
        {
            let iframe = elems[i].firstChild;

            event = new MouseEvent('click', {
                'view': window,
                'bubbles': true,
                'cancelable': true
                });
            s = iframe.contentWindow.dispatchEvent(event);
        }
    }
    return s;
    """

    r = ffRunner.conn().execute_script(script);
    print "clickToolbar result", r




def findDialog():
    # Find the dialog window and leave it current
    # We stay in chrome context. The content context seems to confuse things.
    result = False
    conn = ffRunner.conn()

    conn.set_context(conn.CONTEXT_CHROME)

    for win in conn.window_handles:
        conn.switch_to_window(win)
        if conn.title.find("Kills") >= 0:
            conn.switch_to_frame("container")
            print "frame is now", conn.get_active_frame()
            result = True

    return result



def listElements():
    # List the elements found in the current window.
    elems = ffRunner.conn().find_elements("xpath", "//*")
    print "got #elems", len(elems)
    for elem in elems:
        #print "tag", elem.tag_name, showLoc(elem.location)
        print "tag", elem.tag_name, "id", elem.get_attribute("id")



def findActiveElement():
    # Find id of the element that is active or return null.
    script = """
    let elem = document.activeElement;
    return elem? elem.id : null;
    """
    result = None

    try:
        result = ffRunner.conn().execute_script(script)
    except Exception, exn:
        print exn

    return result



def sendInputText(whichWindow, selector, text):
    # This requires that findAnyMainWindow() be recently run
    # Use selector=! for the currently active element
    sendTestCommand("sendString", [whichWindow, selector, text])



def sendSpecialKey(whichWindow, selector, args):
    # This requires that findAnyMainWindow() be recently run
    # Use selector=! for the currently active element
    #
    # args is a dict with
    #   shift:    BOOL
    #   control:  BOOL
    #   alt:      BOOL
    #   meta:     BOOL
    #   keyCode:  STRING    for VK_ keys e.g. "tab"
    # or just a string with the keyCode

    if isinstance(args, types.StringTypes):
        args = {"keyCode": args}

    sendTestCommand("sendKey", [whichWindow, selector, args])
