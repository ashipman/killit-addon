#!/usr/bin/python

import json
import os
import re
import sys
import time

from mozprofile import Profile
from mozrunner import FirefoxRunner
from marionette import Marionette, MarionetteException

import ffRunner

#======================================================================

conn = ffRunner.startFirefox()

mainWindow = conn.current_window_handle


def showLoc(loc):
    return "(%d, %d)" % (loc['x'], loc['y'])


def listElements():
    # list all elements
    elems = conn.find_elements("xpath", "//*")
    print "got #elems", len(elems)
    for elem in elems:
        #print "tag", elem.tag_name, showLoc(elem.location)
        print "tag", elem.tag_name, "id", elem.get_attribute("id")



# Get access to chrome windows
conn.set_context(conn.CONTEXT_CHROME)

for win in conn.window_handles:
    conn.switch_to_window(win)
    print "window title", conn.title
    print "window type", conn.get_window_type()

    if conn.get_window_type() == "navigator:browser":
        print "found the browser window"
        break


# see if we can inject a script
script = """
let elems = document.querySelectorAll('toolbaritem');
let s = '';
for (let i = 0; i < elems.length; ++i)
{
    if (elems[i].id.match(/killit/))
    {
        let iframe = elems[i].firstChild;

        event = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
            });
        s += "result " + iframe.contentWindow.dispatchEvent(event);
    }
}
return s;
"""

print conn.execute_script(script);

ffRunner.waitUntilDone()
