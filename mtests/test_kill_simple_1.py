#!/usr/bin/python

import json
import os
import sys
import time
import unittest

import ffRunner
import actions

#======================================================================

expect1 = json.loads(
"""
{
    "groups": [
        {
            "comment": "",
            "title": "unsorted",
            "selected": true,
            "enabled": true,
            "parts": [
                {
                    "urlPattern": {
                        "path": "home.html",
                        "host": "",
                        "scheme": "file"
                    },
                    "title": "Home Page",
                    "selected": false,
                    "enabled": true,
                    "selectors": [
                        {
                            "comment": "",
                            "selector": "#title_to_kill",
                            "selected": false,
                            "enabled": false,
                            "method": "hide",
                            "validated": true,
                            "open": false
                        }
                    ],
                    "validated": true,
                    "open": false
                }
            ],
            "validated": true,
            "open": true
        }
    ]
}
""")

expect2 = json.loads(
"""
[
    {
        "comment": "",
        "name": "unsorted",
        "enabled": true,
        "state": "local",
        "contactEMail": "",
        "parts": [
            {
                "selectors": [
                    {
                        "comment": "",
                        "selector": "#title_to_kill",
                        "method": "hide"
                    }
                ],
                "validated": true,
                "enabled": true,
                "urlPattern": {
                    "path": "home.html",
                    "host": "",
                    "scheme": "file"
                },
                "title": "Home Page"
            }
        ],
        "formatVersion": 1,
        "contentVersion": ""
    }
]
""")

class TestKillSimple1(unittest.TestCase):

    def setUp(self):
        # Clear the database
        ffRunner.startFirefox(resetStore = True, debug = False)



    def tearDown(self):
        #time.sleep(1)
        ffRunner.stopFirefox()



    def test_kill_title(self):
        # Make a URL to the home.html file.
        url = "file://" + os.getcwd() + "/home.html"
        print "url is", url

        ffRunner.gotoInitialWindow()
        ffRunner.conn().navigate(url)
        time.sleep(1)

        # We need to be in chrome context now to operate the context menu.
        ffRunner.enterChrome()

        # Use the context menu on one of the title elements
        # This will popup the Choose Panel
        actions.doMainContextMenuAtElement("#title_to_kill", "Kill It")
        time.sleep(1)

        r = actions.sendTestCommand("clickChooseLevel", 1)
        self.assert_(r, "The level 1 choice didn't work")
        time.sleep(1)

        # Install this kill
        r = actions.sendTestCommand("clickChooseInstall")
        self.assert_(r, "installing the kill didn't work")
        time.sleep(1)

        actions.sendTestCommand("hideChoosePanel")

        # Put up the dialog to get a test dump
        actions.clickToolbar()
        time.sleep(1)

        # This comes back as a Python data structure (a dict)
        result = actions.sendTestCommand("modelDump")
        self.compareConfigs(result, expect1)

        r = actions.sendTestCommand("closeDialog")
        self.assert_(r, "The close of the dialog didn't work")
        time.sleep(1)

        # Get the configs from the store and check that they were 
        # saved properly.
        result = actions.sendTestCommand("getConfigs")
        self.compareConfigs(result, expect2)

        r = actions.sendTestCommand("isHidden", "#title_to_kill")
        self.assert_(r, "The title element is not hidden")



    def compareConfigs(self, result, expected):
        #print "result is", actions.formatJSON(result)

        pruned = actions.pruneJSON(result, {
                    "uuid": "remove",
                    "id":   "remove",
                    "lastModified": "remove",
                    "path": "basename"
                    })
        #print "pruned is", actions.formatJSON(pruned)

        actions.sameJSON(expected, pruned)
        self.assertEqual(expected, pruned)


if __name__ == '__main__':
    unittest.main()

