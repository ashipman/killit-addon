#!/usr/bin/python

import json
import os
import re
import sys
import time

import ffRunner
import actions

#ffRunner.resetAddon()

# Get a test dump
conn = ffRunner.startFirefox()

if False:
    print "Press return for a test dump"
    sys.stdin.readline()

    ffRunner.findAnyMainWindow()
    result = actions.sendTestCommand("testDump")
    print "result is", actions.formatJSON(result)

ffRunner.gotoInitialWindow()
conn.switch_to_frame()          # ensure at the top of the window
conn.navigate("file:///home/felix/public_html/index.html")
time.sleep(1)

actions.doMainContextMenuAtElement("div.title", "Kill It")

ffRunner.findAnyMainWindow()
r = actions.sendTestCommand("clickChooseLevel", 1)
print "***** clickChooseLevel returns", r

actions.sendTestCommand("hideChoosePanel")

time.sleep(2)
ffRunner.stopFirefox()
