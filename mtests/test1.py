#!/usr/bin/python

import json
import os
import re
import sys
import time

from mozprofile import Profile
from mozrunner import FirefoxRunner
from marionette import Marionette, MarionetteException

#======================================================================

baseDir = "/home/felix/wk/killit/killit"

ffVersion  = "29.0.1"
ffBinary   = "/usr/local/firefox-" + ffVersion + "/firefox"
ffTimeout  = 5
ffRunner = None
ffLogAll = True

def handleDebugLines(line):
    if ffLogAll or line.lower().find("killit") >= 0:
        print "debug line", line


def onFFTimeout():
    print("Firefox timed out after %d seconds" % (ffTimeout))

#======================================================================


def startFirefox():
    global ffRunner

    profileDir  = os.path.join(baseDir, "profile-" + ffVersion)
    testProfile = Profile(profile = profileDir)

    testProfile.set_preferences({
        "extensions.sdk.console.logLevel":  "debug",
        "extensions.logging.enabled":       True,
        "dom.mozBrowserFramesEnabled":      True,
        "marionette.logging":               True
        })

    #print testProfile.summary()


    # Note the mozrunner doco says "process_args" but we must write kp_kwargs
    ffRunner = FirefoxRunner(
                binary  = ffBinary,
                profile = testProfile,
                cmdargs = ["-marionette"],
                kp_kwargs = {
                     'processOutputLine': [handleDebugLines],
                     'onTimeout':         onFFTimeout
                     }
                )
    ffRunner.start(timeout = ffTimeout)

    # Wait until it has started
    time.sleep(2)

#======================================================================

startFirefox()

marn = Marionette()

for t in range(0, 10):
    try:
        print "trying to start a session"
        sess = marn.start_session()
        break
    except BaseException, exn:
        print "startup exception", exn
        time.sleep(1)
else:
    sys.exit(0)

mainWindow = marn.current_window_handle


def showLoc(loc):
    return "(%d, %d)" % (loc['x'], loc['y'])


def listElements():
    # list all elements
    elems = marn.find_elements("xpath", "//*")
    print "got #elems", len(elems)
    for elem in elems:
        #print "tag", elem.tag_name, showLoc(elem.location)
        print "tag", elem.tag_name, "id", elem.get_attribute("id")


def findToolbarButton():
    elems = marn.find_elements("tag name", "toolbaritem")
    print "findToolbarButton got #elems", len(elems)
    for elem in elems:
        id = elem.get_attribute("id")
        if id.find("killit") >= 0:
            print "toolbaritem", elem.tag_name, "id", elem.get_attribute("id")
            children = marn.find_elements("tag name", "iframe", str(elem))
            print "# IFRAME children", len(children)
            for c in children:
                print "child", c.tag_name, "id", c.get_attribute("id")
                print "clicking"
                c.click()


def listWindows():
    # Get access to chrome windows
    marn.set_context(marn.CONTEXT_CHROME)

    for win in marn.window_handles:
        marn.switch_to_window(win)
        print "window title", marn.title
        print "window type", marn.get_window_type()
        #print "top frame elements"
        findToolbarButton()
        print "toolbar button done"

        if False and marn.get_window_type() != "navigator:browser":
            marn.switch_to_frame("container");
            print "frame is now", marn.get_active_frame()
            listElements()
            marn.switch_to_frame();
            print "frame is now", marn.get_active_frame()

    marn.set_context(marn.CONTEXT_CONTENT)



def gotoURL(url):
    # Find the first browser
    for win in marn.window_handles:
        marn.switch_to_window(win)
        if marn.get_window_type() == "navigator:browser":
            marn.navigate(url)
            break


listWindows()

#gotoURL("file:///usr/share/doc/antlr-manual-2.7.7/index.html")
#listElements()

if ffRunner:
    ffRunner.wait()
