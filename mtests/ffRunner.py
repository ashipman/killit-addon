#!/usr/bin/python

import json
import os
import re
import sys
import time

from mozprofile import Profile
from mozrunner  import FirefoxRunner
from marionette import Marionette, MarionetteException

#======================================================================

baseDir = "/home/felix/wk/killit/killit"

ffVersion  = "29.0.1"
ffBinary   = "/usr/local/firefox-" + ffVersion + "/firefox"
ffTimeout  = 20 * 1000
ffRunner = None
ffLogAll = True

ffProfile = Profile(profile = os.path.join(baseDir, "profile-" + ffVersion))

# The Marionette object
_conn   = None

# When we start a session a new tab is created for us.
initialWindow = None

def handleDebugLines(line):
    if ffLogAll or line.lower().find("killit") >= 0:
        print "debug line", line


def onFFTimeout():
    print("Firefox timed out after %d seconds" % (ffTimeout))

#======================================================================


def startFirefox(resetStore = False, debug = False):
    global ffRunner
    global ffProfile
    global _conn

    # Run with the RESET_TEST_DATA env var set to clear the store.
    # We have to connect and get it to shut down properly for the store to
    # be saved.
    if resetStore:
        os.environ["RESET_TEST_DATA"] = "yes"


    ffProfile.set_preferences({
        "extensions.sdk.console.logLevel":  "debug",
        "extensions.logging.enabled":       False,
        "dom.mozBrowserFramesEnabled":      True,
        "marionette.logging":               debug
        })

    #print testProfile.summary()


    # Note the mozrunner doco says "process_args" but we must write kp_kwargs
    ffRunner = FirefoxRunner(
                binary  = ffBinary,
                profile = ffProfile,
                cmdargs = ["-marionette"],
                kp_kwargs = {
                     'processOutputLine': [handleDebugLines],
                     'onTimeout':         onFFTimeout
                     }
                )
    ffRunner.start(timeout = ffTimeout)

    connect()

    if resetStore:
        del os.environ["RESET_TEST_DATA"]

    return _conn


def conn():
    return _conn


def connect():
    global _conn
    global initialWindow

    _conn = Marionette()

    for t in range(0, 10):
        try:
            time.sleep(1)
            print "trying to start a session"
            _conn.start_session()
            break
        except BaseException, exn:
            print "startup exception", exn
    else:
        sys.exit(0)

    initialWindow = _conn.current_window_handle
    print "initial window", initialWindow
    return _conn



def stopFirefox():
    if findAnyMainWindow():
        #_conn.execute_script("document.querySelector('#cmd_quitApplication').click();");
        try:
            _conn.find_element("id", "cmd_quitApplication").click()
        except Exception, exn:
            print exn
    else:
        print "****** Can't find a browser window"

    waitUntilDone()


def waitUntilDone():
    if ffRunner:
        ffRunner.wait()



def findAnyMainWindow():
    global _conn

    # This will leave the connection pointing at a 
    # top-level chrome browser window and in Chrome context.
    enterChrome()

    for win in _conn.window_handles:
        _conn.switch_to_window(win)
        _conn.switch_to_frame()
        print "findAnyMainWindow:", win, _conn.title
        if _conn.get_window_type() == "navigator:browser":
            return True

    return False



def enterChrome():
    global _conn
    _conn.set_context(_conn.CONTEXT_CHROME)



def gotoInitialWindow():
    # This leaves the connection in Content context for the
    # initial tab that was created. Use this to load the
    # first page.
    global initialWindow
    _conn.set_context(_conn.CONTEXT_CONTENT)
    _conn.switch_to_window(initialWindow)

