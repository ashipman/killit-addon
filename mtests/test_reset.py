#!/usr/bin/python

import json
import sys
import time
import unittest

import ffRunner
import actions

#======================================================================

class TestReset(unittest.TestCase):

    def setUp(self):
        # Clear the database
        ffRunner.startFirefox(resetStore = True, debug = False)


    def test_is_reset(self):
        # We must be in chrome context with a main window
        ffRunner.findAnyMainWindow()

        # The result will be a list of groups
        result = actions.sendTestCommand("getConfigs")
        #print "result is", actions.formatJSON(result)

        pruned = actions.pruneJSON(result, {
                    "lastModified": "remove"
                    })
        #print "pruned is", actions.formatJSON(pruned)

        expect = json.loads(
"""
[
    {
        "comment": "",
        "contentVersion": "",
        "uuid": "f27e7089-c526-497e-ef21-a8d7729aa785",
        "enabled": true,
        "formatVersion": 1,
        "contactEMail": "",
        "state": "local",
        "parts": [],
        "name": "unsorted"
    }

]
""")
        self.assertEqual(expect, pruned, "The killit store is not empty")


    def tearDown(self):
        ffRunner.stopFirefox()


if __name__ == '__main__':
    unittest.main()

