#!/usr/bin/python

import json
import os
import sys
import time
import unittest

import ffRunner
import actions

#======================================================================

expect = json.loads(
"""
{
    "name": "new group",
    "comment": "comment",
    "contentVersion": "v1",
    "enabled": true,
    "formatVersion": 1,
    "contactEMail": "email",
    "state": "local",
    "parts": [
        {
            "title": "part 1",
            "enabled": false,
            "selectors": [
                {
                    "comment": "xpath comment",
                    "selector": "//input",
                    "method": "hide"
                }
            ],
            "validated": true,
            "urlPattern": {
                "path": "",
                "host": "www.example.com",
                "scheme": ""
            }
        }
    ]
}
""")


class TestDlgNewGroup1(unittest.TestCase):

    def setUp(self):
        # Clear the database
        ffRunner.startFirefox(resetStore = True, debug = False)



    def test_new_group_1(self):
        conn = ffRunner.conn()

        # Put up the dialog 
        ffRunner.enterChrome()
        actions.clickToolbar()
        time.sleep(1)

        # Click on New Group in the context menu.
        actions.doDlgContextMenuAtElement("body", "New Group")
        time.sleep(1)

        # Get attention on the dialog's HTML window
        actions.findDialog()
        #actions.listElements()

        # Get the focussed element. It must be an input with class 'levelTitle'
        id = actions.findActiveElement()
        #print "focussed element", id
        elem = conn.find_element("id", id)
        self.assert_("input", elem.tag_name)
        classes = elem.get_attribute("class");
        self.assert_(classes.find("levelTitle") >= 0)

        # Enter a group name. We need chrome context in a main window
        # for the event dispatch to work.
        ffRunner.findAnyMainWindow()
        newName = "new group"
        actions.sendInputText("dialog", "!", newName)

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "v1")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "comment")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "email")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "part 1")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "www.example.com")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "//input")

        actions.sendSpecialKey("dialog", "!", "tab")
        actions.sendInputText("dialog", "!", "xpath comment")

        actions.sendTestCommand("closeDialog")

        # Check that the new group has been saved properly
        result = actions.sendTestCommand("getConfigs")
        #print "configs are", actions.formatJSON(result)

        # We should be able to find the group in the list by name.
        gs = [g for g in result if g["name"] == newName]
        self.assertEqual(1, len(gs))

        pruned = actions.pruneJSON(gs[0], {
                    "uuid": "remove",
                    "id":   "remove",
                    "lastModified": "remove",
                    "path": "basename"
                    })

        actions.sameJSON(expect, pruned)
        self.assertEqual(expect, pruned)


    def tearDown(self):
        time.sleep(2)
        ffRunner.stopFirefox()


if __name__ == '__main__':
    unittest.main()

