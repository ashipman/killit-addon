#!/usr/bin/python

from marionette import Marionette

conn = Marionette()
conn.start_session()

conn.set_context(conn.CONTEXT_CHROME)

for win in conn.window_handles:
    conn.switch_to_window(win)
    title = conn.title
    type  = conn.get_window_type()
    frame = None
    print "window title", title, "type", type
    for i in range(0, 10):
        try:
            conn.switch_to_frame(i)
            frame = conn.get_active_frame()
            conn.switch_to_frame()
            print "active frame [%d]" % i, frame
            tag   = frame.tag_name
            print "active frame [%d] tag=%s" % (i, tag)
        except Exception, exn:
            print "frame exception", exn
            #conn.switch_to_frame()
        finally:
            #conn.switch_to_frame()
            pass

