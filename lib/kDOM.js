/*  This is a non-SDK module that can be used in a content script.
    It contains utilities that are only relevant to the DOM.
*/

if (typeof require !== "undefined")
{
    kUtils = require("./kUtils");
}


let kDOM = (function()
{
"use strict";

//======================================================================

/*  These are simple codes for human-readable descriptions of nodes.
    They will be used as keys into the l10n database.
*/
const ND_IFRAME = "nd.iframe";
const ND_IMAGE  = "nd.image";
const ND_LIST   = "nd.list";
const ND_LITEM  = "nd.listitem";
const ND_OTHER  = "nd.container";
const ND_PARA   = "nd.para";
const ND_TEXT   = "nd.text";

let   node_descriptions = {};

node_descriptions["IFRAME"] = ND_IFRAME;
node_descriptions["IMG"]    = ND_IMAGE;
node_descriptions["LI"]     = ND_LITEM;
node_descriptions["OL"]     = ND_LIST;
node_descriptions["P"]      = ND_PARA;
node_descriptions["SPAN"]   = ND_TEXT;
node_descriptions["UL"]     = ND_LIST;

//======================================================================

var   idCntr = 0;


function newId()
{
    return "id" + (++idCntr);
}



function appendText(parent, text)
{
    let elem = document.createTextNode(text);
    parent.appendChild(elem);
    return elem;
}



function classifyElem(elem)
{
    return node_descriptions[elem.nodeName] || ND_OTHER;
}



function clearDOMChildren(node)
{
    while (node.childNodes.length > 0)
    {
        node.removeChild(node.childNodes[0]);
    }
}



function create(tag, parent)
{
    let elem = document.createElement(tag);
    elem.id = newId();

    if (parent)
    {
        parent.appendChild(elem);
    }
    return elem;
}



function removeElem(elem)
{
    elem.parentNode.removeChild(elem);
}



function toArray(coll)
{
    // Convert a node collection or similar to an array.
    let a = [];

    for (let i = 0; i < coll.length; ++i)
    {
        a.push(coll[i]);
    }

    return a;
}



function drawOpenClosed(canvas, open)
{
    /*  Draw an open/closed icon in a canvas using its CSS styles.
    */
    let style = window.getComputedStyle(canvas);

    function getDim(name, dflt)
    {
        let prop = style.getPropertyCSSValue(name);

        if (prop)
        {
            return prop.getFloatValue(CSSPrimitiveValue.CSS_PX);
        }

        return dflt;
    }

    let w = getDim("width",  12);
    let h = getDim("height", 12);

    /*  Ensure the canvas size is the set to the same or else it will
        be scaled down from the default width of 300 to 12 and the icon
        will be invisible.
    */
    canvas.width  = w;
    canvas.height = h;

    let ctx = canvas.getContext('2d');
    ctx.strokeStyle = style.color;
    ctx.clearRect(0, 0, w, h);

    let bw  = 1;
    let bw2 = bw / 2;

    ctx.beginPath();
    ctx.lineWidth = bw;
    ctx.moveTo(bw2, bw2);
    ctx.lineTo(w - bw2, bw2);
    ctx.lineTo(w - bw2, h - bw2);
    ctx.lineTo(bw2, h - bw2);
    ctx.closePath();
    ctx.stroke();

    // The cross is thicker
    let pad = 1;
    let lw  = 1;
    let hlw = (lw / 2);

    // coords of the outside of the line thickness
    let tlx = bw + pad;
    let tly = tlx;
    let brx = w - bw - pad;
    let bry = h - bw - pad;

    // inner size
    let iw  = brx - tlx;
    let ih  = bry - tly;
    let cy  = (tly + ih / 2);
    let cx  = (tlx + iw / 2);

    ctx.beginPath();
    ctx.lineWidth = lw;
    ctx.moveTo(tlx, cy);
    ctx.lineTo(brx, cy);

    if (!open)
    {
        ctx.moveTo(cx, tly);
        ctx.lineTo(cx, bry);
    }

    ctx.stroke();
}




function findContainer(elem, nodeName)
{
    // Find the innermost container element with the given node name.

    while (elem && elem.nodeName !== nodeName)
    {
        elem = elem.parentElement;
    }

    return elem;
}



function findId(id)
{
    return document.getElementById(id);
}



function findElemBySelector(selector, doc)
{
    /*  This is used by killitContent.js when highlighting and killing
        a single element.
    */
    if (isXPath(selector))
    {
        // evaluate() returns an XPathResult.
        let result = document.evaluate(selector, doc.documentElement, null,
                            XPathResult.FIRST_ORDERED_NODE_TYPE, null);

        return result.singleNodeValue;
    }

    return document.querySelector(selector);
}



function isXPath(selector)
{
    return selector.length > 1 && selector[0] === "/";
}



function getRootPositionForElement(elem)
{
    /*  Add up the offset top and left positions for the element up to
        the root element and return an object with: left, top, width, height

        If any of the parents have a fixed position then set the fixed flag.
        The offsets are probably still correct but wrt the viewport.
    */
    let doc   = elem.ownerDocument;
    let top   = 0;
    let left  = 0;
    let fixed = false;
    let e     = elem;

    while (e && e !== doc.documentElement)
    {
        left += e.offsetLeft;
        top  += e.offsetTop;

        let style = doc.defaultView.getComputedStyle(e);
        fixed |= style.getPropertyValue("position") === "fixed";

        e = e.offsetParent;
    }


    return {
        left:   left,
        top:    top,
        width:  elem.offsetWidth,
        height: elem.offsetHeight,
        fixed:  fixed
        };
}



function getSelectorForElement(elem)
{
    /*  If the element has a unique ID then return a CSS-style selector
        otherwise we create a full XPath.

        If the element has some class names and they uniquely select this element
        then use them.
    */
    if (elem.id)
    {
        return "#" + elem.id;
    }

    if (elem.classList.length > 0)
    {
        let sel = "";

        for (let i = 0; i < elem.classList.length; ++i)
        {
            sel += "." + elem.classList[i];
        }

        let found = elem.ownerDocument.querySelectorAll(sel);

        if (found.length === 1 && found[0] === elem)
        {
            kUtils.debug("getSelectorForElement: using", sel);
            return sel;
        }
    }

    return getXPathForElement(elem);
}



function getXPathForElement(elem)
{
    // This only works in the HTML namespace and only for elements.
    let doc = elem.ownerDocument;

    if (elem === doc.documentElement)
    {
        // Stop the recursion
        return "/" + elem.nodeName;
    }
    else
    if (elem.id)
    {
        // We can jump straight to the id skipping the rest of the path upwards.
        return "//" + elem.nodeName + "[@id='" + elem.id + "']";
    }
    else
    {
        /*  The parent element may have more than one of these.
            The first one is at pos=1.

            We always need to index the element even if there is only
            one to ensure that we get a single result. Remember that
            more elements may be added later.

        */
        let xpath = getXPathForElement(elem.parentNode) + "/" + elem.nodeName;

        let pos   = 0;
        let found = false;
        let elems = elem.parentNode.children;

        for (let i = 0; !found && i < elems.length; ++i)
        {
            let e = elems[i];

            if (e.nodeName === elem.nodeName)
            {
                // If it is ELEMENT_NODE of the same name
                pos += 1;
            }

            if (e === elem)
            {
                found = true;
            }
        }

        xpath += "[" + pos + "]";
        return xpath;
    }
}

/* Old code from Mozilla

        xpath = "*[name()='" + elem.nodeName +
                "' and namespace-uri()='" +
                (elem.namespaceURI === null ? '' : elem.namespaceURI) +
                "'][" + pos + ']' + '/' + xpath;

    xpath = '/' + '*' + "[name()='" + doc.documentElement.nodeName +
            "' and namespace-uri()='" +
            (elem.namespaceURI === null ? '' : elem.namespaceURI) +
            "']" + '/' + xpath;

*/



function mkLevel(elem)
{
    /*  Construct an object describing an element to kill.
    */
    let doc = elem.ownerDocument;
    let url = kUtils.trimURL(doc.documentURI);
    let sel = getSelectorForElement(elem);

    // The position relative to the viewport
    let bounds = elem.getBoundingClientRect();

    let level = {
        selector:       sel,
        docURL:         url,
        docTitle:       doc.title,
        id:             elem.id,
        descr:          isXPath(sel)? classifyElem(elem) : sel,
        position:       getRootPositionForElement(elem),
        viewLeft:       bounds.left,
        viewRight:      bounds.right,
        viewTop:        bounds.top,
        viewBottom:     bounds.bottom,
        winWidth:       doc.documentElement.offsetWidth,
        winHeight:      doc.documentElement.offsetWidth
        };

    return level;
}


//======================================================================

return {
    appendText:         appendText,
    classifyElem:       classifyElem,
    clearDOMChildren:   clearDOMChildren,
    create:             create,
    drawOpenClosed:     drawOpenClosed,
    findContainer:      findContainer,
    findElemBySelector: findElemBySelector,
    findId:             findId,
    getRootPositionForElement: getRootPositionForElement,
    getSelectorForElement: getSelectorForElement,
    isXPath:            isXPath,
    mkLevel:            mkLevel,
    newId:              newId,
    removeElem:         removeElem,
    toArray:            toArray,
    };

}());


// Make it work as an SDK module too
if (typeof exports !== "undefined")
{
    // Re-export without redefining the exports object.
    for (let p in kDOM)
    {
        exports[p] = kDOM[p];
    }
}
