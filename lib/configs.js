
/*  We save the kill configs in simple storage under the name 'configs'.
    Each config can be imported from a RIP-compatible xml file or one
    of our own JSON files.

    In the store we have a map of groups. The key is a UUID.
    We also:
        some UI information saved in the 'ui' property.
        preferences in the 'prefs' property.

    {
    "8778c024-804f-442b-b624-138783fcae6b":
        {
            formatVersion: 2,           // data format version
                                        // see also kUtils.CURR_GROUP_VERSION

            // e.g. "8778c024-804f-442b-b624-138783fcae6b",
            // This uniquely identifies the group in some repository.
            uuid:  STRING,

            // 'imported' - hasn't been changed since it was imported
            // 'modified' - modified from its 'imported' state
            // 'local'    - was originated locally
            state:   'local', 'imported', 'modified'

            // True if this group is to be used while browsing.
            enabled:      BOOL,

            // This is a name which could be used as a file name. The
            // special name ANON_GROUP_NAME is used by the anonymous (unsorted) group.
            name:  STRING,

            contactEMail:  STRING,
            contentVersion: STRING,     // only informative, format n.m
            comment:      STRING,       // only informative REVISIT

            // A Date.now() value 
            lastModified: INTEGER,

            parts: ARRAY
        }
    }

    Each part is:
    {
        // This uniquely identifies the part to make it easy to merge parts.
        uuid:  STRING,

        title:   STRING,        // a title for the part, not required

        // This has wildcard patterns to match against a URL.
        // The query and fragment are omitted. We assume there is no port or auth stuff.
        // urlPattern can be null to match all pages.
        urlPattern:
            {
                scheme: STRING,
                host:   STRING,
                path:   STRING,
            },

        // True if the user has enabled this part
        enabled: BOOL,

        // True if the content appears to be usable.
        validated:    BOOL,

        // A selector is an XPath if it starts with a slash otherwise it's
        // a CSS selector.
        selectors: [
            {
                comment: STRING,
                selector: STRING,
                method:  STRING   // one of "hide", "delete"
            }, ...
            ]
    }

    A group would typically correspond to a config file.
    The titles don't have to be unique within a scope.

    Config Format Versions
    ======================

    1 - the original
    2 - Added the validated field to groups. This should be upgraded automatically
        by the UI.

    (Don't forget to update the metadata for validation).

    Store.ui
    ========

    The store.ui property contains some ordering information and flags.
    It is private to and maintained by the dlgGroupEditContent script.

    {
        opened: MAP from UUID to BOOL,  // for groups and parts
        groupOrder: ARRAY of UUID
    }

    Store.dialogSize
    ================

    This saves the dialog window size.

    Store.prefs
    ===========

    The store.prefs property contains miscellaneous preferences.

    {
        theme:              STRING      // current light vs dark
    }

    Store.catalog
    =============

    This holds an array of extracts of groups that are available to be installed. 
    The array entries are objects with

    {
        uuid:  STRING,
        name:  STRING,

        contactEMail:  STRING,
        contentVersion: STRING,     // only informative
        comment:      STRING,       // only informative REVISIT

        // A Date.now() value 
        lastModified: INTEGER,

        url:    STRING              // where it can be downloaded from
    }

    Events
    ======

    A groupsChanged event is emitted with a name to indicate which agent
    made the change. The event carries info:

    {
        changed:        list of group objects of changed or newly-added groups
        removed:        list of uuids of removed groups
        agent:          name of the agent
    }

    prefsChanged

    catalogLoaded


    Backups
    =======

    {
        backupVersion: 1,       // data format version

        // These are in the order in store.ui.groupOrder
        groups: ARRAY
    }

*/

const window  = require("sdk/addon/window").window;
const selfmod = require("sdk/self");
const storage = require("sdk/simple-storage");
const xhr     = require("sdk/net/xhr");
const neturl  = require("sdk/net/url");
const {env}   = require('sdk/system/environment');

const {EventTarget} = require("sdk/event/target");
const {emit}        = require('sdk/event/core');

const store  = storage.storage;

const kUtils = require("./kUtils");
const kMeta  = require("./kMeta");

const {
    ANON_GROUP_NAME, ANON_GROUP_UUID,
    createUUID,
    debug, debugLevel, log, logException,
    showObj,
    } = kUtils;

// A timeout for fetching configs in seconds.
const reqTimeout = 20;

const configPort = EventTarget();

//======================================================================

storage.on("OverQuota", overQuota);


function init()
{
    if (env.RESET_TEST_DATA)
    {
        store.configs = null;
    }

    if (store.configs === null || typeof store.configs !== "object")
    {
        store.configs    = {};
        store.catalog    = []
        store.ui         = null;
        store.dialogSize = null;
        initPrefs();
    }

    // Ensure we have an anon group no matter what
    let anon = store.configs[ANON_GROUP_UUID];

    if (!anon)
    {
        // There won't be listeners to the changed event at this stage.
        updateGroup(kUtils.groupSkeleton(ANON_GROUP_UUID), kUtils.AGENT_SYSTEM);
    }

    for (let uuid in store.configs)
    {
        upgradeVersion(store.configs[uuid]);
    }

    //debug("after init", JSON.stringify(store.configs, undefined, 4));
}



function initPrefs()
{
    /*  Create the initial prefs object.
        See also restoreBackup().
    */
    store.prefs = {
        theme:              "light",
        debugLevel:         1           // REVISIT
        }
}



function getConfigs()
{
    return {
        configs:    store.configs,
        ui:         store.ui,
        prefs:      store.prefs
        };
}



function getCatalog()
{
    /*  Return the catalog and other useful information.
    */
    let uuids     = Object.keys(store.configs);
    let installed = {};

    uuids.forEach((u) =>
    {
        let grp = store.configs[u];

        installed[u] = {
            version:    grp.contentVersion
            };
    });

    return {
        catalog:    store.catalog,
        installed:  installed,
        };
}



function getPrefs()
{
    return store.prefs;
}



function setPref(name, value)
{
    store.prefs[name] = value;
    debug("configs setPref", name, value);
    emit(configPort, "prefsChanged");
}



function testDump()
{
    // Return an array of the groups.
    let result = [];

    for (let u in store.configs)
    {
        result.push(store.configs[u]);
    }

    return result;
}



function getConfigByUUID(uuid)
{
    return store.configs[uuid] || null;
}



function getPartsForURL(url)
{
    /*  From the enabled groups select the enabled usable parts that match the URL.
        We return an array of objects: {groupName, groupUUID, part}
    */
    let result = [];
    let myURL  = kUtils.myURL(url);

    for (let id in store.configs)
    {
        let g = store.configs[id];

        if (g.enabled)
        {
            g.parts.forEach((part) =>
            {
                if (part.enabled && part.validated &&
                    kUtils.myURLmatch(part.urlPattern, myURL))
                {
                    result.push(
                    {
                        groupName: g.name,
                        groupUUID: g.uuid,
                        part:      part
                    });
                }
            });
        }
    }

    return result;
}



function saveConfigs(changed, removed, agent)
{
    /*  changed is an array of the changed or new groups. It may
        be empty in the case that we are using the 'groupsChanged' event
        for synchronising. We always emit the event even if nothing
        has changed so that we can synchronise parts of the add-on. See
        updateGroup() in configs.js.

        removed is an array of the uuids of the removed groups.
    */
    for (let i = 0; i < changed.length; ++i)
    {
        let g = changed[i];
        store.configs[g.uuid] = g;
    }

    removed = removed || [];
    removed.forEach(id => delete store.configs[id]);

    debug("storage quota", storage.quotaUsage * 100, "%");

    emitChanged(changed, removed, agent);
}



function saveDialogSize(size)
{
    store.dialogSize = size;
}



function getDialogSize()
{
    return store.dialogSize;
}



function saveUI(ui)
{
    store.ui = ui;
}



function updateGroup(group, agent)
{
    store.configs[group.uuid] = group;
    emitChanged([group], [], agent);
    //debug("after updateGroup", JSON.stringify(store.configs, undefined, 4));
}



function emitChanged(changedGroups, removedUUIDs, agent)
{
    if (kUtils.debugLevel)
    {
        debug("groupsChanged: changed", showObj(changedGroups), "removed", showObj(removedUUIDs), "by", agent);
    }

    emit(configPort, "groupsChanged",
        {
            changed: changedGroups,
            removed: removedUUIDs,
            agent:   agent
        });
}



function mkBackup()
{
    /*  Generate a backup object. This may not be accurate if the dialog has the
        the group editor pane running.
    */
    let backup = {
        backupVersion: 1,
        groups: []
        };

    // First put the known groups in order.
    let order = store.ui.groupOrder;
    order.forEach((uuid) => backup.groups.push(store.configs[uuid]));

    // Put the rest of the groups at the end.
    for (let uuid in store.configs)
    {
        if (order.indexOf(uuid) < 0)
        {
            backup.groups.push(store.configs[uuid]);
        }
    }

    //debug("mkBackup", JSON.stringify(backup, undefined, 4));
    return backup;
}



function restoreBackup(backup, agent)
{
    /*  Ensure that the backup is legal. It must pass the metadata check and also
        have an ANON group. Return true if the restore was done.
    */
    let ok = backup.backupVersion === 1  &&
             backup.groups.some((group) => group.uuid === ANON_GROUP_UUID);

    if (ok)
    {
        // Upgrade the groups before checking the metadata.
        backup.groups.forEach((group) => upgradeVersion(group));
        ok = backupMeta.validate(backup);
    }

    if (ok)
    {
        // Replace store.configs and store.ui.groupOrder.
        let oldUuids = Object.keys(store.configs);

        store.configs = {};
        store.ui.groupOrder = [];
        debug("restoreBackup 2");

        backup.groups.forEach((group) =>
        {
            store.configs[group.uuid] = group;
            store.ui.groupOrder.push(group.uuid);
        });

        // The removed uuids are those in oldUuids that are not in the store.
        let removed = oldUuids.filter((uuid) => typeof store.configs[uuid] === "undefined");

        debug("restoreBackup 3");
        emitChanged(backup.groups, removed, agent);
    }

    return ok;
}



function groupInfo(group)
{
    // Extract some useful info about a group that we can pass around.
    // REVISIT - not used anymore
    if (typeof group === "string")
    {
        group = store.configs[group];   // it's a uuid
    }

    return {
        uuid:           group.uuid,
        lastModified:   group.lastModified,
        state:          group.state,
        version:        group.contentVersion
        };
}



function addToAnon(level, method, which, agent)
{
    /* The level arg is the result from mkLevel().
       which is "page" or "site"
    */
    debug("addToAnon");

    try
    {
        let urlPattern = kUtils.myURL(level.docURL);
        let title;

        switch (which)
        {
        case "page":
            title = level.docTitle;
            break;

        case "site":
            //  Replace a www prefix with *.
            urlPattern.host = urlPattern.host.replace(/^www\./i, "*.");
            urlPattern.path = "";
            title = urlPattern.host;
            break;

        default:
            throw("addToAnon: Invalid 'which' parameter: " + which);
            break;
        }

        // See if there is a part with the same urlPattern.
        let anon  = store.configs[ANON_GROUP_UUID];
        let parts = anon.parts;
        let part  = null;

        for (let i = 0; i < parts.length; ++i)
        {
            if (kUtils.myURLsame(parts[i].urlPattern, urlPattern))
            {
                part = parts[i];
                break;
            }
        }

        if (!part)
        {
            // Create a new part
            part = kUtils.partSkeleton();
            anon.parts.push(part);

            part.title      = title;
            part.urlPattern = urlPattern;
            part.enabled    = true;
            part.validated  = true;
        }

        let sel      = kUtils.selectorSkeleton();
        sel.selector = level.selector;
        sel.method   = method;

        part.selectors.push(sel);
        updateGroup(anon, agent);
    }
    catch(e)
    {
        logException(e);
    }
}



function mergeGroupToAnon(newGroup, agent)
{
    /* The group comes from a previously export of ANON. Merge it
       into ANON.
    */
    let anon  = store.configs[ANON_GROUP_UUID];
    let parts = anon.parts;

    // This updates parts in place.
    newGroup.parts.forEach((p) => mergePart(parts, p));

    updateGroup(anon, agent);
}



function mergePart(oldParts, newPart)
{
    // This updates oldParts in place.
    let existing  = null;

    // See if there is a part with the same urlPattern.
    for (let i = 0; i < oldParts.length; ++i)
    {
        if (kUtils.myURLsame(oldParts[i].urlPattern, newPart.urlPattern))
        {
            existing = oldParts[i];
            break;
        }
    }

    if (existing)
    {
        /*  Merge in the selectors. When two selectors are identical choose
            the new one. part is an object by reference.
        */
        newPart.selectors.forEach((newSel) =>
        {
            let oldSels = existing.selectors;
            let found   = false;

            for (let j = 0; !found && j < oldSels.length; ++j)
            {
                if (oldSels[j].selector === newSel.selector)
                {
                    oldSels[j] = newSel;    // copy by reference
                    found = true;
                }
            }

            if (!found)
            {
                oldSels.push(newSel);
            }
        });
    }
    else
    {
        oldParts.push(newPart);
    }
}



function overQuota()
{
    log("over storage quota", storage.quotaUsage * 100, "%");
}

//======================================================================

function readURL(url, groupName, then)
{
    /*  Read from a http or a file. This is done asynchronously.

        We expect to read a JSON encoding of a group object or an XML
        encoding of a RIP config. The RIP config will be given the
        groupName along with a new UUID.

        REVISIT - Reading the same RIP file multiple times will result
        in multiple groups with the same name.

        The then function will be called with a JSONable result object
        when it is complete.  The result object will be

            {error: false, group: new group object}
        or
            {error: <error message>, url: STRING}

        The returned group is not yet saved in the store.

        REVISIT - If we are importing the anon group then preserve its
        essentials such as the enabled flag.

        REVISIT - Don't allow importing a file with a duplicate UUID unless it
        is the anon group in which case we could merge the parts

    */
    let group = null;
    let req   = new xhr.XMLHttpRequest();

    req.timeout = reqTimeout;

    req.onload = function()
    {
        let configs = null;

        if (req.status === 0)
        {
            // Probably a local file. See if we can read it as JSON or XML.
            group = parseUnknown(req.responseText, groupName);
        }
        else
        if (req.status === 200)
        {
            // a web request or a resource: URL
            debug("readURL: got type: '" + req.responseType + "'");

            if (req.responseType === "document")
            {
                group = handleDoc(req.responseXML, groupName);
            }
            else
            {
                //debug("readURL: got text:", req.responseText);
                group = parseUnknown(req.responseText, groupName);
            }
        }


        if (!group)
        {
            group = {error: "Failed to read", url: url};
        }
        else
        if (group instanceof Error)
        {
            group = {error: group.toString(), url: url}
        }
        else
        {
            upgradeVersion(group);

            if (!legalGroup(group))
            {
                group = {error: "Invalid kill file", url: url}
            }
            else
            {
                group = {error: false, group: group};
            }
        }

        debug("got configs", showObj(group));
        then(group);
    };

    req.onerror = function(e)
    {
        let result = new Error("Failed to read the document");
        result.url = url;
        then(result);
    };

    req.ontimeout = function()
    {
        let result = new Error("Time-out while reading the document");
        result.url = url;
        then(result);
    };

    req.open("GET", url, true);
    req.send();
}



function parseUnknown(text, groupName)
{
    let json = null;

    try
    {
        json = JSON.parse(text);
    }
    catch(e)
    {
        /*  Try it as XML. Note that there may not be an exception.
            It may return a parsererror node.
        */
        try
        {
            let parser = new window.DOMParser();
            let doc = parser.parseFromString(text, "application/xml");
            json = handleDoc(doc, groupName);
        }
        catch(e2)
        {
            json = new Error("XML syntax error");
        }
    }

    return json;
}



function handleDoc(doc, groupName)
{
    // doc is an XML Document. It should have the RIP format.
    let root = doc.documentElement;

    if (root.nodeName === "PARSERERROR")
    {
        return new Error("XML syntax error");
    }

    return ripToKillit(root, groupName);
}


function ripToKillit(root, groupName)
{
    /*  Convert the RIP format to ours. The argument
        is the root DOM node which should be 'Config'. It will be
        an Element.

        For example:

        <Config version="1.0">
                <Page name="the age 2" url="*.theage.com.au/" enabled="true">
                        <XPath comment="">//iframe</XPath>
                ...

        Each Page node will become one of our config objects.
        The result will be an array of these or an error.

        We leave the individual parts enabled but the new group
        disabled. The user should review the RIP before enabling it.
    */
    let parts = [];

    try
    {
        if (root.nodeName === "Config" && root.getAttribute("version") === "1.0")
        {
            for (let i = 0; i < root.children.length; ++i)
            {
                let page = root.children[i];
                let part = kUtils.partSkeleton();

                part.title      = page.getAttribute("name");
                part.urlPattern = kUtils.myURL(page.getAttribute("url"));
                part.enabled    = page.getAttribute("enabled") === "true";
                part.validated  = true;
                part.selectors  = [];

                for (let j = 0; j < page.children.length; ++j)
                {
                    let xpnode = page.children[j];
                    let sel = {
                        comment: xpnode.getAttribute("comment"),
                        selector: xpnode.textContent,
                        method:  kUtils.METHOD_HIDE
                        };

                    part.selectors.push(sel);
                }

                parts.push(part);
            }
        }
    }
    catch(e)
    {
        return new Error("Invalid RIP configuration: " + e);
    }

    if (parts.length > 0)
    {
        let skel     = kUtils.groupSkeleton();
        skel.name    = groupName;
        skel.parts   = parts;
        skel.enabled = false;
        return skel;
    }

    return null;
}



const selectorMeta = kMeta.object({
    comment:        kMeta.string(),
    selector:       kMeta.string().notEmpty(),
    method:         kMeta.choice(["hide", "delete"]).notEmpty(),
    });

const urlMeta = kMeta.object({
    scheme:         kMeta.string(),
    host:           kMeta.string(),
    path:           kMeta.string()
    });

const partMeta = kMeta.object({
    uuid:           kMeta.string().notEmpty(),
    title:          kMeta.string(),
    urlPattern:     urlMeta,
    enabled:        kMeta.boolean(),
    validated:      kMeta.boolean(),
    selectors:      kMeta.array(selectorMeta)
    });

const groupMeta = kMeta.object({
    formatVersion:  kMeta.number(),
    name:           kMeta.string().notEmpty(),
    uuid:           kMeta.string().notEmpty(),
    state:          kMeta.choice(["local", "imported", "modified"]),
    enabled:        kMeta.boolean(),
    validated:      kMeta.boolean(),
    contactEMail:   kMeta.string(),
    contentVersion: kMeta.string(),
    comment:        kMeta.string(),
    lastModified:   kMeta.number(),
    parts:          kMeta.array(partMeta)
    });

const backupMeta = kMeta.object({
    backupVersion:  kMeta.number(),
    groups:         kMeta.array(groupMeta)
    });



function legalGroup(group)
{
    if (group === null)
    {
        return false;
    }

    if (debugLevel())
    {
        debug("legalGroup", JSON.stringify(group, undefined, 4));
    }

    return groupMeta.validate(group);
}



function serialise(uuid)
{
    /*  Serialise the group to JSON.
    */
    let group = store.configs[uuid];
    let text  = "";

    if (group)
    {
        debug("serialise group", showObj(group));
        text = JSON.stringify(group, undefined, 4);
    }

    return text;
}



function upgradeVersion(group)
{
    //  Upgrade the group format in place.

    if (group.formatVersion === 1)
    {
        // The UI will have to compute this accurately.
        group.validated = true;
        group.formatVersion = 2;
    }
}


//======================================================================
// The Catalog

function isCatalogLoaded()
{
    return store.catalog.length > 0;
}



function loadCatalog(reason)
{
    /*  Load the catalog from the add-on data. This will be done asynchronously.
        The catalogLoaded event will be sent when this is done.

        It happens that we can read the catalog/ directory and will get back 
        a directory listing in http-index-format. e.g.

            200: filename content-length last-modified file-type
            201: Hand_with_knife.svg 4031 Wed,%2016%20Jul%202014%2009:03:44%20GMT FILE

        We can probably rely on this rather than using our own manifest file.
    
        REVISIT In the future we will also want to get catalog info from the web.
    */
    let loadIt = reason === "install" ||
                 reason === "upgrade" || 
                 env.RELOAD_CATALOG   || 
                 env.RESET_TEST_DATA  ||
                 (store.catalog && store.catalog.length === 0);

    if (!loadIt)
    {
        return;
    }

    debug("loading the catalog");
    store.catalog = [];

    try
    {
        let url = selfmod.data.url("catalog/"); // the trailing slash is required

        neturl.readURI(url).then(
            function success(contents)
            {
                //log("loadCatalog catalog got", typeof contents, contents);
                let lines    = contents.split("\n");
                let manifest = [];
                let ext      = "." + kUtils.KILL_EXT;

                lines.forEach((line) =>
                {
                    let parts = line.split(/\s+/);

                    if (parts.length > 1 && parts[0] === "201:")
                    {
                        // This may have %20 escaping.
                        let file = parts[1];

                        if (file.substr(file.length - ext.length) === ext)
                        {
                            log("loadCatalog file", file);
                            manifest.push(file);
                        }
                    }
                });

                loadCataManifest(manifest);
            },
            function failure(msg)
            {
                log("loadCatalog failure", msg);
            });
    }
    catch(e)
    {
        logException(e);
    }
}



function loadCataManifest(manifest)
{
    function loadOne()
    {
        if (manifest.length > 0)
        {
            let url = selfmod.data.url("catalog/" + manifest.shift());

            neturl.readURI(url).then(

                function success(contents)
                {
                    //log("loadCatalog got", typeof contents, contents);

                    try
                    {
                        let group = JSON.parse(contents);
                        upgradeVersion(group);

                        let entry = {
                            uuid:           group.uuid,
                            name:           group.name,
                            contactEMail:   group.contactEMail,
                            contentVersion: group.contentVersion,
                            comment:        group.comment,
                            lastModified:   group.contactEMail,
                            url:            url
                            };

                        log("loadCatalog entry", showObj(entry));
                        store.catalog.push(entry);
                    }
                    catch(e)
                    {
                        logException(e, "while parsing " + url);
                    }

                    loadOne();
                },

                function failure(msg)
                {
                    log("loadCatalog failure", msg);
                    loadOne();
                }
            );
        }
        else
        {
            emit(configPort, "catalogLoaded");
        }
    }

    loadOne();
}


//======================================================================

exports.addToAnon           = addToAnon;
exports.getCatalog          = getCatalog;
exports.getConfigByUUID     = getConfigByUUID;
exports.getConfigs          = getConfigs;
exports.getDialogSize       = getDialogSize;
exports.getPartsForURL      = getPartsForURL;
exports.getPrefs            = getPrefs;
exports.init                = init;
exports.isCatalogLoaded     = isCatalogLoaded;
exports.loadCatalog         = loadCatalog;
exports.mergeGroupToAnon    = mergeGroupToAnon;
exports.mkBackup            = mkBackup;
exports.port                = configPort;
exports.readURL             = readURL;
exports.restoreBackup       = restoreBackup;
exports.saveConfigs         = saveConfigs;
exports.saveDialogSize      = saveDialogSize;
exports.saveUI              = saveUI;
exports.serialise           = serialise;
exports.setPref             = setPref;
exports.testDump            = testDump;
