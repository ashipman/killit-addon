"use strict";

const cmenu    = require("sdk/context-menu");
const {env}    = require('sdk/system/environment');
const tabs     = require("sdk/tabs");
const pageMod  = require("sdk/page-mod");
const self     = require("sdk/self");
const Panel    = require("sdk/panel").Panel;
const Private  = require("sdk/private-browsing");
const _        = require("sdk/l10n").get;
const timers   = require("sdk/timers");
const urlSdk   = require("sdk/url");
const winutils = require("sdk/window/utils");

const kMisc    = require("./kMisc");
const kUtils   = require("./kUtils");

const {assert, debug, log, logException, showElem, showObj, trimURL} = kUtils;

// We need at least version 30 for the new UI features.
// We need at least version 29 for predicate contexts.
// We aren't using predicate contexts at the moment so allow v28.
assert(kMisc.mozVersion >= 28);

let newUI = kMisc.mozVersion >= 30;

const activity = require("./activity");
const kDOM     = require("./kDOM");
const kWorkers = require("./kWorkers");
const dlgMain  = require("./dlgMain");
const configs  = require("./configs");
const {setIframeCallback, setTestCallback} = require("./kWindows");

var data = self.data;

// To listen for config change events
configs.port.on("groupsChanged", groupConfigsChanged);
configs.port.on("prefsChanged",  configPrefsChanged);

//======================================================================

pageMod.PageMod(
{
    include: ["*", "file://*"],

    contentScriptFile: [
        data.url("../lib/kUtils.js"),
        data.url("../lib/kDOM.js"),
        data.url("killitContent.js")
        ],

    // Attach once the document HTML has been loaded.
    contentScriptWhen: "ready",

    // Attach to each iframe too
    attachTo: ["existing", "top", "frame"],

    onAttach: function(worker)
    {
        /*  For each frame we will get a worker. According to the source in
            sdk/content/worker.js the url will be window.document.location.href.
            So all workers for the same window will have the same url. But this
            also includes pages that have been replaced!

            We also record undo information in an undo attribute.
        */
        let self = worker;
        let url  = worker.url;
        let num  = kWorkers.nextSerial();

        worker.realURL     = trimURL(url);
        worker.documentURL = trimURL(worker.tab.url);
        worker.serialNum   = num;

        kWorkers.addWorker(worker);

        worker.port.emit("init", {
            serialNum:  num,
            debugLevel: kUtils.debugLevel(),      // this can change from page to page
            });

        worker.port.on("canUndo", (flag) => worker.canUndo = flag);
        worker.port.on("requestConfigs", () => setConfigs(worker));
        worker.port.on("killActivity", activity.logActivity);
    },

    onError: logException
});



function setConfigs(worker)
{
    let parts = configs.getPartsForURL(worker.realURL);
    try
    {
        worker.port.emit("setConfigs", parts);
    }
    catch(e)
    {
        /*  Ignore unreachable workers (probably they are hidden)
            Hidden workers will request their configs via requestConfigs
            above.
        */
        //logException(e);
    }
}



function groupConfigsChanged(info)
{
    //  Reinstall the configs for each worker.
    kWorkers.findWorkers().forEach(setConfigs);
}



function configPrefsChanged()
{
    /*  At the moment the prefs are only applicable to the dialog.
    */
    debug("configPrefsChanged");
    let prefs = configs.getPrefs();
    dlgMain.updatePrefs(prefs);
}

//======================================================================

const CHOOSE_WIDTH  = 280;
const CHOOSE_HEIGHT = 345;

const ICON_URL  = data.url("Hand_with_knife24.png");
const ICONX_URL = data.url("Hand_with_knife24_transp.png");

// The toolbar button by any UI.
var gWidget;

// The items in the context menu.
var gContextKill;
var gContextUndo;

var gChoosePanel;
var gContextInfo;        // see contextCmd
var gRecentKill;         // see chooseClickLevel
var gContextWorkers;     // see contextCmd
var gIFrameElem;
var gUndoURLs = [];      // top-level documents where an undo could happen

function createUI()
{
    if (newUI)
    {
        let ActionButton = require('sdk/ui/button/action').ActionButton;

        gWidget = ActionButton({
            id:         "killit",
            label:      "Kill It",
            icon:       ICONX_URL,
            onClick:    (state) => dlgMain.open()
            });
    }
    else
    {
        gWidget = require("sdk/widget").Widget({
            /*  The id must not be changed since it identifies
                the widget in the toolbar and this persists.
            */
            id:    "killit",
            label: "Kill It",
            contentURL: ICONX_URL,
            onClick: dlgMain.open
            });
    }

    /*  A page context would be confusing since the menu would
        disappear over some parts of the page.
    */
    let urlContext = cmenu.URLContext(["*", "file://*"]);

    gContextKill = cmenu.Item({
        label:   _("menu.killit"),
        context: urlContext,

        data:   JSON.stringify({        // Gak!
                    debugLevel: kUtils.debugLevel()   // this is ignored
                    }),

        contentScriptFile: [
            data.url("../lib/kUtils.js"),
            data.url("../lib/kDOM.js"),
            data.url("contextKillContent.js")
            ],

        onMessage: contextCmd
        });

    /*  This is popped up from the context menu. It presents the 
        elements to choose from to kill.
    */
    let chooseOptions = {
        thisSiteWhich:  "*.example.com",
        iconURL:        ICONX_URL,
        };

    gChoosePanel = Panel(
        {
            width:  CHOOSE_WIDTH,
            height: CHOOSE_HEIGHT,

            contentURL: data.url("choosePanel.html"),

            contentScriptFile: [
                data.url("../lib/kUtils.js"),
                data.url("../lib/kDOM.js"),
                data.url("chooseContent.js")
                ],
            contentScriptOptions: chooseOptions,

            onHide:    onChooseHide
        });

    gChoosePanel.port.on("selectLevel", chooseSelectLevel);
    gChoosePanel.port.on("clickLevel",  chooseClickLevel);
    gChoosePanel.port.on("undo",        chooseUndo);
    gChoosePanel.port.on("install",     chooseInstall);
}


//======================================================================


function contextCmd(info)
{
    /*  This is called when an element is clicked on using the context menu.
        See contextContent.js for the contents of info.

        The user may have navigated to a different top-level window for
        which there is a different set of workers that may or may not
        have a pending undo. We have to find these workers again.

        At the moment we find these workers according to the URL of
        the top-level document.  This may be loaded into more than one
        top-level window so we end up operating on all of these top-level
        windows at once. This is a quirk, perhaps a feature.

    */
    if (kMisc.debugLevel())
    {
        debug("contextCmd", showObj(info));
    }

    setContextInfo(info);
    gContextWorkers = setContextWorkers(gContextInfo);

    /*  gIFrameElem was set when the context menu popped up.
        Add it as another level but it will be in a different document.
    */
    if (gIFrameElem)
    {
        gContextInfo.levels.push(kDOM.mkLevel(gIFrameElem));
    }

    // Translate the level descriptions.
    gContextInfo.levels.forEach((level) =>
    {
        level.descr = _(level.descr);
    });

    let levels    = info.levels;
    let numLevels = levels.length;

    // Tell the choose panel what levels are available.
    gChoosePanel.port.emit("setLevels", levels, checkCanUndo(), checkCanSite());

    if (numLevels > 0)
    {
        /*  See if we can place the panel to avoid overlapping levels.
            If we can't then try to place it in an opposite corner.
        */
        let pos;

        for (let i = numLevels - 1; !pos && i >= 0; --i)
        {
            pos = tryPos(levels[i]);
        }

        if (!pos)
        {
            let first = levels[0];
            let left  = 0;
            let top   = 0;
            let gap   = 10;

            if (first.viewLeft < first.winWidth / 2)
            {
                left = first.winWidth - (CHOOSE_WIDTH + gap);
            }

            if (first.viewTop < first.winHeight / 2)
            {
                top = first.winHeight - (CHOOSE_HEIGHT + gap);
            }

            pos = {top: top, left: left};

            if (kMisc.debugLevel())
            {
                debug("contextCmd push", showObj(pos));
            }
        }

        gChoosePanel.show({position: pos});
    }

    return;
}



function setContextInfo(info)
{
    // We are at the beginning of the kill process. 
    gContextInfo = info;
    gRecentKill  = null;
    gChoosePanel.port.emit("setCanInstall", false);
}



function setRecentKill(levelIndex, method, which)
{
    /*  We have just killed something. We don't allow installing
        in private mode so that private URLs aren't saved.
    */
    gRecentKill = {index: levelIndex, method: method, which: which};
    gChoosePanel.port.emit("setCanInstall", !isPrivate());
}



function isPrivate()
{
    /*  This is the closest approximation we can manage at the moment
        for testing private browsing.  See mozilla Bug 1059624.
    */
    let browser = winutils.getMostRecentBrowserWindow();

    return Private.isPrivate(browser);
}



function tryPos(level)
{
    /*  See if we can choose a position that doesn't overlap the element.
        Favour the top-right corner.
    */
    let left = -1;
    let top  = -1;
    let gap  = 10;
    let wid  = CHOOSE_WIDTH  + gap;         // size of the panel
    let hgt  = CHOOSE_HEIGHT + gap;

    if (wid < (level.winWidth - level.viewRight))
    {
        left = level.viewRight + gap;
        top  = level.viewTop;
    }
    else
    if (wid < level.viewLeft)
    {
        left = level.viewLeft - wid;
        top  = level.viewTop;
    }
    else
    if (hgt < level.viewTop)
    {
        top  = level.viewTop - hgt;
        left = level.viewLeft;
    }
    else
    if (hgt < (level.winHeight - level.viewBottom))
    {
        top  = level.viewBottom + gap;
        left = level.viewLeft;
    }

    if (left >= 0 && top >= 0)
    {
        return {top: top, left: left};
    }

    return null;
}



function setContextWorkers(info)
{
    /*  The gContextInfo has the URL of one of the documents, possibly
        within an iframe. Find all associated workers.
    */
    let realURL = trimURL(info.docURL);
    let reals   = kWorkers.findWorkers(realURL, true);
    let urls    = {};

    // Find the set of top-level documents 
    reals.forEach((worker) =>
    {
        urls[worker.documentURL] = true;
    })

    let workers = [];

    for (let u in urls)
    {
        workers = workers.concat(kWorkers.findWorkers(u));
    }

    debug("setContextWorkers #workers", workers.length);
    return workers;
}



function checkCanUndo()
{
    return gContextWorkers.some((w) => w.canUndo);
}



function checkCanSite()
{
    // See if there is any URL with a http-like scheme and a host name.
    let {docURL, levels} = gContextInfo;

    let urls = [urlSdk.getTLD(docURL)];
    levels.forEach((l) => urls.push(urlSdk.getTLD(l.docURL)));
    let can = urls.some((u) => u !== null);

    if (kMisc.debugLevel())
    {
        debug("checkCanSite", showObj(urls));
    }
    return can;
}



function chooseUndo()
{
    gContextWorkers.forEach((w) =>
    {
        try
        {
            w.port.emit("undo");
        }
        catch(e)
        {
            // Ignore unreachable workers (probably they are hidden)
        }
    });

    updateCanUndo();
}



function updateCanUndo()
{
    /*  Give the workers a little time to do something and see if
        there is anything to undo.
    */
    timers.setTimeout(() =>
    {
        debug("updateCanUndo #workers", gContextWorkers.length);
        let can = gContextWorkers.some((w) => w.canUndo);
        gChoosePanel.port.emit("setCanUndo", can);
    }, 100);
}



function chooseInstall()
{
    /*  Install the recent kill as a new selector in the unsorted group.
        This will propagate to all killit workers via the configs 'changed'
        event. 

        We want the dialog to be updated too if it is running. But
        there may be modified configs in the dialog so we can't just
        tell the dialog to reload everything. We have to tell it the
        newly installed part.

    */
    if (kMisc.debugLevel())
    {
        debug("install", showObj(gRecentKill));
    }

    if (gRecentKill)
    {
        let {docURL, levels} = gContextInfo;
        let {index, method, which} = gRecentKill;

        if (index >= 0 && index < levels.length)
        {
            let level = levels[index];

            dlgMain.updateGroup(kUtils.ANON_GROUP_UUID, () =>
            {
                configs.addToAnon(level, method, which, kUtils.AGENT_CHOOSER);
            });
        }

        gChoosePanel.hide();
    }
}



function sendLevelArgs(command, levelIndex, ...extraArgs)
{
    /*  Find a worker that matches the level parameters and send it a
        request to highlight or kill the element.
        
        This runs within one context menu click and the global gContextInfo 
        has been set by contextCmd().
    */
    let {docURL, levels} = gContextInfo;

    if (levelIndex >= 0 && levelIndex < levels.length)
    {
        let level   = levels[levelIndex];
        let args    = [].concat(command, level, extraArgs);
        let workers = kWorkers.findWorkers(level.docURL, true);

        debug("sendLevelArgs", command, docURL, "#workers", workers.length);

        workers.forEach((w) =>
        {
            try
            {
                w.port.emit.apply(w.port, args);
            }
            catch(e)
            {
                // Ignore unreachable workers (probably they are hidden)
            }
        });
    }
}



function chooseSelectLevel(levelIndex, entering)
{
    //debug("chooseSelectLevel", levelIndex, entering, gContextInfo.levels.length);
    sendLevelArgs(entering? "highlightLevel" : "removeHighlight", levelIndex);
}



function chooseClickLevel(levelIndex, method, which)
{
    /*  A message has arrived from the choose panel to kill something.
        Pass it to each relevant worker. This will kill on each different
        page that happens to have loaded the same URL.

        An undo won't clear the recent kill. This lets us undo it and
        still install it.
    */
    //debug("chooseClickLevel", levelIndex);
    sendLevelArgs("killit", levelIndex, method);
    setRecentKill(levelIndex, method, which);
    updateCanUndo();
}



function onChooseHide()
{
    /*  The choose panel has gone away.
        Tell each worker to remove any highlight.

        The SDK prevents us from sending messages to pages that are
        not on show but it's not easy finding out which are usable.
        We have to have unreachable pages remove their highlight when
        they are hidden.

    */
    let workers = kWorkers.findWorkers();

    workers.forEach((w) =>
    {
        try
        {
            w.port.emit("removeHighlight");
        }
        catch(e)
        {
            //logException(e);
        }
    });
}

//======================================================================

exports.main = function (options, callbacks)
{
    kUtils.setDebugLevel(kMisc.debugLevel());

    configs.init();
    configs.loadCatalog(options.loadReason);

    createUI();
    setIframeCallback((elem) => gIFrameElem = elem);
    setTestCallback(testEventHandler);
}

//======================================================================
// Testing hooks

function testEventHandler(ev)
{
    let mod      = require("./kTesting");
    let detail   = ev.detail;
    let args     = JSON.parse(detail.arg);
    let callback = detail.callback || ((a) => log("test dump is", showObj(a)));

    switch (detail.command)
    {
    case "modelDump":
        dlgMain.modelDump(callback);
        break;

    case "clickToolbarButton":
        callback(mod.clickToolbarButton());
        break;

    case "clickChooseLevel":
        callback(mod.clickChooseLevel(args));
        break;

    case "clickChooseInstall":
        callback(mod.clickChooseInstall());
        break;

    case "clickChooseUndo":
        callback(mod.clickChooseUndo());
        break;

    case "clickContextMenu":
        callback(mod.clickContextMenu.apply(null, args));
        break;

    case "closeDialog":
        dlgMain.close();
        callback(true);
        break;

    case "getConfigs":
        callback(configs.testDump());
        break;

    case "hideChoosePanel":
        gChoosePanel.hide();            // fake it
        callback(true);
        break;

    case "isHidden":
        callback(mod.isHidden(args));
        break;

    case "sendKey":
        callback(mod.sendKey.apply(null, args));
        break;

    case "sendString":
        callback(mod.sendString.apply(null, args));
        break;

    default:
        callback("unrecognised command " + detail.command);
        break;
    }
}
