"use strict";

// Code in this module only works with the require() system.

const prefs  = require("sdk/simple-prefs").prefs;
const system = require("sdk/system");

//======================================================================

var forceDebug = 0;
//forceDebug = 1;

exports.debugLevel    = ()  => Math.max(forceDebug, prefs.debugLevel);
exports.setDebugLevel = (n) => prefs.debugLevel = n;
exports.debugEnable   = ()  => prefs.debugEnable;

//======================================================================

let mozVersion = system.version.replace(/\..*/, "") - 0;

exports.mozVersion = mozVersion;

//======================================================================

let locale   = "US";
let language = "en";

/*  This contains code from sdk/l10n/json/core. It is the only way
    I can find to have code that works for SDK 1.16 and 1.17 since
    the core module has been moved.
*/
try
{
    let data = require("@l10n/data");
    locale   = data.bestMatchingLocale;
    language = locale? locale.split("-")[0].toLowerCase() : null;
}
catch(e)
{
}

exports.local    = locale;
exports.language = language;
