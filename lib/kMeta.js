/*  This does a little metadata for validating JSON.
*/

let kMeta = (function()
{
"use strict";

const kUtils = require("./kUtils");
const {debug, debugLevel, log, logException, showObj} = kUtils;

const debugMeta = false;

//======================================================================

const TYPE_OBJECT  = 1;         // check the "own" properties of an object
const TYPE_ARRAY   = 2;
const TYPE_NUMBER  = 3;
const TYPE_STRING  = 4;
const TYPE_BOOLEAN = 5;
const TYPE_ANY     = 6;
const TYPE_ENUM    = 7;         // one of a set of strings or numbers

const TypeNames = {
    TYPE_OBJECT:    "object",
    TYPE_ARRAY:     "array",
    TYPE_NUMBER:    "number",
    TYPE_STRING:    "string",
    TYPE_BOOLEAN:   "boolean",
    TYPE_ANY:       "any",
    TYPE_ENUM:      "choice",
    };



function Meta(type)
{
    this.type      = type;
    this.objectMap = null;      // metadata for object properties
    this.arrayMeta = null;      // type of array elements
    this.enumNames = null;

    this.required_  = true;      // flag for an object property
    this.notEmpty_  = false;     // for strings and arrays and enums, not empty
    this.validFunc_ = null;      // some other validation function
}



Meta.prototype.optional = function()
{
    this.required_ = false;
    return this;
}



Meta.prototype.notEmpty = function()
{
    this.notEmpty_ = true;
    return this;
}



Meta.prototype.check = function(func)
{
    this.validFunc_ = func;
    return this;
}



Meta.prototype.toString = function()
{
    let s = TypeNames[this.type];

    if (this.required_)
    {
        s += "(required)";
    }

    return s;
}



Meta.prototype.validate = function(value)
{
    /*  With firefox, arrays don't always appear as an instance of Array.
        This seems to happen when passing between sandboxes. An array is
        taken to be anything with a length attribute.
    */
    let ok = false;

    switch (this.type)
    {
    case TYPE_OBJECT:
        // Check that all required properties are present
        if (this.objectMap && typeof value === "object")
        {
            ok = true;

            if (debugMeta)
            {
                debug("kMeta validate object value", showObj(value));
                debug("kMeta validate object meta", showObj(this.objectMap));
            }

            for (let k in this.objectMap)
            {
                let m = this.objectMap[k];

                if (m.required_ && typeof value[k] === "undefined")
                {
                    ok = false;
                    debug("kMeta validation failed: the required property", k, "is missing");
                    break;
                }
            }

            if (ok)
            {
                // Check that each property present matches some metadata
                for (let k in value)
                {
                    let m = this.objectMap[k];
                    if (debugMeta) debug("kMeta validate 2 k=", k, showObj(m));

                    if (!m || !m.validate(value[k]))
                    {
                        ok = false;
                        debug("kMeta validation failed for '" + k + "' value=", showObj(value[k]));
                        break;
                    }
                }
            }
        }
        break;

    case TYPE_ARRAY:
        if (typeof value.length !== "undefined")
        {
            ok = true;

            if (this.arrayMeta)
            {
                for (let i = 0; ok && i < value.length; ++i)
                {
                    ok = ok && this.arrayMeta.validate(value[i]);
                }
            }

            ok = ok && !(this.notEmpty_ && value.length === 0);
        }
        break;

    case TYPE_NUMBER:
        ok = typeof value === "number";
        break;

    case TYPE_STRING:
        ok = typeof value === "string" &&
             !(this.notEmpty_ && value.length === 0);
        break;

    case TYPE_BOOLEAN:
        ok = typeof value === "boolean";
        break;

    case TYPE_ENUM:
        ok = !(typeof value === "string" && this.notEmpty_ && value.length === 0) &&
             (this.enumNames && this.enumNames.some((n) => n === value));
        break;

    case TYPE_ANY:
        ok = true;
        break;

    default:
        throw new Error("Meta.validate");
    }

    if (ok && this.validFunc_)
    {
        try
        {
            ok = ok && this.validFunc_(value);
        }
        catch(e)
        {
            ok = false;
        }
    }

    if (!ok) debug("kMeta validation failed:", showObj(value));
    return ok;
}



function number()
{
    return new Meta(TYPE_NUMBER);
}



function string()
{
    return new Meta(TYPE_STRING);
}



function boolean()
{
    return new Meta(TYPE_BOOLEAN);
}



function any()
{
    return new Meta(TYPE_ANY);
}



function choice(names)
{
    let m = new Meta(TYPE_ENUM);
    m.enumNames = names;
    return m;
}



function object(metaMap)
{
    let m = new Meta(TYPE_OBJECT);
    m.objectMap = metaMap;
    return m;
}



function array(arrayMeta)
{
    let m = new Meta(TYPE_ARRAY);
    m.arrayMeta = arrayMeta;
    return m;
}


//======================================================================

return {
    any:                any,
    array:              array,
    boolean:            boolean,
    choice:             choice,
    number:             number,
    object:             object,
    string:             string,
    };

}());


// For SDK compatibility
if (typeof exports !== "undefined")
{
    // Re-export without redefining the exports object.
    for (let p in kMeta)
    {
        exports[p] = kMeta[p];
    }
}
