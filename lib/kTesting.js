"use strict";

const {Cc, Ci} = require('chrome');

const winutils = require("sdk/window/utils");
const timers   = require("sdk/timers");

const kUtils   = require("./kUtils");
const kWindows = require("./kWindows");
const {log, logException, showElem, showObj} = kUtils;

const KeyEvent = Ci.nsIDOMKeyEvent;

//======================================================================
// Experiments

let mainWindow;

function findMainWindow()
{
    /*  Find the first main window.
        This must run in the top-level add-on environment.
    */
    if (!mainWindow)
    {
        let win = winutils.getMostRecentBrowserWindow();
        let doc = win.document;
        let title = doc.title;
        log("findMainWindow title", title);

        if (title && title.match(/Mozilla Firefox/))
        {
            mainWindow = win;
        }
    }

    return mainWindow;
}



function clickContextMenu(whichWindow, selector, labelRegExp, duration)
{
    /*  Find the window indicated by whichWindow: 'main' or 'dialog'.
        Use selector to find an element in the window. Pop up
        the context menu and locate the entry with a label that matches
        the labelRegExp and click it.

        If duration > 0 then hide the menu after that number of milliseconds.
    */
    let chrome   = null;
    let cmenuSel = "";
    let dialog   = false;
    let result   = false;

    log("clickContextMenu whichWindow", whichWindow, "selector", selector);

    switch (whichWindow)
    {
    case "main":
        chrome = findMainWindow();
        cmenuSel = "#contentAreaContextMenu";
        break;

    case "dialog":
        chrome = kWindows.getDialogWindow();
        cmenuSel = "#contextMenu";
        dialog   = true;
        break;
    }

    //log("clickContextMenu window", chrome);

    if (chrome)
    {
        let doc   = findHTMLDocument({window: chrome, dialog: dialog});
        let elem  = doc.querySelector(selector);
        let cmenu = chrome.document.querySelector(cmenuSel);

        //log("clickContextMenu doc", doc.URL);
        //log("clickContextMenu elem", elem, "cmenu", cmenu, cmenu.tagName, cmenu.id);

        if (elem && cmenu)
        {
            let onShow = function(ev)
            {
                //log("clickContextMenu popupshowing");
                cmenu.removeEventListener("popupshowing", onShow, false);

                let items = cmenu.querySelectorAll("menuitem");

                for (let i = 0; i < items.length; ++i)
                {
                    let item = items[i];
                    //log("clickContextMenu label", item.label);

                    if (item.label.match(labelRegExp))
                    {
                        //log("clickContextMenu label matched", labelRegExp);
                        item.doCommand();
                        result = true;
                    }
                }

                if (duration && typeof duration === "number")
                {
                    //log("clickContextMenu timeout", duration);
                    chrome.setTimeout(() => cmenu.hidePopup(), duration);
                }
            }

            cmenu.addEventListener("popupshowing", onShow, false);
            synthMouseEvent(doc, elem, "contextmenu", 2);
        }
    }

    return result;
}



function sendString(whichWindow, selector, text)
{
    /*  Find the window indicated by whichWindow: 'main' or 'dialog'.
        Use selector to find an element in the window. Send the
        characters as key presses.
    */
    let doc = whichDoc(whichWindow);
    //log("sendString doc", doc, text.length, text);

    if (doc)
    {
        if (selector !== "!")
        {
            let elem = doc.querySelector(selector);

            if (elem)
            {
                //log("sendString elem", elem);
                elem.focus();
            }
        }

        for (let i = 0; i < text.length; ++i)
        {
            let c = text.charCodeAt(i);
            let d = {}

            if (c >= 65 && c <= 90)
            {
                d.shift = true;
                c = c + 32;
            }

            d.charCode = c;
            synthKeyEvent(doc, d);
        }
    }
}



function sendKey(whichWindow, selector, descr)
{
    /*  Find the window indicated by whichWindow: 'main' or 'dialog'.
        Use selector to find an element in the window. descr is
            shift:    BOOL
            control:  BOOL
            alt:      BOOL
            meta:     BOOL
            keyCode:  STRING    for VK_ keys
    */
    let doc = whichDoc(whichWindow);

    if (doc)
    {
        if (selector !== "!")
        {
            let elem = doc.querySelector(selector);

            if (elem)
            {
                //log("sendKey elem", elem);
                elem.focus();
            }
        }

        synthKeyEvent(doc, descr);
    }
}




function whichDoc(whichWindow)
{
    /*  Find the window indicated by whichWindow: 'main' or 'dialog'
        and return its document.
    */
    let chrome = null;
    let dialog = false;

    //log("whichDoc whichWindow", whichWindow);

    switch (whichWindow)
    {
    case "main":
        chrome = findMainWindow();
        break;

    case "dialog":
        chrome = kWindows.getDialogWindow();
        dialog   = true;
        break;
    }

    //log("clickContextMenu window", chrome);

    if (chrome)
    {
        return findHTMLDocument({window: chrome, dialog: dialog});
    }

    return null;
}



function synthMouseEvent(doc, elem, type, button)
{
    let win = doc.defaultView;
    var wu  = win.QueryInterface(Ci.nsIInterfaceRequestor)
                 .getInterface(Ci.nsIDOMWindowUtils);

    let rect  = elem.getBoundingClientRect();

    wu.sendMouseEvent(type,
        rect.left + (rect.width / 2),
        rect.top + (rect.height / 2),
        button || 1,
        1,                      // click count
        0);                     // modifiers
}



function synthKeyEvent(doc, descr)
{
    /*  descr is an object with
            shift:    BOOL
            control:  BOOL
            alt:      BOOL
            meta:     BOOL
            keyCode:  STRING    for VK_ keys
            charCode: NUMBER
    */
    //log("synthKeyEvent descr", showObj(descr));

    let win = doc.defaultView;
    var wu  = win.QueryInterface(Ci.nsIInterfaceRequestor)
                 .getInterface(Ci.nsIDOMWindowUtils);

    let mods = 0;
    mods += descr.shift?   wu.MODIFIER_SHIFT : 0;
    mods += descr.control? wu.MODIFIER_CONTROL : 0;
    mods += descr.alt?     wu.MODIFIER_ALT : 0;
    mods += descr.meta?    wu.MODIFIER_META : 0;

    let kcode;
    let ccode = 0;
    let flags = 0;

    if (descr.charCode)
    {
        ccode = descr.charCode;
        kcode = computeKeyCodeFromChar(ccode);
    }
    else
    {
        if (descr.keyCode)
        {
            kcode = KeyEvent["DOM_VK_" + descr.keyCode.toUpperCase()];
        }

        if (!kcode)
        {
            kcode = 0;
        }
    }

    let dfltHappened = wu.sendKeyEvent("keydown", kcode, 0, mods, flags);
    //log("synthKeyEvent send", kcode, ccode, mods, dfltHappened);

    if (!dfltHappened)
    {
        flags |= wu.KEY_FLAG_PREVENT_DEFAULT;
    }

    wu.sendKeyEvent("keypress", kcode, ccode, mods, flags);
    wu.sendKeyEvent("keyup",    kcode, 0,     mods, flags);
}



function computeKeyCodeFromChar(charCode)
{
    // Copied from mozilla code
    // This is probably not needed.

    if (charCode >= 97 && charCode <= 122)
    {
        return KeyEvent.DOM_VK_A + charCode - 97;
    }

    if (charCode >= 65 && charCode <= 90)
    {
        return KeyEvent.DOM_VK_A + charCode - 65;
    }

    if (charCode >= 48 && charCode <= 57)
    {
        return KeyEvent.DOM_VK_0 + charCode - 48;
    }

    // returns US keyboard layout's keycode
    switch (charCode)
    {
    case '~':
    case '`':
        return KeyEvent.DOM_VK_BACK_QUOTE;
    case '!':
        return KeyEvent.DOM_VK_1;
    case '@':
        return KeyEvent.DOM_VK_2;
    case '#':
        return KeyEvent.DOM_VK_3;
    case '$':
        return KeyEvent.DOM_VK_4;
    case '%':
        return KeyEvent.DOM_VK_5;
    case '^':
        return KeyEvent.DOM_VK_6;
    case '&':
        return KeyEvent.DOM_VK_7;
    case '*':
        return KeyEvent.DOM_VK_8;
    case '(':
        return KeyEvent.DOM_VK_9;
    case ')':
        return KeyEvent.DOM_VK_0;
    case '-':
    case '_':
        return KeyEvent.DOM_VK_SUBTRACT;
    case '+':
    case '=':
        return KeyEvent.DOM_VK_EQUALS;
    case '{':
    case '[':
        return KeyEvent.DOM_VK_OPEN_BRACKET;
    case '}':
    case ']':
        return KeyEvent.DOM_VK_CLOSE_BRACKET;
    case '|':
    case '\\':
        return KeyEvent.DOM_VK_BACK_SLASH;
    case ':':
    case ';':
        return KeyEvent.DOM_VK_SEMICOLON;
    case '\'':
    case '"':
        return KeyEvent.DOM_VK_QUOTE;
    case '<':
    case ',':
        return KeyEvent.DOM_VK_COMMA;
    case '>':
    case '.':
        return KeyEvent.DOM_VK_PERIOD;
    case '?':
    case '/':
        return KeyEvent.DOM_VK_SLASH;
    default:
        return 0;
    }
}




function clickToolbarButton()
{
    /*  Find the killit toolbar button and pop it up.
        This is currently for the old "widget" implementation
        of the button.

        This must run in the top-level add-on environment.
    */
    let doc = findMainWindow().document;
    let result = false;

    let elems = doc.querySelectorAll("toolbaritem");

    for (let i = 0; i < elems.length; ++i)
    {
        if (elems[i].id.match(/killit/))
        {
            // Clicks are only accepted when bubbling up from the window.
            let iframe = elems[i].firstChild;
            let w = iframe.contentWindow;

            let event = doc.createEvent("MouseEvent");
            event.initEvent('click', true, true, w, null);
            w.dispatchEvent(event);
            result = true;
            break;
        }
    }

    return result;
}



function findHTMLDocument(args)
{
    /*  Look through the frames of the browser and see if we can find
        a window with a HTML document that isn't an about: or resource: document.

        If we are looking into a dialog then there is only the one frame
        to select.
    */
    let result = null;

    args = args || {};

    let win = args.window || findMainWindow();

    for (let i = 0; i < win.frames.length; ++i)
    {
        let doc = win.frames[i].document;

        if (doc)
        {
            let elem = doc.documentElement;
            //log("findHTMLDocument doc", doc, doc.URL, elem);

            if (elem && elem.tagName === "HTML")
            {
                let ok = args.dialog;

                if (!ok)
                {
                    ok = !(doc.URL.match(/^about:/) ||
                           doc.URL.match(/^resource:/) ||
                           doc.URL.match(/^data:/));
                }

                if (ok)
                {
                    result = doc;
                    break;
                }
            }
        }
    }

    return result;
}



function findChoosePanel()
{
    /*  Look through the frames of the browser and see if we can find
        the Choose Panel. Return its document.
    */
    let doc = null;
    let win = findMainWindow();

    for (let i = 0; i < win.frames.length; ++i)
    {
        let frame = win.frames[i];

        if (frame.document)
        {
            // probably only the first one will work
            let title = frame.document.title ||
                        frame.document.documentElement.getAttribute("title");

            if (title && title.match(/Choose/))
            {
                doc = frame.document;
            }
        }
    }

    return doc;
}



function clickChooseLevel(n)
{
    let result = false;

    if (typeof n === "string")
    {
        n = n - "0";
    }

    let doc = findChoosePanel();

    if (doc)
    {
        let levels = doc.querySelectorAll("#levels input");
        log("clickChooseLevel #levels", levels.length);

        for (let i = 0; i < levels.length; ++i)
        {
            let level = levels[i];
            let m     = level.getAttribute("data-level") - 0;
            log("clickChooseLevel level m=", m, n);

            if (n === m)
            {
                level.click();
                result = true;
                break;
            }
        }
    }

    return result;
}



function clickChooseButton(selector)
{
    let result = false;
    let doc    = findChoosePanel();

    if (doc)
    {
        let button = doc.querySelector(selector);

        if (button)
        {
            button.click();
            log("clicking button", showElem(button));
            result = true;
        }
    }

    return result;
}




function clickChooseInstall()
{
    return clickChooseButton("#installButton");
}




function clickChooseUndo()
{
    return clickChooseButton("#undoButton");
}



function isHidden(selector)
{
    let result = false;
    let doc    = findHTMLDocument()
    //log("isHidden doc", doc, selector);

    if (doc)
    {
        let elem = doc.querySelector(selector);

        if (elem)
        {
            result = (elem.style.display === "none");
        }
    }

    return result;
}


//======================================================================

exports.clickToolbarButton = clickToolbarButton;
exports.clickChooseLevel   = clickChooseLevel;
exports.clickChooseInstall = clickChooseInstall;
exports.clickChooseUndo    = clickChooseUndo;
exports.clickContextMenu   = clickContextMenu;
exports.isHidden           = isHidden;
exports.sendKey            = sendKey;
exports.sendString         = sendString;
