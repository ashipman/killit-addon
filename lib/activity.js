/*  This is a straightforward module to hold the activity log so
    that it is accessible from the main and dlgMain modules.
*/

"use strict";

const {EventTarget} = require("sdk/event/target");
const {emit}        = require('sdk/event/core');

const {
    MAX_ACTIVITY,
    debug, debugLevel,
    log, showObj,
    } = require("./kUtils");

//======================================================================

var gActivityLog = [];      // activity entries from the killit script
var gActivitySerial = 0     // serial number for gActivityLog

const activityPort = EventTarget();



function logActivity(info)
{
    if (debugLevel() > 1)
    {
        debug("logActivity", showObj(info));
    }

    info.serial = ++gActivitySerial;

    if (gActivityLog.length >= MAX_ACTIVITY)
    {
        gActivityLog.shift();
    }

    gActivityLog.push(info);
    emit(activityPort, "activity", info);
}



function getActivity()
{
    return gActivityLog;
}



function clear()
{
    gActivityLog = [];
    emit(activityPort, "cleared");
}


//======================================================================

exports.clear       = clear;
exports.logActivity = logActivity;
exports.getActivity = getActivity;
exports.port        = activityPort;
