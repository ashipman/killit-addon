/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*  This is a variant of the SDK 1.16 page-mod.js. It has been modified
    to attach content to dialogs that are opened with a XUL template.
    The template then loads some HTML into an iframe.

    Some key differences:

    * We open a dialog box here.

    * We don't modify the dialog content except to inject script content.

    * So there is no need for inserting content style.

    * We don't support 'attachTo'. A dialog probably doesn't even have internal
      frames.

    * Any window we pop up is our business so we don't have to pay attention to
      private browsing.

    A XUL template is used to avoid problems
    with setting the window title and some other bugs
    (https://bugzilla.mozilla.org/show_bug.cgi?id=995080). It turns out
    we can set the window title on the window.document node but we may
    need to listen for the pageshow event. The other events don't seem
    to work reliably.

*/

"use strict";

/*  REVISIT - We should probably make this module part of a package
    along side the XUL parts.
*/
const MY_CHROME = "killit";

const {Cc, Ci}                          = require('chrome');

const observers                         = require('sdk/system/events');
const {Loader, validationAttributes}    = require('sdk/content/loader');
const {Worker}                          = require('sdk/content/worker');
const {Registry}                        = require('sdk/util/registry');
const {EventEmitter}                    = require('sdk/deprecated/events');
const {on, emit}                        = require('sdk/event/core');
const wu                                = require('sdk/window/utils');
const {has}                             = require("sdk/util/array");

// This maps from the public interface of a Dialog to the private one.
let dialogs = new WeakMap();

/*  A Dialog object inherits from Loader and EventEmitter.
    The Loader object does some validation of the content options.

    REVISIT - Why is it composing again with EventEmitter when Loader
    is composed with EventEmitter?

*/

const Dialog = Loader.compose(EventEmitter,
{
    on:                     EventEmitter.required,
    _listeners:             EventEmitter.required,
    contentScript:          Loader.required,
    contentScriptFile:      Loader.required,
    contentScriptWhen:      Loader.required,
    contentScriptOptions:   Loader.required,

    contextMenuItems:       null,       // an array of items
    shortCutItems:          null,       // an array of items
    menuBarItems:           null,

    _win:                   null,       // top-level chrome window
    _contentWin:            null,       // non-chrome wrapped window within the iframe
    _worker:                null,
    _url:                   "",
    _winSize:               null,

    _contextMenu:           null,
    _contextBuilt:          false,
    _contextTrigger:        null,
    _shortCutBuilt:         false,
    _handleShortCuts:       false,      // handle short cuts ourselves
    _shortCutInfo:          [],
    _menuBar:               null,
    _menuBarBuilt:          false,
    _allCommands:           null, // all command names appear as a key in here
    _activeSets:            [],   // array of controllable shortcuts or menu items
    _curActives:            null, // set of flags to activate shortcuts etc


    constructor: function Dialog(options)
    {
        /*  Private methods must be manually bound to the new instance if
            passed to listeners.
        */
        this._onContent          = this._onContent.bind(this);
        this._onUncaughtError    = this._onUncaughtError.bind(this);
        this._contextMenuShowing = this._contextMenuShowing.bind(this);
        this._contextMenuHidden  = this._contextMenuHidden.bind(this);
        this._handleCommand      = this._handleCommand.bind(this);
        this._handleKeyPress     = this._handleKeyPress.bind(this);
        this._saveGeometry       = this._saveGeometry.bind(this);

        this._accelModifier = getAccel();

        // We seem to need to recreate this to ensure it is empty.
        this._allCommands = new Object();
        this._activeSets  = [];

        options = options || {};

        if ('contentScript' in options)
        {
            this.contentScript = options.contentScript;
        }

        if ('contentScriptFile' in options)
        {
            // A string or array of strings
            this.contentScriptFile = options.contentScriptFile;
        }

        if ('contentScriptOptions' in options)
        {
            this.contentScriptOptions = options.contentScriptOptions;
        }

        if ('contentScriptWhen' in options)
        {
            this.contentScriptWhen = options.contentScriptWhen;
        }

        if ('onAttach' in options)
        {
            this.on('attach', options.onAttach);
        }

        if ('onDetach' in options)
        {
            this.on('detach', options.onDetach);
        }

        if ('onError' in options)
        {
            this.on('error', options.onError);
        }

        if ('onFinalize' in options)
        {
            this.on('finalized', options.onFinalize);
        }

        if ('onContextMenuShowing' in options)
        {
            this.on('contextMenuShowing', options.onContextMenuShowing);
        }

        if ('onUICommand' in options)
        {
            this.on('UICommand', options.onUICommand);
        }

        if ('contextMenu' in options)
        {
            // REVISIT - check that it is an array of items
            this.contextMenuItems = options.contextMenu;
        }

        if ('menuBar' in options)
        {
            // REVISIT - check that it is an array of items
            this.menuBarItems = options.menuBar;
        }

        if ('shortCuts' in options)
        {
            // REVISIT - check that it is an array of items
            this.shortCutItems = options.shortCuts;
            this._handleShortCuts = ('handleShortCuts' in options);
        }

        // An error listener could be added later.
        this.on('error', this._onUncaughtError);

        dialogManager.add(this._public);
        dialogs.set(this._public, this);

        this._url        = options.url || "";
        this._useMenuBar = options.menuBar || false;

        let openArgs = {
            url:         this._url,
            title:       options.title || null,
            contextMenu: this.contextMenuItems || false,
            menuBar:     this._useMenuBar,
            setContentWin: (win) => this._contentWin = win
            };

        // We are always loading into chrome
        options.features.chrome    = true;
        options.features.dependent = false;         // don't want it always in front of the parent
        options.features.dialog    = true;          // REVISIT doesn't seem to work

        let feats = serializeFeatures(options.features || {});
        //console.log("features", feats);

        this._win = wu.getMostRecentBrowserWindow().openDialog(
                        "chrome://" + MY_CHROME + "/content/cdialog.xul", "",
                        feats, openArgs);

        /*  This captures all command events that are missed by deeper handlers.
        */
        this._win.addEventListener("command", this._handleCommand, false);

        //  Remember resizes
        this._win.addEventListener("resize", this._saveGeometry, false);

        if (this._handleShortCuts)
        {
            // Grab all key events to recognise the short cuts.
            this._win.addEventListener("keypress", this._handleKeyPress, true);
        }
    },


    builtinCommand: function(cmd)
    {
        /*  This duplicates goDoCommand() from globalOverlay.js.
            It is typically called after we have passed a short cut via
            _handleKeyPress() the application and the application wants
            to invoke built-in behaviour. Presumably the element that
            the short cut was triggered on is still focussed.
        */
        try
        {
            var controller = this._win.top.document.commandDispatcher
                                .getControllerForCommand(cmd);

            if (controller && controller.isCommandEnabled(cmd))
            {
                controller.doCommand(cmd);
            }
        }
        catch(e)
        {
            console.error("An error occurred executing '" + cmd + "': " + e);
        }
    },


    close: function close()
    {
        // This is called from the client to start the closing sequence.
        // The destroy machinery will be called via _onWindowClose below.
        if (this._win)
        {
            this._win.close();
        }
    },


    destroy: function destroy()
    {
        /*  There are races here. The dialog's content worker will see
            an 'unload' event and may try to save things. We don't want
            the worker to be destroyed until the content gets a chance
            to save. The worker will become detached as a result of the 
            'unload' event. The worker destroy() method will destroy
            the port but the detach() method won't.

            The window could be destroyed before there is a chance to
            create a worker in which case we call finalize() here.

            This is public so that it can be called from DialogManager.
        */
        if (!this._worker)
        {
            this.finalize();
        }
    },


    finalize: function finalize()
    {
        this._emit('finalized');

        if (this._worker)
        {
            this._worker.destroy();
            this._worker = null;
        }

        //console.log("Dialog finalized");
        dialogs.delete(this._public);
        dialogManager.remove(this._public);

        // Restore focus from the toolbar to the window
        try
        {
            wu.getMostRecentBrowserWindow().gBrowser.contentWindow.focus();
        }
        catch(e){};
    }, 


    get worker()   this._worker,
    get window()   this._win,
    get geometry() this._winSize,


    _saveGeometry: function()
    {
        /*  We want the geometry to be available after the window has been closed.
            so we track it while it is active.
        */
        if (this._win)
        {
            this._winSize = {width: this._win.innerWidth, height: this._win.innerHeight};
        }
    },


    _forWindow: function(window)
    {
        /*  This can be called with the iframe's window when loading or the top window
            when closing. The iframe's window is non-chrome.
        */
        return window.top === this._win || window == this._contentWin;
    },


    _onContent: function _onContent(document)
    {
        /*  A normal HTML document has appeared. It may not yet be fully loaded.
            The XUL shell however must be present so we grab the context menu.
        */
        let window = document.defaultView;
        let state  = document.readyState;

        if ('start' === this.contentScriptWhen ||
            // Is `load` event already dispatched?
            'complete' === state ||
            // Is DOMContentLoaded already dispatched and waiting for it?
            ('ready' === this.contentScriptWhen && state === 'interactive'))
        {
            this._createWorker(window);
            return;
        }

        let eventName = ('end' === this.contentScriptWhen) ? 'load' : 'DOMContentLoaded';
        let self = this;

        window.addEventListener(eventName, function onReady(event)
        {
            if (event.target.defaultView != window)
            {
                return;
            }

            window.removeEventListener(eventName, onReady, true);

            self._createWorker(window);
        }, true);

        if (this.contextMenuItems)
        {
            let xuldoc = this._win.document;
            this._contextMenu  = xuldoc.getElementById("contextMenu");
            this._contextBuilt = false;

            this._contextMenu.addEventListener("popupshowing", this._contextMenuShowing, true);
            this._contextMenu.addEventListener("popuphidden",  this._contextMenuHidden,  true);
            this._contextMenu.addEventListener("command",      this._handleCommand,      true);

            this._constructShortCuts();
            this._constructContextMenu();
        }

        if (this.menuBarItems)
        {
            let xuldoc = this._win.document;
            this._menuBar  = xuldoc.getElementById("menubar");
            this._menuBarBuilt = false;

            this._constructMenuBar();
        }

        this._controlActives();
    },


    _createWorker: function _createWorker(window)
    {
        let self = this;

        this._worker = Worker(
        {
            window: window,

            contentScript:        self.contentScript,
            contentScriptFile:    self.contentScriptFile,
            contentScriptOptions: self.contentScriptOptions,

            // bounce syntax errors from the worker to the dialog object.
            onError: (e) => self._emit('error', e)
        });

        this._worker.window = window;   // save for the client
        this._emit('attach', this._worker);

        this._worker.once('detach', function detach()
        {
            /*  This will be called by the SDK when it notices
                that the document is unloaded which will be when its
                window is destroyed. At this point no more messages can be
                sent to the worker. However it appears that we can still
                get messages out of the worker.  We need the worker to
                hang around a while for these cleanup messages from the
                content. The worker must call finalize() when it is done.
            */
            self._emit('detach');
        });
    },


    _onUncaughtError: function _onUncaughtError(e)
    {
        if (this._listeners('error').length == 1)
        {
            console.exception(e);
        }
    },


    _contextMenuShowing: function(event)
    {
        this._contextTrigger = event.target.triggerNode;

        let info = {
            triggerId:      this._contextTrigger.id,
            triggerTagName: this._contextTrigger.tagName
            };

        this._emit('contextMenuShowing', info);
    },


    _contextMenuHidden: function(event)
    {
        this._contextTrigger = null;
    },


    _handleCommand: function(event)
    {
        /*  This catches the command coming from the context menu or a short cut.
            There may be a context menu trigger element to be included.  There may be a
            sandbox in the way so we must use the id and value.
        */
        //console.log("_handleCommand", event.type, "nodeName", event.target.nodeName, event.target.id);
        let t = event.target;

        let info = {
            triggerId:      this._contextTrigger? this._contextTrigger.id : null,
            triggerTagName: this._contextTrigger? this._contextTrigger.tagName : null,
            command:        t.id,
            value:          t.getAttribute("value"),
            };

        this._emit('UICommand', info);
        event.stopPropagation();
    },


    _handleKeyPress: function(event)
    {
        /*  Grab any key presses we recognise as short cut commands.
        */
        //console.log("_handleKeyPress", event.type, event.target.id, event.keyCode, event.charCode);
        let cmd  = null;

        for (let i = 0; !cmd && i < this._shortCutInfo.length; ++i)
        {
            let info = this._shortCutInfo[i];
            let code = event.keyCode;

            // Skip inactive shortcuts
            if (info.active && !isActive(info.active, this._curActives))
            {
                continue;
            }

            if (!code)
            {
                code = event.charCode;
                if (code >= 0x61 && code <= 0x7a)
                {
                    code -= 0x20;
                }
            }

            if (info.keyCode && info.keyCode === code)
            {
                if (info.any)
                {
                    cmd = info.command;
                }
                else
                {
                    if (info.alt     === event.altKey  &&
                        info.control === event.ctrlKey &&
                        info.meta    === event.metaKey &&
                        info.shift   === event.shiftKey)
                    {
                        cmd = info.command;
                    }
                }
            }
        }

        if (cmd)
        {
            //console.log("_handleKeyPress handled it");
            let info = {
                triggerId:      event.target.id,
                triggerTagName: event.target.tagName,
                command:        cmd,
                value:          ""
                };
            this._emit('UICommand', info);
            event.preventDefault();
        }
    },


    _controlActives: function()
    {
        /*  Run through the list in _activeSet and enable or disable
            them according to the flags in _curActives. If we don't
            have a value for _curActives then all are activated.
        */
        //console.log("_controlActives", this._curActives);
        //console.log("_controlActives sets", this._activeSets);
        this._activeSets.forEach((a) =>
        {
            if (a.flags)
            {
                let flag = isActive(a.flags, this._curActives);

                if (a.kind === "key")
                {
                    a.target.disabled = !flag;
                    //console.log("_controlActives disabling", a.target);
                }
                else
                if (a.kind === "hidable")
                {
                    a.target.hidden = !flag;
                    //console.log("_controlActives hiding", a.target);
                }
            }
        });
    },


    _defineUICommand: function(name)
    {
        /*  Add a command to the commandset if it is not already there.
            The name must be usable as a unique element id.

            The command event will be caught when it bubbles up to the
            top-level window. This seems to work the most reliably.
        */
        if (this._allCommands[name])
        {
            return;
        }

        let xuldoc   = this._contextMenu.ownerDocument;
        let commands = xuldoc.getElementById("commands");
        let cmd      = xuldoc.createElement("command");

        /*  The oncommand attribute seems to be required for a shortcut to generate
            and/or propagate a command event. It must be acceptable to strict JS.
            See bugzilla 371900.
        */
        cmd.setAttribute("id", name);
        cmd.setAttribute("oncommand", "var x = true;");
        commands.appendChild(cmd);
        this._allCommands[name] = true;
    },


    _constructShortCuts: function()
    {
        /*  Set shortcut key entries. The _contextMenu must already
            have been found.

            An item contains
                id:     unique id
                key:    string, printable char or VK_ name
                keytext:  string used as a label
                modifiers: string (optional)
                active: string or array of strings containing context flags

            bugzilla 565004 - The "any" modifier produces a faulty
            accelerator label.
        */
        if (this._shortCutBuilt)
        {
            return;
        }

        if (this._handleShortCuts)
        {
            this._constructShortCutHandler();
        }

        /*  Use the Firefox keyset system even if we are handling our own
            short cuts. This makes short cut labels available for menu items.
        */
        let self   = this;
        let xuldoc = this._contextMenu.ownerDocument;
        let keyset = xuldoc.getElementById("shortcuts");

        this.shortCutItems.forEach(item =>
        {
            if (item.id && item.key)
            {
                let key = xuldoc.createElement("key");

                key.setAttribute("id", item.id);

                if (item.key.match(/^VK_.+/))
                {
                    key.setAttribute("keycode", item.key);
                }
                else
                {
                    key.setAttribute("key", item.key);
                }

                if (item.keytext)
                {
                    key.setAttribute("keytext", item.keytext);
                }

                if (item.modifiers)
                {
                    key.setAttribute("modifiers", item.modifiers);
                }

                if (item.command)
                {
                    this._defineUICommand(item.command);
                    key.setAttribute("command", item.command);
                }

                keyset.appendChild(key);

                if (item.active)
                {
                    self._activeSets.push({
                        kind:  "key",
                        target: key,
                        flags:  mkSet(item.active)
                        });
                }
            }
        });

        this._shortCutBuilt = true;
    },



    _constructShortCutHandler: function()
    {
        /*  Construct some objects to help us recognise short cuts.

            The problem this solves is that we may want to override some
            of FF's shortcut bindings but FF does not let us change the
            bindings on elements such as INPUT.

            REVISIT - this only does ASCII
        */
        this.shortCutItems.forEach(item =>
        {
            if (item.id && item.key)
            {
                try
                {
                    let info = {keyCode: keyCodes[item.key] || 0};

                    const names = ["any", "alt", "control", "meta", "shift"];

                    // Ensure that all the names appear in the info as bools.
                    names.forEach((n) => info[n] = false);

                    if (item.modifiers)
                    {
                        item.modifiers.split(/\s|,/).forEach((m) =>
                            {
                                if (names.indexOf(m) >= 0)
                                {
                                    info[m] = true;
                                }
                                else
                                if (m === "accel")
                                {
                                    info[this._accelModifier] = true;
                                }
                            });
                    }

                    if (item.command)
                    {
                        info.command = item.command;
                    }

                    if (item.active)
                    {
                        info.active = mkSet(item.active);
                    }

                    this._shortCutInfo.push(info);
                }
                catch(e)
                {
                    console.error(e);
                }
            }
        });
    },


    _constructContextMenu: function()
    {
        //  Construct a context menu from the array of items.
        if (this._contextBuilt)
        {
            return;
        }

        this._populateMenu(this._contextMenu, this.contextMenuItems);
        this._contextBuilt = true;
    },


    _populateMenu: function(menupopup, items)
    {
        /*  Each item is an object with
                id: or submenu:
                label:
                shortcut:       id of an entry in the shortcuts
                command:        define or use a command object
                value:
                hidden:         REVISIT - not used
                enabled:        REVISIT - not used
                active:     string or array of strings containing context flags
            hidden defaults to false. enabled defaults to true.
            Use id="_separator_" to get a separator.

            REVISIT - If all menu items are hidden in a context menu
            then we will still get an empty context menu pop up. Nothing
            will be visible but an extra click will be required to pop it
            down. This is sloppy. It would be nice to be able to easily
            disable the context menu if there is nothing to show. Also
            it would be nice to remove separators if there is nothing
            to separate them from.

        */
        let self   = this;
        let xuldoc = this._contextMenu.ownerDocument;

        items.forEach(item =>
        {
            let elem = null;

            if (item.submenu)
            {
                elem = xuldoc.createElement("menu");

                if (item.label)
                {
                    elem.setAttribute("label", item.label);
                }

                let sub = xuldoc.createElement("menupopup");
                this._populateMenu(sub, item.submenu);

                elem.appendChild(sub);
                menupopup.appendChild(elem);
            }
            else
            if (item.id && item.id === "_separator_")
            {
                elem = xuldoc.createElement("menuseparator");
                menupopup.appendChild(elem);
            }
            else
            {
                elem = xuldoc.createElement("menuitem");

                if (item.id)
                {
                    elem.setAttribute("id", item.id);
                }

                if (item.shortcut)
                {
                    elem.setAttribute("key", item.shortcut);
                }

                if (item.label)
                {
                    elem.setAttribute("label", item.label);
                }

                if (item.value)
                {
                    elem.setAttribute("value", item.value);
                }

                if (item.command)
                {
                    this._defineUICommand(item.command);
                    elem.setAttribute("command", item.command);
                }

                elem.setAttribute("hidden",  item.hidden === true);
                elem.setAttribute("disabled", item.enabled === false);

                menupopup.appendChild(elem);
            }

            // Any of the above may be disabled.
            if (elem && item.active)
            {
                self._activeSets.push({
                    kind:  "hidable",
                    target: elem,
                    flags:  mkSet(item.active)
                    });
            }
        });
    },


    _constructMenuBar: function()
    {
        /*  Construct a menu bar from the array of items.
            The top level menus are:
                id:
                label:
                submenu:
            Menu items are as for the context menu.
        */
        if (this._menuBarBuilt)
        {
            return;
        }

        this.menuBarItems.forEach(item =>
        {
            /* Construct
                <menu id="file-menu" label="File">
                    <menupopup>
                        <menuitem id="file-open" label="Open"/>
                    </menupopup>
                </menu>
            */
            let xuldoc = this._contextMenu.ownerDocument;
            let menu   = xuldoc.createElement("menu");

            if (item.id)
            {
                menu.setAttribute("id", item.id);
            }

            if (item.label)
            {
                menu.setAttribute("label", item.label);
            }

            if (item.submenu)
            {
                let popup = xuldoc.createElement("menupopup");
                this._populateMenu(popup, item.submenu);
                menu.appendChild(popup);
            }

            this._menuBar.appendChild(menu);
        });

        this._menuBarBuilt = true;
    },


    setActive: function(which)
    {
        /*  Control shortcut and menu items. The 'which' argument is a 
            string or array of strings. Any shortcut or menu which has
            an active set will only be active if it intersects with
            the set in 'which'. If which is null then all items will
            be active.
        */
        this._curActives = which? mkSet(which) : null;
        this._controlActives();
    }
});


exports.Dialog = Dialog;




const DialogManager = Registry.resolve(
{
    constructor: '_init',
    _destructor: '_registryDestructor'
}).compose(
    {
        constructor: function DialogRegistry(constructor)
        {
            this._init(Dialog);
            this._onContentWindow = this._onContentWindow.bind(this);
            this._onWindowClose   = this._onWindowClose.bind(this);

            observers.on('document-element-inserted', this._onContentWindow);
            observers.on('domwindowclosed', this._onWindowClose, true);
        },

        _destructor: function _destructor()
        {
            observers.off('document-element-inserted', this._onContentWindow);
            observers.off('domwindowclosed', this._onWindowClose);
            this._removeAllListeners();

            // Let the dialogs clean themselves up.
            this._registry.forEach(function(dialog)
            {
                dialog.destroy();
            });

            this._registryDestructor();
        },

        _onContentWindow: function _onContentWindow({subject: document})
        {
            if (document instanceof Ci.nsIDOMHTMLDocument)
            {
                let window = document.defaultView;

                if (window)
                {
                    // Find a dialog for this window.
                    this._registry.forEach(function(dialog)
                    {
                        let priv = dialogs.get(dialog);

                        if (priv._forWindow(window))
                        {
                            priv._onContent(document);
                        }
                    });
                }
            }
        },

        _onWindowClose: function({subject: window})
        {
            this._registry.forEach(function(dialog)
            {
                let priv = dialogs.get(dialog);

                if (priv._forWindow(window))
                {
                    dialog.destroy();
                }
            });
        },

        off: function off(topic, listener)
        {
            this.removeListener(topic, listener);
        }
    });


const dialogManager = DialogManager();


//======================================================================

function have(v)
{
    return !(v === null || typeof v === "undefined");
}


const legalFeatures = [
    "alwaysLowered",
    "alwaysRaised",
    "centerscreen",
    "chrome",
    "close",
    "dependent",
    "dialog",
    "height",
    "innerHeight",
    "innerWidth",
    "left",
    "location",
    "menubar",
    "minimizable",
    "modal",
    "outerHeight",
    "outerWidth",
    "personalbar",
    "private",
    "resizable",
    "screenX",
    "screenY",
    "scrollbars",
    "status",
    "titlebar",
    "toolbar",
    "top",
    "width",
    "z-lock"
    ];


function serializeFeatures(options)
{
    let result = []

    Object.keys(options).forEach(key =>
    {
        if (legalFeatures.indexOf(key) >= 0)
        {
            let value = options[key];

            // the chrome and private features are special
            switch (key)
            {
            case "private":
            case "chrome":
                if (value)
                {
                    result.push(key);
                }
                break;

            default:
                result.push(key + "=" +
                   (value === true ? 'yes' : value === false ? 'no' : value));
                break;
            }
        }
    });

    return result.join(",");
}



function mkSet(arg)
{
    /*  The arg is a string or array of strings. Convert this to a set
        of strings. When the arg is a single string we split it on white space
        or commas. NOTE there is a bug, perhaps in the SDK, that causes the Array-ness
        of arg to be lost. We have to look for the length property.
    */
    let words = [];
    let set   = {};

    if (typeof arg === "string")
    {
        words = arg.split(/\s,/);
    }
    else
    if (typeof arg === "object" && arg.length > 0)
    {
        words = arg;
    }

    words.forEach((w) => set[w] = true);
    return set;
}



function isActive(itemSet, flags)
{
    /*  The args must be sets of strings represented as maps from strings to true.
        See if there is a non-empty intersection. If flags is empty then the
        result will be false.
    */
    if (flags)
    {
        for (let k in flags)
        {
            if (itemSet[k])
            {
                return true;
            }
        }
    }

    return false;
}



function getAccel()
{
    /*  Look up the ui.key.accelKey to see what the default accelerator is.
        Return "control" or "alt".
    */
    let prefService = Cc["@mozilla.org/preferences-service;1"].
                        getService(Ci.nsIPrefService);
    try
    {
        let branch = prefService.getBranch("ui.key.");

        if (branch)
        {
            let n = branch.getIntPref("accelKey");

            if (n === Ci.nsIDOMKeyEvent.DOM_VK_ALT)
            {
                return "alt";
            }
        }
    }
    catch(e)
    {
        console.info("Killit: failed to read ui.key.accel " + e);
    }

    return "control";
}


// Key name translation for keyCodes.
const keyCodes = {
    VK_ADD:		Ci.nsIDOMKeyEvent.DOM_VK_ADD,
    VK_ALT:		Ci.nsIDOMKeyEvent.DOM_VK_ALT,
    VK_BACK:		Ci.nsIDOMKeyEvent.DOM_VK_BACK,
    VK_BACK_QUOTE:	Ci.nsIDOMKeyEvent.DOM_VK_BACK_QUOTE,
    VK_BACK_SLASH:	Ci.nsIDOMKeyEvent.DOM_VK_BACK_SLASH,
    VK_CANCEL:		Ci.nsIDOMKeyEvent.DOM_VK_CANCEL,
    VK_CAPS_LOCK:	Ci.nsIDOMKeyEvent.DOM_VK_CAPS_LOCK,
    VK_CLEAR:		Ci.nsIDOMKeyEvent.DOM_VK_CLEAR,
    VK_CLOSE_BRACKET:	Ci.nsIDOMKeyEvent.DOM_VK_CLOSE_BRACKET,
    VK_COMMA:		Ci.nsIDOMKeyEvent.DOM_VK_COMMA,
    VK_CONTROL:		Ci.nsIDOMKeyEvent.DOM_VK_CONTROL,
    VK_DECIMAL:		Ci.nsIDOMKeyEvent.DOM_VK_DECIMAL,
    VK_DELETE:		Ci.nsIDOMKeyEvent.DOM_VK_DELETE,
    VK_DIVIDE:		Ci.nsIDOMKeyEvent.DOM_VK_DIVIDE,
    VK_DOWN:		Ci.nsIDOMKeyEvent.DOM_VK_DOWN,
    VK_END:		Ci.nsIDOMKeyEvent.DOM_VK_END,
    VK_EQUALS:		Ci.nsIDOMKeyEvent.DOM_VK_EQUALS,
    VK_ESCAPE:		Ci.nsIDOMKeyEvent.DOM_VK_ESCAPE,
    VK_F1:		Ci.nsIDOMKeyEvent.DOM_VK_F1,
    VK_F10:		Ci.nsIDOMKeyEvent.DOM_VK_F10,
    VK_F11:		Ci.nsIDOMKeyEvent.DOM_VK_F11,
    VK_F12:		Ci.nsIDOMKeyEvent.DOM_VK_F12,
    VK_F13:		Ci.nsIDOMKeyEvent.DOM_VK_F13,
    VK_F14:		Ci.nsIDOMKeyEvent.DOM_VK_F14,
    VK_F15:		Ci.nsIDOMKeyEvent.DOM_VK_F15,
    VK_F16:		Ci.nsIDOMKeyEvent.DOM_VK_F16,
    VK_F17:		Ci.nsIDOMKeyEvent.DOM_VK_F17,
    VK_F18:		Ci.nsIDOMKeyEvent.DOM_VK_F18,
    VK_F19:		Ci.nsIDOMKeyEvent.DOM_VK_F19,
    VK_F2:		Ci.nsIDOMKeyEvent.DOM_VK_F2,
    VK_F20:		Ci.nsIDOMKeyEvent.DOM_VK_F20,
    VK_F21:		Ci.nsIDOMKeyEvent.DOM_VK_F21,
    VK_F22:		Ci.nsIDOMKeyEvent.DOM_VK_F22,
    VK_F23:		Ci.nsIDOMKeyEvent.DOM_VK_F23,
    VK_F24:		Ci.nsIDOMKeyEvent.DOM_VK_F24,
    VK_F3:		Ci.nsIDOMKeyEvent.DOM_VK_F3,
    VK_F4:		Ci.nsIDOMKeyEvent.DOM_VK_F4,
    VK_F5:		Ci.nsIDOMKeyEvent.DOM_VK_F5,
    VK_F6:		Ci.nsIDOMKeyEvent.DOM_VK_F6,
    VK_F7:		Ci.nsIDOMKeyEvent.DOM_VK_F7,
    VK_F8:		Ci.nsIDOMKeyEvent.DOM_VK_F8,
    VK_F9:		Ci.nsIDOMKeyEvent.DOM_VK_F9,
    VK_HELP:		Ci.nsIDOMKeyEvent.DOM_VK_HELP,
    VK_HOME:		Ci.nsIDOMKeyEvent.DOM_VK_HOME,
    VK_INSERT:		Ci.nsIDOMKeyEvent.DOM_VK_INSERT,
    VK_LEFT:		Ci.nsIDOMKeyEvent.DOM_VK_LEFT,
    VK_MULTIPLY:	Ci.nsIDOMKeyEvent.DOM_VK_MULTIPLY,
    VK_NUM_LOCK:	Ci.nsIDOMKeyEvent.DOM_VK_NUM_LOCK,
    VK_NUMPAD0:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD0,
    VK_NUMPAD1:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD1,
    VK_NUMPAD2:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD2,
    VK_NUMPAD3:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD3,
    VK_NUMPAD4:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD4,
    VK_NUMPAD5:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD5,
    VK_NUMPAD6:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD6,
    VK_NUMPAD7:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD7,
    VK_NUMPAD8:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD8,
    VK_NUMPAD9:		Ci.nsIDOMKeyEvent.DOM_VK_NUMPAD9,
    VK_OPEN_BRACKET:	Ci.nsIDOMKeyEvent.DOM_VK_OPEN_BRACKET,
    VK_PAGE_DOWN:	Ci.nsIDOMKeyEvent.DOM_VK_PAGE_DOWN,
    VK_PAGE_UP:		Ci.nsIDOMKeyEvent.DOM_VK_PAGE_UP,
    VK_PAUSE:		Ci.nsIDOMKeyEvent.DOM_VK_PAUSE,
    VK_PERIOD:		Ci.nsIDOMKeyEvent.DOM_VK_PERIOD,
    VK_PRINTSCREEN:	Ci.nsIDOMKeyEvent.DOM_VK_PRINTSCREEN,
    VK_QUOTE:		Ci.nsIDOMKeyEvent.DOM_VK_QUOTE,
    VK_RETURN:		Ci.nsIDOMKeyEvent.DOM_VK_RETURN,
    VK_RIGHT:		Ci.nsIDOMKeyEvent.DOM_VK_RIGHT,
    VK_SCROLL_LOCK:	Ci.nsIDOMKeyEvent.DOM_VK_SCROLL_LOCK,
    VK_SEMICOLON:	Ci.nsIDOMKeyEvent.DOM_VK_SEMICOLON,
    VK_SEPARATOR:	Ci.nsIDOMKeyEvent.DOM_VK_SEPARATOR,
    VK_SHIFT:		Ci.nsIDOMKeyEvent.DOM_VK_SHIFT,
    VK_SLASH:		Ci.nsIDOMKeyEvent.DOM_VK_SLASH,
    VK_SPACE:		Ci.nsIDOMKeyEvent.DOM_VK_SPACE,
    VK_SUBTRACT:	Ci.nsIDOMKeyEvent.DOM_VK_SUBTRACT,
    VK_TAB:		Ci.nsIDOMKeyEvent.DOM_VK_TAB,
    VK_UP:		Ci.nsIDOMKeyEvent.DOM_VK_UP,

    "A":		Ci.nsIDOMKeyEvent.DOM_VK_A,
    "B":		Ci.nsIDOMKeyEvent.DOM_VK_B,
    "C":		Ci.nsIDOMKeyEvent.DOM_VK_C,
    "D":		Ci.nsIDOMKeyEvent.DOM_VK_D,
    "E":		Ci.nsIDOMKeyEvent.DOM_VK_E,
    "F":		Ci.nsIDOMKeyEvent.DOM_VK_F,
    "G":		Ci.nsIDOMKeyEvent.DOM_VK_G,
    "H":		Ci.nsIDOMKeyEvent.DOM_VK_H,
    "I":		Ci.nsIDOMKeyEvent.DOM_VK_I,
    "J":		Ci.nsIDOMKeyEvent.DOM_VK_J,
    "K":		Ci.nsIDOMKeyEvent.DOM_VK_K,
    "L":		Ci.nsIDOMKeyEvent.DOM_VK_L,
    "M":		Ci.nsIDOMKeyEvent.DOM_VK_M,
    "N":		Ci.nsIDOMKeyEvent.DOM_VK_N,
    "O":		Ci.nsIDOMKeyEvent.DOM_VK_O,
    "P":		Ci.nsIDOMKeyEvent.DOM_VK_P,
    "Q":		Ci.nsIDOMKeyEvent.DOM_VK_Q,
    "R":		Ci.nsIDOMKeyEvent.DOM_VK_R,
    "S":		Ci.nsIDOMKeyEvent.DOM_VK_S,
    "T":		Ci.nsIDOMKeyEvent.DOM_VK_T,
    "U":		Ci.nsIDOMKeyEvent.DOM_VK_U,
    "V":		Ci.nsIDOMKeyEvent.DOM_VK_V,
    "W":		Ci.nsIDOMKeyEvent.DOM_VK_W,
    "X":		Ci.nsIDOMKeyEvent.DOM_VK_X,
    "Y":		Ci.nsIDOMKeyEvent.DOM_VK_Y,
    "Z":		Ci.nsIDOMKeyEvent.DOM_VK_Z,
    "0":		Ci.nsIDOMKeyEvent.DOM_VK_0,
    "1":		Ci.nsIDOMKeyEvent.DOM_VK_1,
    "2":		Ci.nsIDOMKeyEvent.DOM_VK_2,
    "3":		Ci.nsIDOMKeyEvent.DOM_VK_3,
    "4":		Ci.nsIDOMKeyEvent.DOM_VK_4,
    "5":		Ci.nsIDOMKeyEvent.DOM_VK_5,
    "6":		Ci.nsIDOMKeyEvent.DOM_VK_6,
    "7":		Ci.nsIDOMKeyEvent.DOM_VK_7,
    "8":		Ci.nsIDOMKeyEvent.DOM_VK_8,
    "9":		Ci.nsIDOMKeyEvent.DOM_VK_9,

    '^':                Ci.nsIDOMKeyEvent.DOM_VK_CIRCUMFLEX,
    '!':                Ci.nsIDOMKeyEvent.DOM_VK_EXCLAMATION,
    '"':                Ci.nsIDOMKeyEvent.DOM_VK_DOUBLE_QUOTE,
    '#':                Ci.nsIDOMKeyEvent.DOM_VK_HASH,
    '$':                Ci.nsIDOMKeyEvent.DOM_VK_DOLLAR,
    '%':                Ci.nsIDOMKeyEvent.DOM_VK_PERCENT,
    '&':                Ci.nsIDOMKeyEvent.DOM_VK_AMPERSAND,
    '_':                Ci.nsIDOMKeyEvent.DOM_VK_UNDERSCORE,
    '(':                Ci.nsIDOMKeyEvent.DOM_VK_OPEN_PAREN,
    ')':                Ci.nsIDOMKeyEvent.DOM_VK_CLOSE_PAREN,
    '*':                Ci.nsIDOMKeyEvent.DOM_VK_ASTERISK,
    '+':                Ci.nsIDOMKeyEvent.DOM_VK_PLUS,
    '|':                Ci.nsIDOMKeyEvent.DOM_VK_PIPE,
    '-':                Ci.nsIDOMKeyEvent.DOM_VK_HYPHEN_MINUS,

};