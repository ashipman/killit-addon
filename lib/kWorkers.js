"use strict";

const {log, debug, debugLevel} = require("./kUtils");

//======================================================================

/*  We decorate each worker object with these:

    realURL - the url with which it was loaded. This will differ from worker.url
              for iframes.

    documentURL - the window.document.location when it was loaded. This will
              differ from worker.url when the page is hidden (e.g. in the bfcache).

    REVISIT There is the problem of the same page being open in different windows.
    They will all be treated as one batch.

*/
let workers = [];

// A new one is assigned to each worker.
let serialNum = 0;


function nextSerial()
{
    return ++serialNum;
}



function addWorker(worker)
{
    if (debugLevel())
    {
        debug(worker.serialNum.toString() + ": new worker for documentURL=" +
            worker.documentURL + " realURL=" + worker.realURL);
    }

    workers.push(worker);

    worker.on('detach', function ()
    {
        detachWorker(worker);
    });
}



function findWorkers(url, useReal)
{
    /*  Find all workers with the same document or real URL.
        If the URL is null or undefined then return all of them.
    */
    let result = []
    let len = workers.length;

    for (let i = 0; i < len; ++i)
    {
        if (!url || 
            (useReal  && workers[i].realURL === url) ||
            (!useReal && workers[i].documentURL === url))
        {
            result.push(workers[i]);
        }
    }

    return result;
}



function detachWorker(worker)
{
    let index = workers.indexOf(worker);

    if(index != -1)
    {
        workers.splice(index, 1);

        if (debugLevel())
        {
            debug(worker.serialNum.toString() + ": removed worker " + worker.realURL);
        }
    }
}



function dumpWorkers()
{
    let len = workers.length;

    for (let i = 0; i < len; ++i)
    {
        try
        {
            let w = workers[i];
            debug("dumpWorkers [" + i + "] documentURL=" + w.documentURL +
                " realURL=" + w.realURL);
        }
        catch(e)
        {
            debug("dumpWorkers [" + i + "] exception " + e);
        }
    }
}



exports.nextSerial   = nextSerial;
exports.addWorker    = addWorker;
exports.findWorkers  = findWorkers;
exports.detachWorker = detachWorker;
exports.dumpWorkers  = dumpWorkers;
