/*  This is a non-SDK module that can be used in a content script.
*/

let kUtils = (function()
{
"use strict";

//======================================================================

// Agents that can change the group configs.
const AGENT_CATALOG = 1;
const AGENT_CHOOSER = 2;
const AGENT_EDITOR  = 3;
const AGENT_IMPORT  = 4;
const AGENT_SYSTEM  = 5;

// REVISIT - this name must be localised
const ANON_GROUP_NAME = "unsorted";
const ANON_GROUP_UUID = "f27e7089-c526-497e-ef21-a8d7729aa785";

// This is the current store format that we implement.
const CURR_GROUP_VERSION = 2;

const STATE_LOCAL    = "local";
const STATE_IMPORTED = "imported";
const STATE_MODIFIED = "modified";

const METHOD_HIDE    = "hide";
const METHOD_DELETE  = "delete";

// Default kill file extension
const KILL_EXT   = "kill";
const BACKUP_EXT = "bkill";

// This gets the scheme at index 2, host at index 4 and path at index 5.
const URL_SCHEME    = new RegExp(/^(\w+):(.*)$/);
const URL_HOST1     = new RegExp(/^\/\/([^/]*)(.*)$/);
const VERSION_RE    = new RegExp(/^\d+(\.\d+)*$/);

// Maximum number of activity entries
const MAX_ACTIVITY  = 20;

function assert(test, msg)
{
    if (!test)
    {
        msg = msg || "assertion failure";
        console.error(msg);
        throw Error(msg);
    }
}



function cleanWS(text)
{
    /*  Replace runs of WS including CR and LF with a single blank.
        Trim leading and trailing WS.
    */
    return text.replace(/\s+/, " ").
                replace(/^\s+/, "").
                replace(/\s+$/, "");
}



function validVersion(v)
{
    return v.match(VERSION_RE);
}



function compareVersions(v1, v2)
{
    /*  The versions are of the form "1.2". Split them into individual
        numbers are return -1, 0 or 1. The versions must be valid.
        A shorter version comes before a longer one i.e. 1.2 before 1.2.3
    */
    let n1 = v1.split(".").map((s) => s - 0);   // get a list of numbers
    let n2 = v2.split(".").map((s) => s - 0);   // get a list of numbers

    while (n1.length > 0 && n2.length > 0)
    {
        let d1 = n1.shift();
        let d2 = n2.shift();

        if (d1 < d2) return -1;
        if (d1 > d2) return 1;
    }

    if (n1.length == 0 && n2.length == 0) return 0;
    if (n1.length == 0) return -1;      // v1 is shorter
    return 1;
}



function copyObj(toObj, fromObj)
{
    // Clone the own properties.
    for (let p in fromObj)
    {
        toObj[p] = fromObj[p];
    }

    return toObj;
}



function createUUID()
{
    // From http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
    let d = Date.now();

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
    {
        let r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        let v = (c === 'x') ? r : (r & 0x7 | 0x8);
        return v.toString(16);
    });
}



function have(v)
{
    return !(v === null || typeof v === "undefined");
}



function groupSkeleton(uuid)
{
    // These are the defaults for a group.
    return {
        formatVersion:  CURR_GROUP_VERSION,
        uuid:           (uuid || createUUID()),
        state:          STATE_LOCAL,
        enabled:        true,
        validated:      false,           // controlled by the UI
        name:           (uuid === ANON_GROUP_UUID)? ANON_GROUP_NAME : "",
        contactEMail:   "",
        contentVersion: "",
        comment:        "",
        lastModified:   Date.now(),
        parts:          []
        };
}



function partSkeleton(uuid)
{
    return {
        uuid:           (uuid || createUUID()),
        title:          "",
        urlPattern:     "",
        enabled:        false,
        validated:      true,           // controlled by the UI
        selectors:      []
        };
}



function selectorSkeleton()
{
    return {
        comment: "",
        selector: "",
        method:  METHOD_HIDE
        };
}


const entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#039;',
    "/": '&#047;'
    };

function escapeHtml(string)
{
    return String(string).replace(/[&<>"'\/]/g, s => entityMap[s]);
}



function myURL(url)
{
    /*  This is not a constructor. We need to JSONify the result so it
        is an ordinary object.  The query and fragment will be omitted.
        (The SDK url module is not accessible within a content script).

        We try to recognise "*.theage.com.au/*" as a host and path.

        An empty string returns null.
    */
    let u = url.replace(/\s*/, "");

    if (!u)
    {
        return null;
    }

    let obj = {
        scheme: "",
        host:   "",
        path:   "",
        };

    let m = URL_SCHEME.exec(u);

    if (m !== null)
    {
        obj.scheme = m[1];
        u = m[2];
    }

    m = URL_HOST1.exec(u);

    if (m !== null)
    {
        obj.host = m[1];
        obj.path = m[2];
    }
    else
    {
        // Perhaps it's a host without a leading //
        m = u.indexOf("/");

        if (m >= 0)
        {
            obj.host = u.substring(u, m);
            obj.path = u.substring(m);
        }
        else
        {
            // no slash at all. Treat it as a domain.
            obj.host = u;
        }
    }

    return obj;
}



function myURLsame(url1, url2)
{
    return url1.scheme === url2.scheme &&
           url1.host === url2.host &&
           url1.path === url2.path;
}



function myURLtoString(url)
{
    // We allow null to return the empty string.
    let s = "";

    if (typeof url === "object" && url !== null)
    {
        if (url.scheme)
        {
            s += url.scheme + ":";
        }

        if (url.host)
        {
            /*  We want to see file:///a/b/c but only if there is a scheme.
                Similarly we interpret www.x.y/abc as a host and a path.
            */
            if (url.scheme)
            {
                s += "//";
            }

            s += url.host;
        }

        if (url.path)
        {
            if (url.path[0] !== "/")
            {
                s += "/";
            }
            s += url.path;
        }
    }

    return s;
}



function myURLmatch(pattern, url)
{
    /*  The arguments must both have been produced by mkURL() above.
        The first may contain wildcards. The second will be matched
        against it.

        If the scheme in the pattern is empty then it will match 'http'
        or 'https'.  We refuse to match with 'chrome' or 'resource'.
        Otherwise an asterisk will match any sequence of characters.

        If the pattern path is empty or "/" then any url path will
        be accepted.

        The pattern can be null to match with anything (except the chrome
        or resource schemes).

    */
    //log("myURLmatch 1", showObj(pattern), showObj(url));
    if (url.scheme === "chrome" || url.scheme === "resource") 
    {
        return false;
    }

    if (!pattern)
    {
        return true;
    }

    if (pattern.scheme)
    {
        if (!wildcardMatch(pattern.scheme, url.scheme || "http"))
        {
            //log("myURLmatch 2");
            return false;
        }
    }
    else
    {
        if (!(url.scheme === "http" || url.scheme === "https" || url.scheme === ""))
        {
            //log("myURLmatch 3");
            return false;
        }
    }

    if (pattern.host || url.host)
    {
        if (!wildcardMatch(pattern.host, url.host))
        {
            //log("myURLmatch 4");
            return false;
        }
    }

    if (pattern.path && pattern.path !== "/")
    {
        if (!url.path || !wildcardMatch(pattern.path, url.path))
        {
            //log("myURLmatch 5");
            return false;
        }
    }

    //log("myURLmatch success");
    return true;
}



function Sorter(array, extractor, comparator)
{
    /*  This is an object that represents a permutation to be applied to an
        array. The extractor function is applied to each array element and
        then the extracted values are sorted using the comparator function.

        The permutation can then be applied to the original array (or
        any array of the same length).

        The extracted values are retained to avoid the extra effort of
        erasing them.
    */
    let elems = array.map(extractor);
    this.perm  = elems.map((v, ix) => Object({index: ix, value: v}));
    this.perm.sort((a, b) => comparator(a.value, b.value));
}



Sorter.prototype.apply = function(array, forwards)
{
    // Permute the array in place.
    if (array.length === this.perm.length)
    {
        let copy = array.slice(0);

        if (forwards)
        {
            this.perm.forEach((pair, ix) => array[ix] = copy[pair.index]);
        }
        else
        {
            this.perm.forEach((pair, ix) => array[pair.index] = copy[ix]);
        }
    }
}



function sortGroups(grps)
{
    /*  Treat grps as an array of objects with a property called 'name'. Sort
        according to the name. This is used for showing groups in alphabetical
        order. The array is sorted in-place.
    */
    grps.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
}



function trimURL(url)
{
    //  Remove the fragment and query for easy comparing.
    return url.replace(/#.*/, "").replace(/\?.*/, "");
}



function wildcardToRE(wildcard, options)
{
    let reg = wildcard.replace(/\]/g, "\\]")
                      .replace(/\\/g, "\\\\")
                      .replace(/\^/g, "\\^")
                      .replace(/[.{}()$|+[]/g, "\\$&")
                      .replace(/\*/g, ".*")
                      .replace(/\?/g, ".");

    options  = options || {};
    let flag = options.caseInsensitive? "i" : "";

    return new RegExp("^" + reg + "$", flag);
}



function wildcardMatch(wildcard, target, options)
{
    try
    {
        return wildcardToRE(wildcard, options).test(target);
    }
    catch(e)
    {
        logException(e);
    }

    return false;
}



//======================================================================
// Logging

var debugLevel = 0;


function getDebugLevel()
{
    return debugLevel;
}


function setDebugLevel(level)
{
    debugLevel = level;
}



function error()
{
    let args = Array.slice(arguments);
    args.unshift("Killit:");
    console.error(args.join(""));
}



function lognw()
{
    //  The prefix seen in the SDK does not appear in the Browser Console.
    let args = Array.slice(arguments);
    args.unshift("Killit: ");
    console.info(args.join(""));
}
 


function debug()
{
    //  With white space.
    //  console.debug() defaults to no output when using cfx so we use console.log().
    if (debugLevel > 0)
    {
        let args = Array.slice(arguments);
        args.unshift("Killit: ");
        console.log(args.join(" "));
    }
}
 


function log()
{
    //  With white space.
    let args = Array.slice(arguments);
    args.unshift("Killit: ");
    console.info(args.join(" "));
}



function logException(error, prefix)
{
    // Log an exception with a file location.
    let loc = "";

    if (error.fileName)
    {
        loc += "file " + error.fileName + ":";
    }

    if (error.lineNumber && typeof(error.lineNumber == "number"))
    {
        loc += "line " + error.lineNumber + ":";
    }

    prefix = prefix || "";

    console.error(loc + prefix + error.toString());
}



function sameJSON(obj1, obj2, skips)
{
    /*  This tests if two JSONable objects or values contain the
        same data. The algorithm doesn't protect itself against loops
        in the objects.

        For convenience the undefined value is accepted and matches nothing.

        If there is a skips object with properties that evaluate to true
        then the name match is skipped. 

        REVISIT - unit test
    */
    //log("sameJSON", typeof obj1, typeof obj2, obj1, obj2);

    skips = skips || {};

    if (obj1 === null || obj2 === null)
    {
        return obj1 === obj2;
    }
    else
    if (typeof obj1 === "object" && typeof obj2 === "object")
    {
        /*  The keys must be the same and then each value must be the same.
            They will be strings unless they are arrays in which case the 
            keys will be integers.
        */
        let keys1 = Object.keys(obj1).filter((k) => !skips[k]);
        let keys2 = Object.keys(obj2).filter((k) => !skips[k]);

        if (keys1.length !== keys2.length)
        {
            return false;
        }

        keys1.sort();
        keys2.sort();

        for (let i = 0; i < keys1.length; ++i)
        {
            let k1 = keys1[i];
            let k2 = keys2[i];

            if (k1 !== k2 || !sameJSON(obj1[k1], obj2[k2], skips))
            {
                //log("sameJSON mismatch", k1, k2);
                return false;
            }
        }

        return true;
    }
    else
    if (typeof obj1 === "undefined" || typeof obj2 === "undefined")
    {
        return false;
    }
    else
    {
        return obj1 === obj2;
    }
}



function showObj(obj, levelLimit)
{
    let stack  = [];
    levelLimit = levelLimit || 100;

    function asStr(v)
    {
        if (typeof v === "undefined")
        {
            return "undefined";
        }
        else
        if (typeof v === "string")
        {
            return '"' + v + '"';
        }
        else
        if (v === null)
        {
            return "null";
        }
        else
        {
            return v.toString();
        }
    }

    function loop(inner)
    {
        // Watch for loops in objects e.g. window.top.
        if (typeof inner === "object" && inner !== null)
        {
            if (stack.length >= levelLimit)
            {
                return asStr(inner);
            }
            else
            {
                try
                {
                    let keys = Object.keys(inner);
                    stack.push(inner);

                    let s = "";
                    keys.forEach(key =>
                    {
                        let value = inner[key];

                        if (s)
                        {
                            s += ", ";
                        }

                        if (stack.indexOf(value) >= 0)
                        {
                            // We've already done it.
                            s += key + ": " + asStr(value);
                        }
                        else
                        {
                            s += key + ": " + loop(value);
                        }
                    });
                    s = "{" + s + "}";

                    stack.pop();
                    return s;
                }
                catch(e)
                {
                    return asStr(inner);
                }
            }
        }
        else
        {
            return asStr(inner);
        }
    }

    return loop(obj);
}



function showElem(elem)
{
    if (!elem)
    {
        return "none";
    }

    let s = elem.nodeName;

    if (elem.id)
    {
        s += " #" + elem.id;
    }

    if (elem.className)
    {
        s += " ." + elem.className;
    }

    if (elem.value)
    {
        s += " " + elem.value;
    }

    return s;
}

//======================================================================

return {
    AGENT_CATALOG:      AGENT_CATALOG,
    AGENT_CHOOSER:      AGENT_CHOOSER,
    AGENT_EDITOR:       AGENT_EDITOR,
    AGENT_IMPORT:       AGENT_IMPORT,
    AGENT_SYSTEM:       AGENT_SYSTEM,

    ANON_GROUP_NAME:    ANON_GROUP_NAME,
    ANON_GROUP_UUID:    ANON_GROUP_UUID,
    CURR_GROUP_VERSION: CURR_GROUP_VERSION,
    KILL_EXT:           KILL_EXT,
    BACKUP_EXT:         BACKUP_EXT,
    STATE_IMPORTED:     STATE_IMPORTED,
    STATE_MODIFIED:     STATE_MODIFIED,
    STATE_LOCAL:        STATE_LOCAL,

    METHOD_HIDE:        METHOD_HIDE,
    METHOD_DELETE:      METHOD_DELETE,

    MAX_ACTIVITY:       MAX_ACTIVITY,

    assert:         assert,
    cleanWS:        cleanWS,
    compareVersions: compareVersions,
    copyObj:        copyObj,
    createUUID:     createUUID,
    debug:          debug,
    debugLevel:     getDebugLevel,
    error:          error,
    escapeHtml:     escapeHtml,
    groupSkeleton:  groupSkeleton,
    have:           have,
    logException:   logException,
    log:            log,
    lognw:          lognw,
    myURLmatch:     myURLmatch,
    myURL:          myURL,
    myURLsame:      myURLsame,
    myURLtoString:  myURLtoString,
    partSkeleton:   partSkeleton,
    sameJSON:       sameJSON,
    selectorSkeleton: selectorSkeleton,
    setDebugLevel:  setDebugLevel,
    showElem:       showElem,
    showObj:        showObj,
    Sorter:         Sorter,
    sortGroups:     sortGroups,
    trimURL:        trimURL,
    validVersion:   validVersion,
    wildcardMatch:  wildcardMatch
    };

}());

// For SDK compatibility
if (typeof exports !== "undefined")
{
    // Re-export without redefining the exports object.
    for (let p in kUtils)
    {
        exports[p] = kUtils[p];
    }
}
