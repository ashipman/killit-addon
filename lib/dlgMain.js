"use strict";

const {Cc, Ci} = require("chrome");
const data     = require("sdk/self").data;
const pageMod  = require("sdk/page-mod");
const winutils = require("sdk/window/utils");
const iofile   = require("sdk/io/file");
const tabs     = require("sdk/tabs");
const neturl   = require("sdk/net/url");
const _        = require("sdk/l10n").get;

const activity = require("./activity");
const configs  = require("./configs");
const kUtils   = require("./kUtils");
const kworkers = require("./kWorkers");
const dialog   = require("./dialog");
const kDOM     = require("./kDOM");
const kMisc    = require("./kMisc");

const {
    debug, debugLevel,
    error, log, logException,
    showObj, trimURL
    } = require("./kUtils");

configs.port.on("groupsChanged", groupConfigsChanged);
activity.port.on("activity",     newActivity);
activity.port.on("cleared",      activityCleared);

//======================================================================

const dlgGroupEditURL = data.url("dlgMain.html");

let dlgContentParams = {
    enabledIcon:    data.url("enabled.png"),
    disabledIcon:   data.url("disabled.png")
    };

// Other translation keys
let dlgContentKeys = [
    "dlg.enabledTip",
    "dlg.disabledTip",

    "dlg.error.badVersion",
    "dlg.error.required",
    "dlg.error.titleRequired",
    "dlg.error.urlInvalid",
    "dlg.error.urlRequired",
    "dlg.error.selectorRequired",
    "dlg.error.cssSelectorSyntax",
    "dlg.error.xpathSyntax",

    "dlg.groups.comment",
    "dlg.groups.delete",
    "dlg.groups.email",
    "dlg.groups.group",
    "dlg.groups.hide",
    "dlg.groups.method",
    "dlg.groups.part",
    "dlg.groups.url",
    "dlg.groups.version",

    "dlg.activity.hide",
    "dlg.activity.delete",
    "dlg.activity.none",

    ];

dlgContentKeys.forEach((k) => dlgContentParams[k] = _(k));

//======================================================================

var theDialog = null;

function open()
{
    /*  This actually toggles the presence of the dialog.
    */

    if (theDialog)
    {
        // The onclose() will null theDialog
        theDialog.close();
        return;
    }

    let dialogSize = configs.getDialogSize() || {width: 850, height: 700};

    theDialog = dialog.Dialog(
    {
        url:   dlgGroupEditURL,
        title: "Manage Your Kills",

        /*  A short cut delivers the command name to onUICommand().
            REVISIT localise the menu text
        */
        handleShortCuts: true,

        shortCuts: [
            {
                id: "sc-expand-all",
                key: "8",
                keytext: "*",
                modifiers: "accel",
                command: "cmd-expand-all"
            },
            {
                id: "sc-close",
                key: "W",
                modifiers: "accel",
                command: "cmd-close"
            },
            {
                id: "sc-copy",
                key: "C",
                modifiers: "accel",
                command: "cmd-copy"
            },
            {
                id: "sc-cut",
                key: "X",
                modifiers: "accel",
                command: "cmd-cut"
            },
            {
                id: "sc-delete",
                key: "VK_DELETE",
                modifiers: "accel",
                command: "cmd-delete"
            },
            {
                id: "sc-paste",
                key: "V",
                modifiers: "accel",
                command: "cmd-paste"
            },
            {   // This is only here to get a menu entry. We always delegate to FF.
                id: "sc-select-all",
                key: "A",
                modifiers: "accel",
                command: "cmd-select-all"
            },
            {
                id: "sc-undo",
                key: "Z",
                modifiers: "accel",
                command: "cmd-undo"
            },
            {
                id: "sc-redo",
                key: "Z",
                modifiers: "accel, shift",
                command: "cmd-redo"
            }
            ],

        contextMenu: [
            {
                label:    _("cm.clearActivity"),
                command:  "cmd-clear-activity",
                active:   "activity",
            },

            // At the moment there is nothing following when the context is "activity"
            //{id: "_separator_", active: "activity"},

            {
                label:    _("cm.expandCollapse"),
                shortcut: "sc-expand-all",
                command:  "cmd-expand-all",
                active:   "editor",
            },

            {id: "_separator_", active: "editor"},

            {
                command: "cmd-new-selector",   
                label:    _("cm.newSelector"), 
                active:  "editor"
            },
            {
                command: "cmd-new-part",       
                label:   _("cm.newPart"), 
                active:  "editor"
            },
            {
                command:  "cmd-new-group",      
                label:    _("cm.newGroup"), 
                active:   "editor"
            },

            {id: "_separator_", active: "editor"},

            {
                command: "cmd-import-group",
                label:   _("cm.importGroup"),
                active:   "editor"
            },

            {
                command:  "cmd-export-group",
                label:    _("cm.exportGroup"),
                active:   "validGroup",
            },

            // One or the other of these two will be active.
            {
                label:    _("cm.sortGroups"),
                command:  "cmd-sort-groups",
                active:   "sortGroups",
            },
            {
                label:    _("cm.sortParts"),
                command:  "cmd-sort-parts",
                active:   "sortParts",
            },

            {id: "_separator_", active: "editor"},

            {
                label:    _("cm.undo"),
                shortcut: "sc-undo",
                command:  "cmd-undo",
                active:   "editor",
            },
            {
                label:    _("cm.redo"),
                shortcut: "sc-redo",
                command:  "cmd-redo",
                active:   "editor",
            },
            {
                label:    _("cm.cut"),
                shortcut: "sc-cut",
                command:  "cmd-cut",
                active:   "editor",
            },
            {
                label:    _("cm.copy"),
                shortcut: "sc-copy",
                command:  "cmd-copy",
                active:   "editor",
            },
            {
                label:    _("cm.paste"),
                shortcut: "sc-paste",
                command:  "cmd-paste",
                active:   "editor",
            },
            {
                label:    _("cm.selectAll"),
                shortcut: "sc-select-all",
                command:  "cmd-select-all",
                active:   "editor",
            },
            {
                label:    _("cm.deselectAll"),
                command:  "cmd-deselect-all",
                active:   "editor",
            },

            {id: "_separator_", active: "editor"},

            {
                label:    _("cm.delete"),
                shortcut: "sc-delete",
                command:  "cmd-delete",
                active:   "editor",
            },
            ],

        // These are Firefox window features
        features:
        {
            menubar:     false,
            toolbar:     false,
            location:    false,
            resizable:   true,
            scrollbars:  true,
            width:       dialogSize.width,
            height:      dialogSize.height,
        },

        contentScriptFile: [
            data.url("../lib/kUtils.js"),
            data.url("../lib/kDOM.js"),
            data.url("dlgMainTheme.js"),
            data.url("dlgGroupEditContent.js"),
            data.url("dlgCatalogContent.js"),
            data.url("dlgAdminContent.js"),
            data.url("dlgActivityContent.js"),
            data.url("dlgMainContent.js"),
            ],

        onAttach: function(worker)
        {
            /*  This is a worker that was attached to a dialog. It will be destroyed
                automatically when the document is unloaded.

                The worker must call finalize() when it has finished
                cleaning up.  In general, if it is not here in response
                to some message then it must be in the onDetach() handler.
            */
            worker.port.on("command", function ()
            {
                let args = Array.slice(arguments);
                doCommand(args, theDialog);
            });

            worker.port.on("builtinCommand", function (cmd)
            {
                theDialog.builtinCommand(cmd);
            });

            worker.port.on("unloaded", function (cmd)
            {
                debug("dialog says unloaded");
                theDialog.finalize();
            });

            dlgContentParams.debugLevel  = debugLevel();   // may have changed
            dlgContentParams.debugEnable = kMisc.debugEnable();

            worker.port.emit("run", dlgContentParams, configs.getPrefs());
        },

        onDetach: function()
        {
            /*  At this point no more messages can be sent to the worker.
                However we can get still shutdown messages out of the worker which
                may be a fluke of the SDK implementation.
            */
            debug("dlgMain: onDetach, saving geometry", showObj(theDialog.geometry));
            configs.saveDialogSize(theDialog.geometry);
        },

        onError: logException,

        onFinalize: () => 
        {
            debug("dlgMain: onFinalize");
            theDialog = null;
        },

        onContextMenuShowing: function(info)
        {
            debug("onContextMenuShowing", showObj(info));
            theDialog.worker.port.emit("contextmenushowing", info);
        },

        onUICommand: function(info)
        {
            //log("onContextMenuCommand", showObj(info));

            // Catch certain commands that start here.
            switch (info.command)
            {
            case "cmd-import-group":
                importFile(theDialog);
                break;

            case "cmd-close":
                theDialog.close();
                break;

            case "cmd-clear-activity":
                activity.clear();
                break;

            default:
                theDialog.worker.port.emit("UICommand", info);
                break;
            }
        }
    });
}



function close()
{
    if (theDialog)
    {
        // The onclose() will null theDialog
        theDialog.close();
    }
}



function doCommand(args, theDialog)
{
    /*  Perform a command coming from the main dialog.
        A saveConfigs command may result from the close of the
        dialog in which case the worker may already be defunct.
    */
    let name = args.shift();
    debug("doCommand", name);

    switch (name)
    {
    case "getConfigs":
        let cfgs = configs.getConfigs();
        //debug("emitting", JSON.stringify(cfgs));
        theDialog.worker.port.emit(name, cfgs);
        break;

    case "saveConfigs":
        // The changed list may be empty which is used for synchronising.
        let changed = args[0];
        let removed = args[1];

        if (debugLevel())
        {
            debug("save args changed", showObj(changed));
            debug("save args removed", showObj(removed));
        }

        configs.saveConfigs(changed, removed, kUtils.AGENT_EDITOR);
        break;

    case "saveUI":
        let ui = args[0];
        //debug("save ui", showObj(ui));
        configs.saveUI(ui);
        break;

    case "exportGroup":
        //  Saving the group first will ensure it is normalised by the configs module.
        let theGroup = args[0];
        configs.saveConfigs([theGroup], null, kUtils.AGENT_EDITOR);
        let text = configs.serialise(theGroup.uuid, true);
        putFile(theDialog.window, theGroup.name, kUtils.KILL_EXT, text);
        break;

    case "getCatalog":
        if (configs.isCatalogLoaded())
        {
            deliverCatalog();
        }
        else
        {
            configs.port.once("catalogLoaded", deliverCatalog);
        }
        break;

    case "getActivity":
        let act = activity.getActivity();
        //debug("activity emitting", JSON.stringify(act));
        theDialog.worker.port.emit(name, act);
        break;

    case "installFromCatalog":
        installGroup(theDialog, args[0], "unnamed", kUtils.AGENT_CATALOG);
        break;

    case "setMenuContext":
        theDialog.setActive(args[0]);
        break;

    case "getPrefs":
        let prefs = configs.getPrefs();
        //debug("emitting", JSON.stringify(prefs));
        theDialog.worker.port.emit(name, prefs);
        break;

    case "setPref":
        configs.setPref(args[0], args[1]);
        break;

    case "setDebugLevel":
        kUtils.setDebugLevel(args[0]);  // for the top-level sandbox
        kMisc.setDebugLevel(args[0]);   // saved for later
        break;

    case "doBackup":
        doBackup(theDialog.window);
        break;

    case "doRestore":
        doRestore(theDialog.window);
        break;

    case "showHelp":
        openHelp();
        break;

    default:
        error("Unrecognised dialog command:", name);
        break;
    }
}



function deliverCatalog()
{
    let cata = configs.getCatalog();
    //debug("emitting", JSON.stringify(cata, undefined, 4));
    theDialog.worker.port.emit("getCatalog", cata);
}



function updateGroup(uuid, callback)
{
    /*  We are about to update a particular group, identified by the uuid.
        If the dialog is up then the group may have been modified in the dialog.
        We need the dialog to save it first then the callback can do the
        update. Then the dialog can show the updated group via groupConfigsChanged()
        below which will be called with a different agent.
    */
    if (theDialog)
    {
        configs.port.once("groupsChanged", callback);
        theDialog.worker.port.emit("saveGroup", uuid);
    }
    else
    {
        callback();
    }
}



function updatePrefs(prefs)
{
    if (theDialog)
    {
        theDialog.worker.port.emit("updatePrefs", prefs);
    }
}



function modelDump(callback)
{
    //  Pass a test dump from the dialog content script to the callback.
    if (theDialog)
    {
        let port = theDialog.worker.port;
        port.once("testDump", callback);
        port.emit("testDump");
    }
    else
    {
        callback(null);
    }
}



function putFile(parent, name, ext, text)
{
    //  This starts the FilePicker to save the text to the file.
    let nsIFilePicker = Ci.nsIFilePicker;

    let fp = mkFilePicker(parent, nsIFilePicker.modeSave, ext)

    /*  The defaultExtension thingo doesn't work well enough. We
        do our own.
    */
    if (!(/\.\w*$/.test(name)))
    {
        name += "." + ext;
    }

    fp.defaultString    = name;

    fp.open((result) =>
    {
        if (result === nsIFilePicker.returnOK ||
            result === nsIFilePicker.returnReplace)
        {
            let path = fp.file.path;
            let strm = null;

            debug("writing to file", path);

            try
            {
                strm = iofile.open(path, "w");

                if (!strm.closed)
                {
                    strm.write(text);
                    strm.close();
                    strm = null;
                }
            }
            catch(e)
            {
                if (strm)
                {
                    strm.close();
                }
            }
        }
    });
}



function importFile(theDialog)
{
    /*  Read a file which contains a group and save it.
        This is triggered by the editor but the editor wants to know about
        the import so the agent is AGENT_IMPORT.
    */
    let fp = mkFilePicker(theDialog.window, Ci.nsIFilePicker.modeOpen, kUtils.KILL_EXT)

    fp.open((result) =>
    {
        if (result === Ci.nsIFilePicker.returnOK)
        {
            let path = fp.file.path;    // this is always absolute
            let bn   = iofile.basename(path);
            let name = bn.replace(/\.\w*$/, "");  // remove the extension
            let url  = "file://" + path;
            debug("reading from url", url, name);

            installGroup(theDialog, url, name, kUtils.AGENT_IMPORT);
        }
    });
}



function installGroup(theDialog, url, name, agent)
{
    /*  The name will be used for the group name if the url points to a RIP file.
    */
    configs.readURL(url, name, (result) =>
    {
        if (result.error)
        {
            theDialog.window.alert("Error: Cannot read " + url + ": " + result.error);
        }
        else
        if (result.group)
        {
            let save  = true;
            let group = result.group;
            let uuid  = group.uuid;

            if (uuid === kUtils.ANON_GROUP_UUID)
            {
                configs.mergeGroupToAnon(group, agent);
            }
            else
            {
                let existing = configs.getConfigByUUID(uuid);

                if (existing)
                {
                    if (debugLevel())
                    {
                        debug("existing uuid", showObj(existing));
                    }

                    // REVISIT l10n
                    let msg = "This will overwrite group " + existing.name;
                    save = theDialog.window.confirm(msg);
                }

                if (save)
                {
                    // This will call back to groupConfigsChanged() below.
                    configs.saveConfigs([group], [], agent);
                }
            }
        }
    });
}



function groupConfigsChanged(event)
{
    if (theDialog)
    {
        // The editor does not need reminding of its own changes.
        if (event.agent !== kUtils.AGENT_EDITOR)
        {
            theDialog.worker.port.emit("groupsChanged", event);
        }
    }
}



function newActivity(info)
{
    if (theDialog)
    {
        theDialog.worker.port.emit("newActivity", info);
    }
}



function activityCleared()
{
    if (theDialog)
    {
        theDialog.worker.port.emit("activityCleared");
    }
}



function mkFilePicker(parent, mode, ext)
{
    //  This starts the FilePicker to load or save the text of a config.
    let fp = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);

    fp.init(parent, "Select a File", mode);

    fp.addToRecentDocs  = false;
    fp.appendFilter("Kill", "*." + ext + ";*.xml");
    fp.appendFilters(Ci.nsIFilePicker.filterAll);

    return fp;
}



function doBackup(parent)
{
    /*  This is run in response to clicking the Backup button on the Admin page.
        At this point the groups editor pane is deactivated so the configs have
        been saved.
    */
    let text = JSON.stringify(configs.mkBackup(), undefined, 4);
    putFile(parent, "backup", kUtils.BACKUP_EXT, text);
}



function doRestore()
{
    /*  This is run in response to clicking the Restore button on the Admin page.
    */
    let fp = mkFilePicker(theDialog.window, Ci.nsIFilePicker.modeOpen, kUtils.BACKUP_EXT)

    fp.open((result) =>
    {
        if (result === Ci.nsIFilePicker.returnOK)
        {
            let path = fp.file.path;    // this is always absolute
            let url  = "file://" + path;
            debug("reading backup from url", url);

            neturl.readURI(url).then(
                function success(text)
                {
                    //debug("read backup", text);
                    try
                    {
                        let json = JSON.parse(text);

                        if (!configs.restoreBackup(json, kUtils.AGENT_IMPORT))
                        {
                            theDialog.window.alert(_("file.error.invalidBackup"));
                        }
                    }
                    catch(e)
                    {
                        theDialog.window.alert(_("file.error.invalidBackup"));
                    }
                },
                function failure(err)
                {
                    theDialog.window.alert(_("file.error.cannotRead"));
                }
            );
        }
    });
}



function openHelp()
{
    //  We want to try various URLs e.g. en-US-help.html, en-help.html and then help.html.
    function tryFiles(files)
    {
        if (files.length > 0)
        {
            let url = data.url(files.shift());
            //log("trying help", url);
            neturl.readURI(url).then(
                function success()
                {
                    tabs.open(url);
                },
                function failure()
                {
                    tryFiles(files);
                }
            );
        }
    }

    let help  = "help.html";
    tryFiles([kMisc.locale + "-" + help, kMisc.language + "-" + help, help]);
}

//======================================================================

exports.close           = close;
exports.modelDump       = modelDump;
exports.open            = open;
exports.updateGroup     = updateGroup;
exports.updatePrefs     = updatePrefs;
