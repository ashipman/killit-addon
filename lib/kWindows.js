
/*  This tracks top-level windows to find all context menus. We want to
    get an event from a menu click at the add-on level i.e. in privileged
    code so that we can the iframes linking documents.

    A callback function will be called for each context menu popup. It
    will be the iframe element surrounding the trigger node or null if
    there isn't one.  The iframe is the outermost one containing the
    trigger node.

    *** Note that the callback is done at popup time which is well before
    a menu item can be clicked.

*/

const winUtils  = require("sdk/window/utils");
const sysEvents = require("sdk/system/events");
const unload    = require('sdk/system/unload');

const {debug, showElem, showObj, trimURL} = require("./kUtils");
const kDOM = require("./kDOM");

var iframeCallback = null;
var testCallback   = null;
var dialogWindow   = null;


function setIframeCallback(cb)
{
    iframeCallback = cb;
}


sysEvents.on('toplevel-window-ready', onNewTopWindow);
winUtils.windows().forEach(regTopWindow);

unload.when(() =>
{
    sysEvents.off('toplevel-window-ready', onNewTopWindow);
    winUtils.windows().forEach(untrackWindow);
});



function onNewTopWindow({subject})
{
    regTopWindow(subject);
}



function regTopWindow(win)
{
    // The XUL document probably hasn't loaded yet.
    if (win.document.readyState == 'complete')
    {
        trackWindow(win);
    }
    else
    {
        win.addEventListener('load', onNewTopLoad, false);
    }
}



function onNewTopLoad(ev)
{
    // Strangely the target is a XULDocument
    if (ev.target)
    {
        let window = ev.target.defaultView;

        if (window)
        {
            window.removeEventListener('load', onNewTopLoad, false);
            trackWindow(window);
        }
    }
}



function trackWindow(win)
{
    let cmenu = win.document.getElementById("contentAreaContextMenu");

    if (cmenu)
    {
        //debug("found cmenu", cmenu, cmenu.nodeName);
        cmenu.addEventListener("popupshowing", onContextClick, false);
    }

    win.addEventListener("testHarness", onTestEvent, false);

    // Our dialog has a different name
    let cmenu2 = win.document.getElementById("contextMenu");

    if (cmenu2)
    {
        //debug("trackWindow: found the dialog");
        dialogWindow = win;
    }
}



function untrackWindow(win)
{
    let cmenu = win.document.getElementById("contentAreaContextMenu");

    if (cmenu)
    {
        cmenu.removeEventListener("popupshowing", onContextClick, false);
    }

    win.removeEventListener("testHarness", onTestEvent, false);

    if (win === dialogWindow)
    {
        //debug("untrackWindow: removed the dialog");
        dialogWindow = null;
    }
}



function onContextClick(ev)
{
    if (!iframeCallback)
    {
        return;
    }

    let trigger = ev.target.triggerNode;
    debug("onContextClick ", trigger);

    // Track the path back through iframes.
    let elem   = trigger;
    let iframe = null;

    while (elem)
    {
        let doc = elem.ownerDocument;
        let win = doc.defaultView;

        elem = win.frameElement;

        if (elem)
        {
            iframe = elem;
        }
    }

    iframeCallback(iframe);
}



function onTestEvent(ev)
{
    //debug("received onTestEvent", showObj(ev.detail));
    debug("received onTestEvent", ev.detail.command, ev.detail.arg);
    if (testCallback)
    {
        testCallback(ev);
    }
}



function setTestCallback(callback)
{
    testCallback = callback;
}



function getDialogWindow()
{
    /*  Return what we think is the most recent dialog window.
    */
    return dialogWindow;
}

//======================================================================

exports.setIframeCallback = setIframeCallback;
exports.setTestCallback   = setTestCallback;
exports.getDialogWindow   = getDialogWindow;
